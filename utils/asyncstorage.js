import AsyncStorage from '@react-native-community/async-storage'

export const setItem = (key, value) => {
  if (value === null) {
    value = ''
  }
  return AsyncStorage.setItem(key, value)
}

export const getItem = (key) => {
  return AsyncStorage.getItem(key)
}

export const removeItem = (key) => {
  return AsyncStorage.removeItem(key)
}

export const multiGet = ([key1, key2]) => {
  return AsyncStorage.multiGet([key1, key2])
}
import i18n from 'i18n-js'

import en from './locales/en.json'
import vi from './locales/vi.json'

const translations = { en, vi }

//i18n.defaultLocale = 'en'
i18n.locale = 'vi'
i18n.fallbacks = true
i18n.translations = translations

export default i18n
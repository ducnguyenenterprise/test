import TrackPlayer, { STATE_PLAYING, STATE_PAUSED, STATE_STOPPED } from 'react-native-track-player'
// import { ProgressComponent } from 'react-native-track-player'
// import { TrackPlayerEventTypes } from 'react-native-track-player'
import { alertPlaybackError } from '../alerts'
import { handleShowPoint } from '../showPointUtil'

let didAddListeners = false

let isPlaying = false
let isReceivePoint5Min = false
let isReceivePointFull = false
let startDate

module.exports = async () => {
  if (didAddListeners) {
    console.log('TrackPlayer:', 'Listeners have already been added. Do not add again.');
    return;
  }
  didAddListeners = true
  TrackPlayer.addEventListener('remote-play', async () => await TrackPlayer.play())

  TrackPlayer.addEventListener('remote-pause', async () => await TrackPlayer.pause())

  TrackPlayer.addEventListener('remote-stop', () => TrackPlayer.destroy())

  TrackPlayer.addEventListener('remote-jump-forward', async () => {
    console.log('REMOTE-FWD')
    TrackPlayer.getPosition()
    .then(pos => {
      TrackPlayer.seekTo(pos + 30)
    })
  })

  TrackPlayer.addEventListener('playback-error', (err) => {
    alertPlaybackError(err.message)
  })

  TrackPlayer.addEventListener('remote-jump-backward', () => {
    console.log('REMOTE-BCK')
    TrackPlayer.getPosition()
    .then(pos => {
      TrackPlayer.seekTo(pos - 15)
    })
  })

  let playingBeforeDuck;
  //let volumeBeforeDuck;
  //const DUCKED_VOLUME = 0.2;
  TrackPlayer.addEventListener(
    'remote-duck',
    async ({ paused, permanent, ducking }) => {
      // if (permanent) {
      //   TrackPlayer.stop();
      //   return;
      // }

      if (paused) {
        const playerState = await TrackPlayer.getState();
        playingBeforeDuck = playerState === TrackPlayer.STATE_PLAYING;
        TrackPlayer.pause();
        return;
      }

      // if (ducking) {
      //   const volume = await TrackPlayer.getVolume();
      //   if (volume > DUCKED_VOLUME) {
      //     volumeBeforeDuck = volume;
      //     TrackPlayer.setVolume(DUCKED_VOLUME);
      //   }
      //   return;
      // }

      if (playingBeforeDuck) {
        TrackPlayer.play();
      }

      // const playerVolume = await TrackPlayer.getVolume();
      // if (volumeBeforeDuck > playerVolume) {
      //   TrackPlayer.setVolume(volumeBeforeDuck || 1);
      // }

      playingBeforeDuck = null;

      //volumeBeforeDuck = playingBeforeDuck = null;
    }
  );
  
  TrackPlayer.addEventListener('playback-state', (state) => {
    if (state.state === STATE_PLAYING) {
      isPlaying = true
      startDate = new Date()
    } else if (state.state === STATE_PAUSED || state.state === STATE_STOPPED) {
      isPlaying = false
    }
  })

  TrackPlayer.addEventListener('playback-track-changed', (track) => {
    isReceivePointFull = false
  })

  // This service needs to be registered for the module to work
  // but it will be used later in the "Receiving Events" section
}

playerProgressRun = (place,duration) => {
  if (isPlaying) {
    const currentDate = new Date()
    const interval = (currentDate.getTime() - startDate.getTime())/1000
    
    if ((interval > 5*60) && !isReceivePoint5Min) { //5min
      isReceivePoint5Min = true
      receivePoint('5min')
    }
    if ((interval >= (duration - 5)) && !isReceivePointFull) {
      isReceivePointFull = true
      receivePoint('full')
    }
  }
}

receivePoint = (type) => {
  if (type === '5min') {
    handleShowPoint()
    
  } else if (type === 'full') {
    handleShowPoint()
  }
}
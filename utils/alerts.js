import { Alert } from 'react-native'

export const alertServerError = () => {
  Alert.alert(
    'Server error!',
    'My Alert Msg',
    [
      {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  )
}

export const alertPlaybackError = (error) => {
  Alert.alert(
    'Playback error',
    error,
    [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
  )
}

export const alertInternetConnection = () => {
  Alert.alert(
    'No internet!',
    'My Alert Msg',
    [
      {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  )
}

export const alertTrackPlayerError = () => {
  Alert.alert(
    'Track Player Error!',
    'My Alert Msg',
    [
      {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  )
}

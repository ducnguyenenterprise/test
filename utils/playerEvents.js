import TrackPlayer from 'react-native-track-player'

export const eventHandler = async data => {
  let position

  switch (data.type) {
    case 'remote-play':
      TrackPlayer.play()
      break
    case 'remote-pause':
      TrackPlayer.pause()
      break
    case 'remote-jump-forward':
      position = await TrackPlayer.getPosition()
      TrackPlayer.seekTo(position + 30)
      break
    case 'remote-jump-backward':
      position = await TrackPlayer.getPosition()
      TrackPlayer.seekTo(position - 15)
      break
    case 'remote-stop':
    case 'playback-queue-ended':
      TrackPlayer.seekTo(0)
      // pause()
      break
  }
}
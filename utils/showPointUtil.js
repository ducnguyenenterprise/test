import { store } from '../redux/store'
import { setShowPoint } from '../screens/utils/actions'

export const handleShowPoint = () => {
    store.dispatch(setShowPoint(true))
    setTimeout(() => {
        store.dispatch(setShowPoint(false))
    }, 1100)
}
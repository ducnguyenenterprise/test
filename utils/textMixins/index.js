import React from 'react'
import { Text, TouchableWithoutFeedback } from 'react-native'
import { styles } from './styles'

export const Heading3 = (props) => {
  return (
    <Text style={styles.heading3}>{props.children}</Text>
  )
}

export const Heading5 = (props) => {
  return (
    <Text style={[styles.heading5, props.styles]}>{props.children}</Text>
  )
}

export const ButtonText = (props) => {
  return (
    <Text style={styles.buttonText}>{props.children}</Text>
  )
}

export const Heading3light = (props) => {
  return (
    <Text numberOfLines={props.lines} style={[styles.heading3light, props.styles]}>{props.children}</Text>
  )
}

export const Category = (props) => {
  return (
    <Text style={styles.category}>{props.children}</Text>
  )
}

export const Episode = (props) => {
  return (
    <Text numberOfLines={props.lines} style={[styles.episode, props.styles]}>{props.children}</Text>
  )
}

export const EpisodeTitle = (props) => {
  return (
    <Text numberOfLines={props.lines} style={[styles.episodeTitle, props.styles]}>{props.children}</Text>
  )
}

export const EpisodeTitleLight = (props) => {
  return (
    <Text numberOfLines={props.lines} style={styles.episodeTitleLight}>{props.children}</Text>
  )
}

export const Body1 = (props) => {
  return (
    <Text numberOfLines={props.lines} style={[styles.body1, props.styles]}>{props.children}</Text>
  )
}

export const Body1light = (props) => {
  return (
    <Text numberOfLines={props.lines} style={[styles.body1light, props.styles]}>{props.children}</Text>
  )
}

export const SeeMore = (props) => {
  return (
    <Text style={styles.seeMore}>{props.children}</Text>
  )
}

export const SeeMoreLight = (props) => {
  return (
    <Text style={styles.seeMoreLight}>{props.children}</Text>
  )
}

export const Body2 = (props) => {
  return (
    <Text style={[styles.body2, props.styles]}>{props.children}</Text>
  )
}

export const Body2light = (props) => {
  return (
    <Text style={[styles.body2light, props.styles]} numberOfLines={props.lines}>{props.children}</Text>
  )
}

export const LargeParagraph = (props) => {
  return (
    <Text style={styles.largeParagraph}>{props.children}</Text>
  )
}

export const Author = (props) => {
  return (
    <Text numberOfLines={props.lines} style={styles.author}>{props.children}</Text>
  )
}

export const AuthorLight = (props) => {
  return (
    <Text numberOfLines={props.lines} style={[styles.authorLight, props.styles]}>{props.children}</Text>
  )
}

export const TimeLight = (props) => {
  return (
    <Text style={[styles.timeLight, props.styles]}>{props.children}</Text>
  )
}

export const Time = (props) => {
  return (
    <Text style={[styles.time, props.styles]}>{props.children}</Text>
  )
}
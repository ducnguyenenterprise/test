import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  heading3: {
    fontFamily: 'Roboto-Medium',
    fontSize: 24,
    lineHeight: 32,
    textTransform: 'capitalize',
    color: /*'#232735'*/ '#3E465F',
  },
  heading5: {
    fontFamily: 'Roboto-Medium',
    fontSize: 16,
    lineHeight: 24,
    textTransform: 'capitalize',
    color: /*'#232735'*/ '#3E465F',
  },
  buttonText: {
    fontFamily: 'Roboto-Regular',
    fontSize: 18,
    //lineHeight: 16,
    textTransform: 'capitalize',
    letterSpacing: 0.5,
    color: '#FFFFFF',
  },
  heading3light: {
    fontFamily: 'Roboto-Medium',
    fontSize: 24,
    lineHeight: 32,
    textTransform: 'capitalize',
    color: 'white'
  },
  category: {
    fontFamily: 'Roboto-Regular',
    fontSize: 18,
    lineHeight: 24,
    textTransform: 'uppercase',
    color: '#232735',
    marginBottom: 10
  },
  episode: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    lineHeight: 18,
    textTransform: 'capitalize',
    color: '#3E465F',
    letterSpacing: 1
  },
  episodeTitle: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    lineHeight: 14,
    textTransform: 'capitalize',
    color: '#232735',
    letterSpacing: 0.75
  },
  episodeTitleLight: {
    fontFamily: 'Roboto-Medium',
    fontSize: 14,
    lineHeight: 14,
    textTransform: 'capitalize',
    color: 'white',
    letterSpacing: 0.75
  },
  body1: {
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    lineHeight: 18,
    color: '#505565',
  },
  body1light: {
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    lineHeight: 18,
    color: '#D3D4D8',
  },
  seeMore: {
    fontFamily: 'Roboto-Bold',
    fontSize: 16,
    lineHeight: 18,
    color: '#505565',
  },
  seeMoreLight: {
    fontFamily: 'Roboto-Bold',
    fontSize: 16,
    lineHeight: 18,
    color: '#D3D4D8',
  },
  body2: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    lineHeight: 18,
    color: /*'#505565'*/  '#3E465F',
  },
  body2light: {
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    lineHeight: 18,
    color: '#D3D4D8',
  },
  body2semiBold: {
    fontFamily: 'Roboto-Medium',
    fontSize: 12,
    lineHeight: 24,
    color: '#505565',
  },
  largeParagraph: {
    fontFamily: 'Roboto-Regular',
    fontSize: 20,
    lineHeight: 30,
    color: '#505565',
  },
  author: {
    fontFamily: 'Roboto-Regular',
    fontSize: 12,
    lineHeight: 16,
    textTransform: 'capitalize',
    color: '#505565',
  },
  authorLight: {
    fontFamily: 'Roboto-Regular',
    fontSize: 12,
    lineHeight: 16,
    textTransform: 'capitalize',
    color: 'white',
  },
  time: {
    fontFamily: 'Roboto-Regular',
    fontSize: 12,
    lineHeight: 32,
    color: '#232735',
  },
  timeLight: {
    fontFamily: 'Roboto-Regular',
    fontSize: 12,
    color: '#D3D4D8',
  },
  title: {
    fontFamily: 'Roboto-Medium',
    fontSize: 18,
    lineHeight: 24,
    textTransform: 'capitalize',
    color: '#232735',
  },
})
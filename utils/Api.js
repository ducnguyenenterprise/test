import axios from 'axios'
import { apiRoot } from './config.js'
import { PointUser } from '../models/point_user.js'

export default class Api {
  
  get(path) {
    return axios.get(apiRoot + path, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': 0
      }
    })
    .then(response => {
      console.log(response)
      return response.data
    })
    .catch(error => error.message)
  }

  post(path, body) {
    return axios.post(apiRoot + path, body)
  }

  put(path, body) {
    return axios.put(apiRoot + path, body)
  }

  delete(path, body) {
    return axios.delete(apiRoot + path, { data: body })
  }

  fetchHome(personaId) {
    return this.get(`/home?persona=${personaId}`)
  }

  fetchShow(id) {
    return this.get(`/show/${id}/?source=listen_notes`) // ?source=listen_notes
  }

  fetchPersonas() {
    return this.get('/personas')
  }

  search(input, type, page, language) {
    return this.get(`/search?q=${input}&type=${type}&page=${page}&language=${language}`)
  }

  addToLibrary(userId, contentId, contentType) {
    const body = {content_source_id: contentId, content_type: contentType}
    return this.post(`/library/${userId}`, body)
  }

  removeFromLibrary(userId, id) {
    return this.delete(`/library/rm/${userId}`, {'content_source_id': id})
  }
  
  fetchLibrary(userId) {
    return this.get(`/library/${userId}`)
  }

  addEvent(userId, body) {
    return this.post(`/event/${userId}`, body)
  }

  newUser() {
    return this.post('/user/new')
  }

  authenticate(userId, accessToken) {
    return this.post('/user/auth', {userId, accessToken})
  }

  fetchTopSearches() {
    return this.get('/admin/search')
  }

  fetchShowTitle(showId) {
    return this.get(`/episode/showTitle/${showId}`)
  }

  changeProfilePicture(userId, url) {
    return this.put(`/user/${userId}`, {'user' : { 'image_url': url }})
  }

  getUserData(id) {
    return this.get(`/user/${id}`)
  }

  setOneSignalId(oneSignalId, userId) {
    return this.put(`/user/${userId}`, {
      user: {
        onesignal_id: oneSignalId
      }
    })
  }

  getPointByUser(userId) {
    return this.get(`/points/${userId}/total`);
  }

  addPointToUser(body) {
    return this.post(`/points`, body);
  }

  updatePointToUser(userId, body) {
    return this.put(`/points/${userId}`, body);
  }
}
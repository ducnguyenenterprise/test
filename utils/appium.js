import wd from 'wd'
const { join } = require("path")
import { capabilities } from './config'

jest.setTimeout(300000)
jasmine.DEFAULT_TIMEOUT_INTERVAL = 300000

const capabilities = {
  automationName: "UiAutomator2",
  deviceName: "Appium Simulator", // This is name of your Simulator
  platformName: "Android",
  platformVersion: "9", // Android version of Simulator
  orientation: "PORTRAIT",
  maxInstances: 1,
  app: join(process.cwd(), "/android/app/build/outputs/apk/debug/app-debug.apk"),
  // absolute path to the apk 
  noReset: true,
  newCommandTimeout: 240,
  adbExecTimeout: 300000
}

console.log(JSON.stringify(capabilities))

class Helper {
  async setup() {
    this.driver = wd.promiseChainRemote('0.0.0.0', 4723)
    await this.driver.init(capabilities)
    await this.driver.sleep(6000)
  }
  teardown() {
    return this.driver.quit()
  }
}

export const helper = new Helper()
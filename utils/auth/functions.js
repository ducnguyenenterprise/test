import { okta } from '../config'

// based on the selected social identity provider it adds an additional idp parameter to the base config
export const socialConfig = socialIdentityProvider => {
  let clientId = socialIdentityProvider === 'facebook' ? okta.facebookClientId : okta.googleClientId
  let config = JSON.parse(JSON.stringify(okta.baseConfig))
  config.additionalParameters.idp = clientId
  return config
}

export const authorizeUser = async (socialIdentityProvider) => {
  let authConfig = socialConfig(socialIdentityProvider)
  console.log('AUTH_CONFIG:', authConfig)
  
  let authData = null
  
  try {
    authData = await authorize(authConfig)
    console.log('authData:', authData)

  } catch (error) {
    console.log('Failed to authorize', error.message)
  }
  return authData
}

export const refreshSession = async (socialIdentityProvider, refreshToken) => {
  let config = socialConfig(socialIdentityProvider)
  let authState = null
  try {
    authState = await refresh(config, {
      refreshToken
    }) 
  } catch (error) {
    console.log('Failed to refresh token', error.message)
  }
  return authState
}

export const validateAccessTokenExpirationDate = expirationDate => {
  let isExpired = true
  const currentDate = new Date()
  if (currentDate > expirationDate) {
    isExpired = false
  }
  return isExpired
}

// Set expiration date to 8 hours from now, okta expiration date is always California time
export const setExpirationDate = () => {
  const currentDateTime = new Date()
  const expirationDate = currentDateTime.setHours(currentDateTime.getHours() + 8)
  return expirationDate
}
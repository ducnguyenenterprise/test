/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import TrackPlayer from 'react-native-track-player';

TrackPlayer.setupPlayer({
  minBuffer: 5
})
TrackPlayer.registerPlaybackService(() => require('./utils/player/service'));
AppRegistry.registerComponent(appName, () => App);

import { UserRepository } from "../repositories/user_repository";
import { PointUserRepository } from "../repositories/point_user_repository";

export class Waves8UnitOfWork{
    user_repository;
    point_user_repository;
    constructor(){
        this.user_repository = new UserRepository();
        this.point_user_repository = new PointUserRepository(this.user_repository);
    }
}
export default {
  oidc: {
    clientId: '0oa284eqjkKRshfnm357',
    redirectUri: 'com.okta.dev-934735:/callback	',
    endSessionRedirectUri: 'com.okta.dev-934735:/callback	',
    discoveryUri: 'https://dev-934735.okta.com/oauth2/default',
    scopes: ['openid', 'profile', 'offline_access'],
    requireHardwareBackedKeyStore: true,
  },
};

//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Laszlo Mári on 12/11/19.
//  Copyright © 2019 wvs8. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end

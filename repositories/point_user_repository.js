import { BaseRepository } from "./base_repository";
import { getItem, setItem } from "../utils/asyncstorage";
import { PointUser } from "../models/point_user";
import { PointEvent, POINT_EVENTS } from "../models/point_event";

export class PointUserRepository extends BaseRepository{
    $user_repository;
    $isAddedPointCompleted = false;
    constructor(user_repository){
        super();
        this.$user_repository = user_repository;
    }

    getPointByUser = async (userId = null) => {
        if(userId == null || userId == ""){
            userId = await getItem("userId");
        }
        if(userId != null && userId != ""){
            let pointUserJson = await getItem("POINT_USER");
            let pointUser = JSON.parse(pointUserJson);
            if(pointUser == null || pointUser.user.id != userId){
                const totalPoint = await this.$api.getPointByUser(userId);
                if(pointUser == null){
                    pointUser = new PointUser();
                }
                pointUser.user = await this.$user_repository.getUserData();
                pointUser.total = totalPoint.total ?? 0;
                await setItem("POINT_USER", JSON.stringify(pointUser));
            }
            return pointUser;
        }
        return null;
    }


    checkAllowAddPoint = async (eventName = POINT_EVENTS.SignIn) => {
        let userId = await getItem("userId");
        
        if(userId != null && userId != ""){
            let pointUser = await this.getPointByUser(userId);
            if(pointUser != null){
                let events = [...pointUser.point_events];
                switch(eventName){
                    case POINT_EVENTS.SignIn:
                        let signInEvent = events.find(f => f.event_name == POINT_EVENTS.SignIn);
                        if(signInEvent != null){
                            return false;
                        }
                        break;
                    case POINT_EVENTS.DailyLogin:
                        let dailyLoginEvent = events.find(f => f.event_name == POINT_EVENTS.DailyLogin);
                        if(dailyLoginEvent != null){
                            let now = new Date();
                            let loginDate = new Date(dailyLoginEvent.created_date);
                            if(loginDate.getDate() == now.getDate() 
                            && loginDate.getMonth() == now.getMonth() 
                            && loginDate.getFullYear() == now.getFullYear()){
                                return false;
                            }else{
                                let newEvents = pointUser.point_events.filter(f => f.event_name != POINT_EVENTS.DailyLogin);
                                pointUser.point_events = [...newEvents];
                            }
                        }
                        break;
                }
            }
            return true;
        }
        return false;

    };

    addPointToUser = async (userId = null, eventName = POINT_EVENTS.SignIn) => {
        this.$isAddedPointCompleted = false;
        if(userId == null || userId == ""){
            userId = await getItem("userId");
        }
        if(userId != null && userId != ""){
            let pointUser = await this.getPointByUser(userId);
            if(await this.checkAllowAddPoint(eventName) === false){
                return pointUser;
            }
            const result = await this.$api.addPointToUser({
                user:{
                    id: userId
                },
                eventName: eventName
            });
            let pointEvent = new PointEvent();
            pointEvent.user_id = userId;
            pointEvent.id = result.data.pointsEvent.id;
            pointEvent.event_name = result.data.pointsEvent.name;
            pointEvent.point = result.data.pointsEvent.count;
            
            pointUser.point_events.push(pointEvent);
            pointUser.total += pointEvent.point;
            await setItem("POINT_USER", JSON.stringify(pointUser));
            this.$isAddedPointCompleted = true;
            return pointEvent;
        }
        return null;
    }

    updatePointToUser = async (userId = null, point = 0) => {
        if(userId == null || userId == ""){
            userId = await getItem("userId");
        }
        let isLoggedIn = JSON.parse(await getItem("isLoggedIn"));
        let pointUser = await this.getPointByUser(userId);
        if(isLoggedIn === true && point > 0){
            const result = await this.$api.updatePointToUser(userId, {
                total: point
            });
            pointUser.total = result.data.pointsTotal.count;
            pointUser.id = result.data.pointsTotal.id;
            pointUser.created_date = result.data.pointsTotal.createdAt;
            pointUser.modified_date = result.data.pointsTotal.updatedAt;
            await setItem("POINT_USER", JSON.stringify(pointUser));
        }
        return pointUser;
    };
}
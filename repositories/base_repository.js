import Api from "../utils/Api";
import { getItem } from "../utils/asyncstorage";

export class BaseRepository {
    $api;
    constructor(){
        this.$api = new Api();
    }
}
import { BaseRepository } from "./base_repository";
import { getItem } from "../utils/asyncstorage";
import { User } from "../models/user";

export class UserRepository extends BaseRepository{
    constructor(){
        super();
    }

    async getUserData() {
        const userId = await getItem("userId");
        if(userId != null || userId!= ""){
            let userData = await this.$api.getUserData(userId);
            var user = new User();
            user.id = userData.user.id;
            user.email = userData.user.email;
            user.image_url = userData.user.image_url;
            user.name = userData.user.name;
            user.onesignal_id = userData.user.onesignal_id;
            user.persona_id = userData.user.persona_id;
            user.social_id = userData.user.social_id;
            return user;
        }
        return null;
    }
}
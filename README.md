# Waves Mobile
## Prequisites
- node 12.10
- yarn 1.15.2
- react-native-cli 2.0.1
- XCode and Android Studio
- adb (v29.0)
- Recommended: MacOS Mojave
- Recommended: [React Native Debugger](https://github.com/jhen0409/react-native-debugger)
- For testing: Appium desktop client, Jest
## Installation
1. Clone repo
2. Run `yarn install`
3. Connect your Android device (we are testing on live devices, not tested in the emulator)
4. Run `adb devices` and makes sure your device shows up
5. Run `yarn android` or `react-native run-ios --device "Laszlo’s iPhone"`
## Testing
1. Make sure Appium is installed and launch the desktop client
2. Connect your Android device (testing not tested/working on iOS for now)
3. Install the app on the device with the above steps (make sure Metro bundler is running)
4. Run `yarn test`

import { helper } from '../../../utils/appium'

describe('Navigation tests', () => {
  beforeAll(() => helper.setup())
  afterAll(() => helper.teardown())
  
  test('Intro screen / login button exists', async () => {
    expect(await helper.driver.hasElementByAccessibilityId('loginButton')).toBe(true)
  })

  test('Intro screen / continue button exists', async () => {
    expect(await helper.driver.hasElementByAccessibilityId('skipToApp')).toBe(true)
  })

  test(`On click to continue button, the tabbar with the 'Home', 'Search', 'Profile', and 'Settings' tabs  exists and the home screen opens with the twext 'Home'`, async () => {
    await helper.driver.elementByAccessibilityId('skipToApp').click()
    await helper.driver.sleep(4000)
    expect(await helper.driver.hasElementByAccessibilityId('Home')).toBe(true)
    expect(await helper.driver.hasElementByAccessibilityId('Search')).toBe(true)
    expect(await helper.driver.hasElementByAccessibilityId('Profile')).toBe(true)
    expect(await helper.driver.hasElementByAccessibilityId('Settings')).toBe(true)
    expect(await helper.driver.elementByAccessibilityId('homeText').text()).toBe('Home')
  })

  test(`On click to search tab, the search screen opens with the text 'Search'`, async () => {
    await helper.driver.elementByAccessibilityId('Search').click()
    await helper.driver.sleep(4000)
    expect(await helper.driver.elementByAccessibilityId('searchText').text()).toBe('Search')
  })

  test(`On click to profile tab, the profile screen opens with the text 'Profile'`, async () => {
    await helper.driver.elementByAccessibilityId('Profile').click()
    await helper.driver.sleep(4000)
    expect(await helper.driver.elementByAccessibilityId('profileText').text()).toBe('Profile')
  })

  test(`On click to settings tab, the settings screen opens with the text 'Settings'`, async () => {
    await helper.driver.elementByAccessibilityId('Settings').click()
    await helper.driver.sleep(4000)
    expect(await helper.driver.elementByAccessibilityId('settingsText').text()).toBe('Settings')
  })

  test(`On click to home tab, the home screen opens with the text 'Home'`, async () => {
    await helper.driver.elementByAccessibilityId('Home').click()
    await helper.driver.sleep(4000)
    expect(await helper.driver.elementByAccessibilityId('homeText').text()).toBe('Home')
  })
  

})

import React from 'react'
import { View, Image } from 'react-native'
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs'
import HomeScreen from '../../screens/Home/Home'
import SearchScreen from '../../screens/Search/Search'
import SocialScreen from '../../screens/Social/Social'
import LibraryScreen from '../../screens/Library/Library'
import orangeHome from '../../assets/orange_home.png'
import orangeSocial from '../../assets/orange_social.png'
import orangeLibrary from '../../assets/orange_library.png'
import orangeSearch from '../../assets/orange_search.png'
import greyHome from '../../assets/grey_home.png'
import greySocial from '../../assets/grey_social.png'
import greyLibrary from '../../assets/grey_library.png'
import greySearch from '../../assets/grey_search.png'
import { getItem, setItem } from '../../utils/asyncstorage'
import TabBar from './TabBar'

const generateKey = () => {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
}

const TabNavigator = createMaterialTopTabNavigator({
  Home: {
    screen: HomeScreen,
    tabBarLabel: 'navbar-home',
  },
  Community: {
    screen: SocialScreen,
    tabBarLabel: 'navbar-community',
  },
  Library: {
    screen: LibraryScreen,
    tabBarLabel: 'navbar-library',
  },
  Search: {
    screen: SearchScreen,
    tabBarLabel: 'navbar-search',
  }
},
{
  tabBarComponent: TabBar,
  tabBarPosition: 'bottom', 
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state
      let icon = ''
      switch (routeName) {
        case 'Home':
          if (focused) {
            icon = orangeHome
          } else {
            icon = greyHome
          }
          break
        case 'Community':
          if (focused) {
            icon = orangeSocial
          } else {
            icon = greySocial
          }
          break
        case 'Library':
          if (focused) {
            icon = orangeLibrary
          } else {
            icon = greyLibrary
          }
          break
        case 'Search':
          if (focused) {
            icon = orangeSearch
          } else {
            icon = greySearch
          }
          break
      }

      return (
        <View accessibilityLabel={routeName} style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start', width: 32, height: 32}}>
          <Image source={icon} style={{width: 32, height: 32}}/>
        </View>
      )
    },
    tabBarOnPress: async ({navigation, defaultHandler}) => {
      if (navigation.state.routeName === 'Library') {
        // generate a random key to store event in async
        const eventKey = generateKey()

        // we add the key to a string of keys waiting to be sent to the Api
        getItem('eventString')
        .then(string => {
          setItem('eventString', string + '||' + eventKey)
        })

        const userId = await getItem('userId')

        const body = {
          event_key: eventKey,
          event_type: 'open',
          content_type: 'library',
          date: new Date(),
          user_id: userId
        }

        // save data to async with the random key
        setItem(eventKey, JSON.stringify(body))
      }

      defaultHandler()
    },
    backBehavior: 'history',
    lazy: false,
    swipeEnabled: false,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: '#FF724C',
      inactiveTintColor: '#D3D4D8',
      tabStyle: {
        height: 54,
        borderTopColor: 'transparent'
      },
      labelStyle: {
        fontSize: 14,
        lineHeight: 24,
        fontFamily: 'Roboto-Regular',
      },
      style: {
        height: 54,
        borderTopColor: '#D3D4D8',
        borderTopWidth: 1
      }
     
    }
  })
})

export default TabNavigator
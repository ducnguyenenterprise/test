import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Platform, Dimensions } from 'react-native'
import i18n from '../../utils/i18n'
import { connect } from 'react-redux'

const S = StyleSheet.create({
  container: { flexDirection: "row", height: 54, borderTopColor: '#F0F1F3', borderTopWidth: 1, marginBottom: (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 34 : 0},
  tabButton: { flex: 1, justifyContent: "center", alignItems: "center" }
})

const TabBar = props => {
  const {
    renderIcon,
    getLabelText,
    activeTintColor,
    inactiveTintColor,
    onTabPress,
    onTabLongPress,
    getAccessibilityLabel,
    navigation
  } = props

  const { routes, index: activeRouteIndex } = navigation.state

  i18n.locale = props.language === 'Vietnamese' ? 'vi' : 'en'

  return (
    <View style={S.container}>
      {routes.map((route, routeIndex) => {
        const isRouteActive = routeIndex === activeRouteIndex
        const tintColor = isRouteActive ? activeTintColor : inactiveTintColor

        return (
          <TouchableOpacity
            key={routeIndex}
            style={S.tabButton}
            onPress={() => {
              onTabPress({ route })
            }}
            onLongPress={() => {
              onTabLongPress({ route })
            }}
            accessibilityLabel={getAccessibilityLabel({ route })}
          >
            {renderIcon({ route, focused: isRouteActive, tintColor })}

            <Text style={{ color: tintColor }}>{i18n.t(getLabelText({ route }))}</Text>
          </TouchableOpacity>
        )
      })}
    </View>
  )
}

const mapStateToProps = state => {
  return {
    language: state.language,
  }
}

const mapDispatchToProps = dispatch => ({
 
})

export default connect(mapStateToProps, mapDispatchToProps)(TabBar)
import { createAppContainer } from "@react-navigation/native"
import { createStackNavigator } from 'react-navigation-stack'
import ShowScreen from '../../screens/Show/Show'
import TabNavigator from './TabNavigator'
import ProfileScreen from '../../screens/Profile/Profile'
import PlaybackScreen from '../../screens/Player/PlaybackScreen'
import { fromBottom } from 'react-navigation-transitions'

let MainStack = createStackNavigator({
  App: TabNavigator,
  Show: ShowScreen,
  Profile: ProfileScreen
},
{
  initialRouteName: 'App',
  headerMode: 'none'
})

const RootStack = createStackNavigator(
  {
    Main: {
      screen: MainStack,
    },
    PlaybackScreen: {
      screen: PlaybackScreen,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
    transitionConfig: () => fromBottom(350),
  }
);

let StackNavigation = createAppContainer(RootStack)

export default StackNavigation

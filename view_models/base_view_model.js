
export class BaseViewModel{
    $unit_of_work;
    getComponent;
    constructor(component, unit_of_work){
        this.getComponent = () => component;
        this.$unit_of_work = unit_of_work;
    }

    setState(data){
        this.getComponent().setState(data);
    }
}
import { BaseViewModel } from '../base_view_model';
import { PointUser } from '../../models/point_user';

export class ProfileViewModel extends BaseViewModel{

    switch = false;
    languageSwitch = false;
    
    constructor(component, unit_of_work){
        super(component, unit_of_work);
    }
}
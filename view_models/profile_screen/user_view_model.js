import { BaseViewModel } from "../base_view_model";

export class UserViewModel extends BaseViewModel{
    imagePath = "";
    imageUri = "";
    img = {};
    point = 0;
    editing = false;

    constructor(component,unit_of_work){
        super(component, unit_of_work);
    }

    getPointByUser = async () => {
        const pointUser = await this.$unit_of_work.point_user_repository.getPointByUser();
        if(pointUser != null){
            if(this.$unit_of_work.point_user_repository.$isAddedPointCompleted === true){
                this.$unit_of_work.point_user_repository.$isAddedPointCompleted = false;
                this.repeatPointPlus(0, pointUser.total, 2000);
            }else {
                this.setState({point: pointUser.total});
            }
        }
    }

    repeatPointPlus = (oldPoint, newPoint, duration) => {
        var increment = Number.parseInt((newPoint)/(duration / 250));
        if(oldPoint + increment < newPoint){
            this.setState({point: oldPoint + increment});
            setTimeout(this.repeatPointPlus, 100, oldPoint + increment, newPoint, duration);
        } else{
            this.setState({point: newPoint});
        }
    };
}
import React from 'react'
import { Modal, Text, TouchableWithoutFeedback, View, Dimensions, Image } from 'react-native'
import { connect } from 'react-redux'
import { setSpeedModal } from '../utils/actions'
import TrackPlayer from 'react-native-track-player'
import i18n from '../../../utils/i18n'
import close from '../../../assets/close_speed.png'

class Speed extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      rates: [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2],
      speedIndex: 3
    }
  }

  changeSpeed = (direction) => {
    const rate = this.state.rates[this.state.speedIndex + direction]
    this.setState({ speedIndex: this.state.speedIndex + direction })
    TrackPlayer.setRate(rate)
    this.props.syncSpeed(rate.toString() + 'x')
  }

  render() {
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <Modal
        animationType='slide'
        transparent={true}
        visible={this.props.speedModal === 'opened' ? true : false}
        onRequestClose={() => console.log('close')}
        >
        <TouchableWithoutFeedback onPress={() => this.props.setSpeedModal('closed')}><View style={{position: 'absolute', top: 0, height: window.height * 0.8, width: window.width}}></View></TouchableWithoutFeedback>
        <View style={{position: 'absolute', bottom: 0, height: window.height * 0.2, width: window.width, backgroundColor: 'rgba(0, 0, 0, 1)'}} blurRadius={54}>
          <View style={{alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', height: 32, marginVertical: 20}}>
            <View style={{width: 48, height: 48, marginLeft: 10}}></View>
            <Text style={{fontFamily: 'Roboto', color: 'white', fontSize: 24, textTransform: 'capitalize'}}>{i18n.t('playback-speed')}</Text>
            <TouchableWithoutFeedback onPress={() => this.props.setSpeedModal('closed')}>
              <Image source={close} style={{width: 48, height: 48, marginRight: 10}} />
            </TouchableWithoutFeedback>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <TouchableWithoutFeedback onPress={() => {this.state.speedIndex > 0 && this.changeSpeed(-1)}}>
              <View style={{width: 100, height: 48, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(255, 255, 255, 0.2)', borderBottomLeftRadius: 24, borderBottomRightRadius: 4, borderTopLeftRadius: 24, borderTopRightRadius: 4}}>
                <Text style={{fontFamily: 'Roboto', fontSize: 32, color: this.state.speedIndex === 0 ? '#D3D4D8' : '#FF724C'}}>-</Text>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback>
              <View style={{width: 94, height: 48, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(255, 255, 255, 0.2)', marginHorizontal: 6, borderRadius: 4}}>
                <Text style={{fontFamily: 'Roboto', fontSize: 26, color: '#D3D4D8'}}>{this.state.rates[this.state.speedIndex].toString() + 'x'}</Text>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => {this.state.speedIndex < 7 && this.changeSpeed(1)}}>
              <View style={{width: 100, height: 48, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(255, 255, 255, 0.2)', borderBottomLeftRadius: 4, borderBottomRightRadius: 24, borderTopLeftRadius: 4, borderTopRightRadius: 24}}>
                <Text style={{fontFamily: 'Roboto', fontSize: 32, color: this.state.speedIndex === 7 ? '#D3D4D8' : '#FF724C'}}>+</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </Modal>
    )
  }
}

const mapStateToProps = state => {
  return {
    speedModal: state.speedModal,
    language: state.language
  }
}

const mapDispatchToProps = dispatch => ({
  setSpeedModal: (state) => dispatch(setSpeedModal(state)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Speed)
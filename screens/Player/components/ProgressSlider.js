import TrackPlayer, { ProgressComponent } from 'react-native-track-player'
import React from 'react'
import { View, Dimensions, Text, TouchableWithoutFeedback } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Slider from 'react-native-slider'
import { connect } from 'react-redux'
import { getItem, setItem } from '../../../utils/asyncstorage'
import Helper from '../../utils/Helper'
import { formatTime } from '../utils/functions'

class ProgressSlider extends ProgressComponent {
  constructor(props) {
    super(props)
    this.state = {
      tempPosition: null,
      pressedPosition: null
    }

    this.helper = new Helper({})
  }

  

  handleSlider = async value => {
    TrackPlayer.seekTo(value)

    setTimeout(() => {
      if (this.props.isPlaying === 'playing') {
        TrackPlayer.play()
      }

    }, 1500)

    setTimeout(() => {
      this.setState({ tempPosition: null })
    }, 1500)

    const userId = await getItem('userId')

    const body = {
      event_type: 'skip',
      content_type: 'episode',
      content_id: this.props.currentTrack.source_id,
      date: new Date(),
      user_id: userId,
      progress: this.state.position,
      progress_to: value
    }

    this.helper.storeEvent(body)
  }

  handlePress = async e => {
    const pressedPosition = e.nativeEvent.locationX
    const positionOnSlider = pressedPosition + 20

    const window = Dimensions.get('window')
    const endOfSlider = window.width - 20

    const positionRate = this.props.currentTrack.duration * positionOnSlider / endOfSlider
    this.setState({ pressedPosition: positionRate })
    
    setTimeout(() => {
      this.setState({ pressedPosition: null })
    }, 1500)
    
    TrackPlayer.seekTo(positionRate)

    const userId = await getItem('userId')

    const body = {
      event_type: 'skip',
      content_type: 'episode',
      content_id: this.props.currentTrack.source_id,
      date: new Date(),
      user_id: userId,
      progress: this.state.position,
      progress_to: positionRate
    }

    this.helper.storeEvent(body)
  }

  render() {
    const window = Dimensions.get('window')
    const { position, tempPosition, duration } = this.state
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <TouchableWithoutFeedback onPress={(e) => this.handlePress(e)}>
          <Slider 
            value={this.state.pressedPosition !== null ? this.state.pressedPosition : position > 0 ? position : 0}
            minimumValue={0}
            maximumValue={this.props.currentTrack.duration}
            style={{width: window.width - 40, height: 30, marginHorizontal: 20}}
            customMinimumTrack={(
              <LinearGradient 
                start={{x: 1, y: 1}} 
                end={{x: 0, y: 1}} 
                colors={['#FF9549', '#FE4E4E']} 
                style={{height: 2, width: '100%', borderTopLeftRadius: 2, borderBottomLeftRadius: 2, backgroundColor: 'transparent'}} />
            )}
            maximumTrackTintColor='#A1A4B1'
            thumbTouchSize={{width: 40, height: 40}}
            thumbStyle={{width: 24, height: 24, borderRadius: 9999, backgroundColor: 'white'}}
            trackStyle={{height: 2, borderWidth: 0}}
            onSlidingStart={() => TrackPlayer.pause()}
            onValueChange={(value) => this.setState({ tempPosition: value })}
            onSlidingComplete={(value) => this.handleSlider(value)}
          />
        </TouchableWithoutFeedback>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20}}>
          <Text style={{fontSize: 12, color: '#A1A4B1'}}>
            {tempPosition === null ? formatTime(position === undefined ? 0 : position) : formatTime(tempPosition)}
          </Text>
          <Text style={{textAlign: 'center', fontSize: 12, color: '#A1A4B1'}}>{formatTime(this.props.currentTrack.duration)}</Text>
        </View>
      </View>
    )
  }
  
}

const mapStateToProps = state => {
  return {
    isPlaying: state.isPlaying,
    currentTrack: state.currentTrack
  }
}

const mapDispatchToProps = dispatch => ({
  
})

export default connect(mapStateToProps, mapDispatchToProps)(ProgressSlider)
import React from 'react'
import { Text, View, Image, TouchableWithoutFeedback, Dimensions, ActivityIndicator, Animated } from 'react-native'
import TrackPlayer from 'react-native-track-player'
import play from '../../../assets/big_play.png'
import pause from '../../../assets/big_pause.png'
import fullHeart from '../../../assets/full_show_heart.png'
import emptyHeart from '../../../assets/empty_show_heart.png'
import skipforward from '../../../assets/skipforward.png'
import skipback from '../../../assets/skipback.png'
import { setPlayerState, setPlayerView, setSpeedModal } from '../utils/actions'
import { connect } from 'react-redux'
import ProgressSlider from './ProgressSlider'
import Speed from './Speed'
import { getItem, setItem } from '../../../utils/asyncstorage'
import Helper from '../../utils/Helper'
import { storeSavedEpisodes } from '../../Library/utils/actions'
import { storeDownloadedIds } from '../../Library/utils/actions'
import { UnitOfWork } from '../../../App'
import { POINT_EVENTS } from '../../../models/point_event'
import { handleShowPoint } from '../../../utils/showPointUtil'

class Controls extends TrackPlayer.ProgressComponent {
  constructor(props) {
    super(props)

    this.state = {
      speed: '1x',
      isPlaying: false,
      isSaved: null,
      scale: new Animated.Value(0)
    }

    this.helper = new Helper({
      setItem: setItem,
      getItem: getItem
    })

  }

  onPlay = async () => {
    this.state.position >= this.props.currentTrack.duration && TrackPlayer.seekTo(0)
    await TrackPlayer.play()
    this.state.position >= this.props.currentTrack.duration ? setTimeout(() => {
      this.props.setPlayerState('playing')
    }, 1) : this.props.setPlayerState('playing')
    const userId = await getItem('userId')

    const body = {
      event_type: 'play',
      content_type: 'episode',
      content_id: this.props.currentTrack.source_id,
      date: new Date(),
      user_id: userId,
      progress: this.state.position
    }

    this.helper.storeEvent(body)

  }

  onPause = async () => {
    TrackPlayer.pause()
    this.props.setPlayerState('notPlaying')
    const userId = await getItem('userId')

    const body = {
      event_type: 'pause',
      content_type: 'episode',
      content_id: this.props.currentTrack.source_id,
      date: new Date(),
      user_id: userId,
      progress: this.state.position
    }

    this.helper.storeEvent(body)

  }

  skipForward = async () => {
    
    if (this.state.position + 30 < this.props.currentTrack.duration) {
      TrackPlayer.seekTo(this.state.position + 30)
    } else {
      TrackPlayer.seekTo(this.props.currentTrack.duration)
    }
    const userId = await getItem('userId')

    const body = {
      event_type: 'skip',
      content_type: 'episode',
      content_id: this.props.currentTrack.source_id,
      date: new Date(),
      user_id: userId,
      progress: this.state.position,
      progress_to: this.state.position + 30 < this.props.currentTrack.duration ? this.state.position + 30 : this.props.currentTrack.duration
    }

    this.helper.storeEvent(body)

  }

  skipBack = async () => {

    if (this.state.position - 15 > 0) {
      TrackPlayer.seekTo(this.state.position - 15)
    } else {
      TrackPlayer.seekTo(0)
    }
    const userId = await getItem('userId')

    const body = {
      event_type: 'skip',
      content_type: 'episode',
      content_id: this.props.currentTrack.source_id,
      date: new Date(),
      user_id: userId,
      progress: this.state.position,
      progress_to: this.state.position - 15 > 0 ? this.state.position - 15 : '0'
    }

    this.helper.storeEvent(body)

  }

  displaySpeed = value => {
    this.setState({ speed: value })
  }

  handleEpisode = (id, fileId) => {
    console.log('IDIDID', id, 'fileIDIDID', fileId)
    this.setState({ isSaved: !this.state.isSaved })
    
    this.helper = new Helper({
      savedEpisodes: this.props.savedEpisodes,
      downloadedIds: this.props.downloadedIds,
      storeSavedEpisodes: this.props.storeSavedEpisodes,
      storeDownloadedIds: this.props.storeDownloadedIds,
      connectionType: this.props.connectionType
    })

    this.helper.handleEpisode(id, fileId)
    // .then(res => {
    //   if (res === 'saved' && this.state.isSaved !== true) {
    //     this.setState({
    //       isSaved: true
    //     })
    //   } else if (res === 'removed' && this.state.isSaved === true) {
    //     this.setState({
    //       isSaved: false
    //     })
    //   }
    // })
  }

  handleControl = () => {
    this.setState({ isPlaying: !this.state.isPlaying }) 
    this.props.isPlaying === 'playing' ? this.onPause() : this.onPlay()
  }

  animateHeart = () => {
    Animated.spring(this.state.scale, {
      toValue: 2,
      friction: 3,
    }).start(async () => {
      this.state.scale.setValue(0)
      if(this.state.isSaved === true){
        const userId = await getItem('userId');
        handleShowPoint();
        await UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.Favourite);
      }
    })
  }

  render() {
    playerProgressRun('control',this.props.currentTrack.duration)
    
    const window = Dimensions.get('window')

    if (this.props.isPlaying === 'playing' && this.state.isPlaying !== true) {
      this.setState({
        isPlaying: true
      })
    } else if (this.props.isPlaying === 'notPlaying' && this.state.isPlaying !== false) {
      this.setState({
        isPlaying: false
      })
    }

    if (this.state.isSaved === null) {
      if ((this.props.savedEpisodes.find( ({ source_id }) => source_id === this.props.currentTrack.source_id ) !== undefined) && this.state.isSaved !== true) {
        this.setState({
          isSaved: true
        })
      } else if ((this.props.savedEpisodes.find( ({ source_id }) => source_id === this.props.currentTrack.source_id ) === undefined) && this.state.isSaved !== false) {
        this.setState({
          isSaved: false
        })
      }
    }

    if (this.state.position >= this.props.currentTrack.duration && this.props.isPlaying !== 'notPlaying') {
      this.props.setPlayerState('notPlaying')
    }

    const bouncyHeart = this.state.scale.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [1, .6, 1]
    })

    return (
      <View style={{zIndex: 1, position: 'absolute', bottom: 0, left: 0, right: 0, paddingBottom: 30, height: window.height * 0.3, backgroundColor: 'rgba(0, 0, 0, 0.4)'}} blurRadius={80}>
        <View style={{position: 'absolute', bottom: 0, left: 0, right: 0, backgroundColor: 'rgba(0, 0, 0, 0.4)', paddingBottom: 10, height: window.height * 0.3}}></View>
        <ProgressSlider />
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
          <TouchableWithoutFeedback onPress={() => this.props.setSpeedModal('opened')}>
            <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 18, width: 48, height: 48}}>
              <Text style={{fontSize: 16, color: '#D3D4D8'}}>{this.state.speed}</Text>
            </View>
          </TouchableWithoutFeedback>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <TouchableWithoutFeedback onPress={this.skipBack}>
            <Image source={skipback} style={{width: 48, height: 48}} />
            </TouchableWithoutFeedback>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center', width: 120, height: 120}}>
            <TouchableWithoutFeedback onPress={this.handleControl}>
              {
                (this.state.bufferedPosition < this.state.position + 10 && this.state.position < this.props.currentTrack.duration) ?
                <ActivityIndicator size="large" color="#FF724C" />
                :
                <Image source={this.state.isPlaying === true ? pause : play} style={{width: 100, height: 100}} />
              }
            </TouchableWithoutFeedback>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <TouchableWithoutFeedback onPress={this.skipForward}>
              <Image source={skipforward} style={{width: 48, height: 48}} />
            </TouchableWithoutFeedback>
          </View>
          <TouchableWithoutFeedback onPress={() => {this.handleEpisode(this.props.currentTrack.source_id, this.props.currentTrack.file_id); this.animateHeart()}}>
            <Animated.View style={{justifyContent: 'center', alignItems: 'center', marginLeft: 18, width: 48, height: 48, transform: [{ scale: bouncyHeart }]}}>
              <Image source={this.state.isSaved === true ? fullHeart : emptyHeart} style={{width: 48, height: 48}} />
            </Animated.View>
          </TouchableWithoutFeedback>
        </View>
        <Speed syncSpeed={(value) => this.displaySpeed(value)} />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentTrack: state.currentTrack,
    playerView: state.playerView,
    isPlaying: state.isPlaying,
    showData: state.showData,
    savedEpisodes: state.savedEpisodes,
    downloadedIds: state.downloadedIds,
    connectionType: state.connectionType
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  setSpeedModal: (state) => dispatch(setSpeedModal(state)),
  storeSavedEpisodes: (episodes) => dispatch(storeSavedEpisodes(episodes)),
  storeDownloadedIds: (idString) => dispatch(storeDownloadedIds(idString))
})

export default connect(mapStateToProps, mapDispatchToProps)(Controls)
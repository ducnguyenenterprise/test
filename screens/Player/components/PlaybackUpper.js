import React from 'react'
import { View, Image, ScrollView, Dimensions, TouchableWithoutFeedback, Text } from 'react-native'
import TrackPlayer from 'react-native-track-player'
import { setPlayerState, setPlayerView } from '../../Player/utils/actions'
import { connect } from 'react-redux'
import { setActiveScreen } from '../../utils/actions'
import Swiper from 'react-native-swiper'
import { Heading3light, AuthorLight } from '../../../utils/textMixins'
import { transformShowDescription } from '../../Player/utils/functions'
import Api from '../../../utils/Api'
import Helper from '../../utils/Helper'
import { storeShow } from '../../Show/utils/actions'
import FastImage from 'react-native-fast-image'

class EpisodeHeader extends TrackPlayer.ProgressComponent {
  constructor(props) {
    super(props)
    this.state = {
      showTitle: '',
      epId: ''
    }

    this.api = new Api
  }

  fetchTitle = () => {
    this.setState({
      showTitle: '...',
      epId: this.props.currentTrack.source_id
    })

    this.api.fetchShowTitle(this.props.currentTrack.show_source_id)
    .then(res => {
      if (res.success === true) {
        this.setState({ showTitle: res.title})
      }
    })
  }

  openShow = id => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      api: this.api,
      navigation: this.props.navigation,
    })

    this.helper.openShow(id)
  }

  render() {
    const window = Dimensions.get('window')

    if (this.state.epId !== this.props.currentTrack.source_id && this.state.showTitle !== '...') {
      this.fetchTitle()
    }
    
    return (
      <View style={{backgroundColor: 'rgba(0, 0, 0, 0.4)'}}>
        <Swiper
          paginationStyle={{bottom: 10}}
          dotStyle={{height: 6.5, width: 6.5, marginLeft: 6.5, marginRight: 6.5}}
          activeDotStyle={{height: 6.5, width: 6.5, marginLeft: 6.5, marginRight: 6.5}}
          showsButtons={false} 
          loop={false}
          dotColor='rgba(255, 255, 255, 0.4)' 
          activeDotColor='rgba(255, 255, 255, 0.9)'
          style={{height: window.height * 0.4 + window.height * 0.1 - 24, marginTop: window.height * 0.1 + 100 - 24}}
        >
          <View style={{width: window.width, alignItems: 'center', justifyContent: 'center'}}>
            <FastImage source={{uri: this.props.currentTrack.thumbnail}} style={{width: window.height * 0.4, height: window.height * 0.4}} />
          </View>
          <View style={{height: 14 * 18}}>
            <ScrollView contentContainerStyle={{width: window.width - 40, alignItems: 'flex-start', marginHorizontal: 20, paddingBottom: 50}}>
              {transformShowDescription(this.props.currentTrack.description)}
            </ScrollView>
          </View>
        </Swiper>
        <View style={{alignItems: 'center', marginHorizontal: 20, width: window.width - 40}}>
          <Heading3light lines={1}>{this.props.currentTrack.title}</Heading3light>
          <TouchableWithoutFeedback onPress={() => this.openShow(this.props.currentTrack.show_source_id)}>
            <Text style={{fontFamily: 'Roboto-Regular', marginBottom: 8, color: '#FF724C', padding: 10, fontSize: 16, textAlign: 'center'}}>{this.state.showTitle}</Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentTrack: state.currentTrack,
    playerView: state.playerView,
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  setActiveScreen: (activeScreen) => dispatch(setActiveScreen(activeScreen)),
  storeShow: (showData) => dispatch(storeShow(showData))
})

export default connect(mapStateToProps, mapDispatchToProps)(EpisodeHeader)
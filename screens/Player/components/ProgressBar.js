import TrackPlayer from 'react-native-track-player'
import React from 'react'
import { View } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { setItem } from '../../../utils/asyncstorage'
import { connect } from 'react-redux'

class ProgressBar extends TrackPlayer.ProgressComponent {
  constructor(props) {
    super(props)
  }
  componentDidUpdate(prevProps, prevState) {    
    if (prevState.position !== this.state.position) {
      setItem('lastPlayedAudioPosition', JSON.stringify(this.state.position))
      setItem(this.props.currentTrack.id, JSON.stringify(this.state.position))
    }
  }

  render() {
    const { position, duration } = this.state
    let progressRate = (position > 0 && duration > 0) ? position / duration : 0
    let progressPercent = (progressRate * 100) + '%'

    return (
      <View style={this.props.style}>
        <LinearGradient 
          start={{x: 1, y: 1}} 
          end={{x: 0, y: 1}} 
          colors={['#FF9549', '#FE4E4E']}  
          style={{width: progressPercent, height: 2, position: 'absolute', zIndex: 1}}>
        </LinearGradient>
      </View>
    )
  }
  
}

const mapStateToProps = state => {
  return {
    currentTrack: state.currentTrack
  }
}

/*const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track))
})*/

export default connect(mapStateToProps/*, mapDispatchToProps*/)(ProgressBar)
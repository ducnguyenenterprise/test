import React from 'react'
import { View, Image, TouchableWithoutFeedback, Dimensions, InteractionManager, Platform } from 'react-native'
import TrackPlayer from 'react-native-track-player'
import play from '../../../assets/mini_play.png'
import pause from '../../../assets/mini_pause.png'
import ProgressBar from './ProgressBar'
import { setPlayerState, setPlayerView } from '../utils/actions'
import { storeShow } from '../../Show/utils/actions'
import { connect } from 'react-redux'
import { EpisodeTitleLight, TimeLight } from '../../../utils/textMixins'
import Api from '../../../utils/Api'
import { formatTime } from '../utils/functions'
import NavigationService from '../../../NavigationService'
import ItemAnimator from '../../../components/ItemAnimator'

class MiniPlayer extends TrackPlayer.ProgressComponent {
  constructor(props) {
    super(props)

    this.state = {
      isLoaded: false
    }
    
    this.api = new Api
  }
  
  onPlay = async () => {
    this.state.position >= this.props.currentTrack.duration && TrackPlayer.seekTo(0)
    await TrackPlayer.play()
    this.state.position >= this.props.currentTrack.duration ? setTimeout(() => {
      this.props.setPlayerState('playing')
    }, 1) : this.props.setPlayerState('playing')
  }

  onPause = async () => {
    await TrackPlayer.pause()
    this.props.setPlayerState('notPlaying')
  }

  openPlayerScreen = showId => {
    NavigationService.navigate('PlaybackScreen', { fromMini: true, miniPosition: this.props.playerView })
    setTimeout(() => {
      this.props.setPlayerView('hidden')
    }, 200)
    
  }

  render() {
    playerProgressRun('mini',this.props.currentTrack.duration)

    const window = Dimensions.get('window')
    const { position } = this.state

    if (this.state.position >= this.props.currentTrack.duration && this.props.isPlaying !== 'notPlaying') {
      this.props.setPlayerState('notPlaying')
    }
    
    return this.props.currentTrack ? (
      <TouchableWithoutFeedback onPress={() => this.openPlayerScreen()} style={{height: 50}}>
        <View style={{position: 'absolute', bottom: this.props.bottom, left: 0, right: 0, height: 50}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Image source={{uri: this.props.currentTrack.artwork}} style={{width: 50, height: 50}}/>
            <View style={{backgroundColor: '#232735', flexDirection: 'row', flex: 1, justifyContent: 'space-between'}}>
              <View style={{paddingLeft: 10, justifyContent: 'center', maxWidth: window.width - 50 - 50 - 10}}>
                <View style={{height: 6}}></View>
                <EpisodeTitleLight lines={1}>{this.props.currentTrack.title}</EpisodeTitleLight>
                <View style={{height: 2}}></View>
                <TimeLight>{this.state.position ? formatTime(position) : '00:00'} / {formatTime(this.props.currentTrack.duration)}</TimeLight>
              </View>
              <TouchableWithoutFeedback onPress={this.props.isPlaying === 'playing' ? this.onPause : this.onPlay}>
                <View style={{width: 50, height: 50, alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={this.props.isPlaying === 'playing' ? pause : play} style={{width: 32, height: 32}}/>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <ProgressBar style={{width: window.width, height: 2, backgroundColor: '#232735'}} />
        </View>
      </TouchableWithoutFeedback>
    ) : null
  }
}

const mapStateToProps = state => {
  return {
    currentTrack: state.currentTrack,
    playerView: state.playerView,
    isPlaying: state.isPlaying
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  storeShow: (showData) => dispatch(storeShow(showData))
})

export default connect(mapStateToProps, mapDispatchToProps)(MiniPlayer)
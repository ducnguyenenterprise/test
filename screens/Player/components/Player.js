import React from 'react'
import MiniPlayer from './MiniPlayer'
import { connect } from 'react-redux'
import { View, Animated, Dimensions, Platform } from 'react-native'

class Player extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showFullScreen: false,
      type: null
    }

    this.moveAnimation = new Animated.Value(Dimensions.get('window').height)
  }

  animate = () => {
    Animated.spring(this.moveAnimation, {
      toValue: this.state.showFullScreen === true ? -Dimensions.get('window').height : Dimensions.get('window').height,
      speed: 10,
      bounciness: 4,
      useNativeDriver: true
    }).start()
  }

  triggerFullScreen = () => {
    this.animate()
    this.setState({
      showFullScreen: !this.state.showFullScreen,
      type: this.props.playerView
    })
  }

  render() {
    const { playerView } = this.props
    let toReturn = <MiniPlayer bottom={(Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 54 + 34 : 54} />

    if (playerView === 'miniUp') {
      toReturn = (
        <MiniPlayer bottom={(Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 54 + 34 : 54} />
      )
    } else if (playerView === 'miniDown') {
      toReturn = (
        <>
          <MiniPlayer bottom={(Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 34 : 0} />
          {
            ((Dimensions.get('window').height >= 812 && Platform.OS === 'ios') && this.props.playerView === 'miniDown') && <View style={{height: 34, width: window.width, backgroundColor: '#232735'}}></View>
          }
        </>
      )
    } else if (playerView === 'hidden') {
      toReturn = null
    }
    
    return (
      <>
        {toReturn}
      </>
    )
   
  }
}

const mapStateToProps = state => {
  return {
    playerView: state.playerView
  }
}

export default connect(mapStateToProps)(Player)
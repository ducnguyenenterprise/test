import { SET_PLAYER_VIEW, SET_PLAYER_STATE, ADD_TRACK, SET_SPEED_MODAL, SET_SHOW_TITLE } from './types'

export const setPlayerView = playerView => {
  return {
    type: SET_PLAYER_VIEW,
    playerView
  }
}

export const setPlayerState = isPlaying => {
  return {
    type: SET_PLAYER_STATE,
    isPlaying
  }
}

export const addTrack = track => {
  return {
    type: ADD_TRACK,
    track
  }
}

export const setSpeedModal = state => {
  return {
    type: SET_SPEED_MODAL,
    state
  }
}

export const setShowTitle = title => {
  return {
    type: SET_SHOW_TITLE,
    title
  }
}
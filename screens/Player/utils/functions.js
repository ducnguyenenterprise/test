import React from 'react'
import { Text } from 'react-native'
import { Body1, Body1light, Body2light } from '../../../utils/textMixins'
import HTML from 'react-native-render-html'

const style = {fontFamily: 'Roboto-Regular', fontSize: 16, lineHeight: 18, color: '#D3D4D8'}

const tags = {
  p: style,
  ul: style,
  a: style,
  ol: style,
  span: style,
  h1: style,
  h2: style,
  h3: style,
  h4: style,
  h5: style,
  h6: style,
  strong: style,
  
}

const isHtml = (description) => {
  return (description.includes('<p>') || description.includes('<div>') || description.includes('<P>') || description.includes('<em>'))
}

const regex = /(<([^>]+)>)/ig

export const transformEpisodeDescription = description => {
  if (description && isHtml(description)) {
    return (
      <HTML 
        html={description} 
        tagsStyles={tags}
        listsPrefixesRenderers={{
          ul: () => {
            return (
              <Text style={{fontFamily: 'Roboto-Regular', fontSize: 16, lineHeight: 18, color: '#D3D4D8'}}>•</Text>
            )
          }
        }}
      />
    )
  } else {
    return (
      <Body1light>{description}</Body1light>
    )
  }
}

export const transformMiniEpisodeDescription = description => {
  if (description && isHtml(description)) {
    const descriptionString = description.replace(regex, ' ').substring(1, description.length).replace(/ +(?= )/g,'').replace(/&nbsp;/gi, '').replace(/&amp;/gi, '')
    return (
      <Body2light styles={{paddingHorizontal: 20, paddingBottom: 10}} lines={2}>{descriptionString}</Body2light>
    )
  } else {
    return (
      <Body2light styles={{paddingHorizontal: 20, paddingBottom: 10}} lines={2}>{description}</Body2light>
    )
  }
}

export const transformDescriptionOnHomeScreen = description => {
  if (description && isHtml(description)) {
    const descriptionString = description.replace(regex, ' ').substring(1, description.length).replace(/ +(?= )/g,'').replace(/&nbsp;/gi, '').replace(/&amp;/gi, '')
    return (
      <Body1 styles={{marginTop: 6}} lines={4}>{descriptionString}</Body1>
    )
  } else {
    return (
      <Body1 styles={{marginTop: 6}} lines={4}>{description}</Body1>
    )
  }
}

export const transformDescriptionOnSearchScreen = description => {
  if (description && isHtml(description)) {
    const descriptionString = description.replace(regex, ' ').substring(1, description.length).replace(/ +(?= )/g,'').replace(/&nbsp;/gi, '').replace(/&amp;/gi, '')
    return (
      <Body1 styles={{marginTop: 6}} lines={3}>{descriptionString}</Body1>
    )
  } else {
    return (
      <Body1 styles={{marginTop: 6}} lines={3}>{description}</Body1>
    )
  }
}

export const transformShowDescription = description => {
  // return (
  //   <Body1light>{description}</Body1light>
  // )
  if (description && isHtml(description)) {
    return (
      <HTML 
        html={description} 
        tagsStyles={tags}
        listsPrefixesRenderers={{
          ul: () => {
            return (
              <Text style={{fontFamily: 'Roboto-Regular', fontSize: 16, lineHeight: 18, color: '#D3D4D8'}}>•</Text>
            )
          }
        }}
      />
    )
  } else {
    return (
      <Body1light>{description}</Body1light>
    )
  }
}

export const formatTime = secs => {
  const secNum = parseInt(secs, 10)
  const hours   = Math.floor(secNum / 3600)
  const minutes = Math.floor(secNum / 60) % 60
  const seconds = secNum % 60

  return [hours,minutes,seconds]
  .map(v => v < 10 ? '0' + v : v)
  .filter((v,i) => v !== '00' || i > 0)
  .join(':')
}
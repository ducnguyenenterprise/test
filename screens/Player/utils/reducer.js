import { SET_PLAYER_STATE, SET_PLAYER_VIEW, ADD_TRACK, SET_SPEED_MODAL, SET_SHOW_TITLE } from './types'

export const playerView =  (state = {}, action) => {
  switch (action.type) {
    case SET_PLAYER_VIEW:
     return action.playerView
    default:
      return state
  }
}

export const isPlaying =  (state = {}, action) => {
  switch (action.type) {
    case SET_PLAYER_STATE:
      return action.isPlaying
    default:
      return state
  }
}

export const addTrack = (state = {}, action) => {
  switch (action.type) {
    case ADD_TRACK:
      return action.track
    default:
      return state
  }
}

export const speedModal = (state = {}, action) => {
  switch (action.type) {
    case SET_SPEED_MODAL:
      return action.state
    default:
      return state
  }
}

export const showTitle = (state = {}, action) => {
  switch (action.type) {
    case SET_SHOW_TITLE:
      return action.title
    default:
      return state
  }
}
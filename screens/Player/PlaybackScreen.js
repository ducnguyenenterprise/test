import React from 'react'
import { View, Image, StatusBar, Dimensions, ImageBackground, BackHandler, TouchableWithoutFeedback, Platform, InteractionManager } from 'react-native'
import { setPlayerView } from '../Player/utils/actions'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import downArrow from '../../assets/down_arrow.png'
import share from '../../assets/share.png'
import PlaybackUpper from './components/PlaybackUpper'
import Controls from './components/Controls'
import Share from 'react-native-share'
import NavigationService from '../../NavigationService'
import i18n from '../../utils/i18n'
import RNExitApp from 'react-native-exit-app'
import { UnitOfWork } from '../../App'
import { POINT_EVENTS } from '../../models/point_event'
import PointAnimatedItem from '../Common/PointAnimatedItem'
import { handleShowPoint } from '../../utils/showPointUtil'

class PlaybackScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      interactionsComplete: false
    }
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      BackHandler.addEventListener('hardwareBackPress', () => {
        if (this.props.navigation.isFocused()) {
          const fromMini = this.props.navigation.getParam('fromMini', false)
          const miniPosition = this.props.navigation.getParam('miniPosition', 'miniUp')
          if (fromMini === true) {
            this.props.setPlayerView(miniPosition)
            NavigationService.goBack()
            BackHandler.removeEventListener('hardwareBackPress')
            return true
          } else {
            NavigationService.goBack()
            BackHandler.removeEventListener('hardwareBackPress')
            return true
          }
        } else {
          RNExitApp.exitApp()
        }
      })

      this.setState({
        interactionsComplete: true
      })
    })
  }

  goBack = () => {
    const fromMini = this.props.navigation.getParam('fromMini', false)
    if (fromMini === true) {
      NavigationService.goBack()
      setTimeout(() => {
        const miniPosition = this.props.navigation.getParam('miniPosition', 'miniUp')
        this.props.setPlayerView(miniPosition)
      }, 30)
      InteractionManager.runAfterInteractions(() => {
        this.setState({
          interactionsComplete: false
        })
      })
    } else {      
      NavigationService.goBack()
    }
  }

  share = () => {
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    const link = Platform.OS === 'ios' ?  'https://apps.apple.com/gb/app/waves-podcast-player/id1492378044' : 'https://play.google.com/store/apps/details?id=com.waves8.app'
    const shareOptions = {
      title: 'Share via',
      message: `${i18n.t('share-1')} ${this.props.currentTrack.title} ${i18n.t('share-2')} ${link}${i18n.t('share-3')}`,
      // url: 'https://waves8.com',
      // social: Share.Social.WHATSAPP,
      
    }
    handleShowPoint()
    Share.open(shareOptions).then(async openReturn => {
      if(openReturn.dismissedAction){
        const userId = await getItem('userId');
        await UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.Share);
      }
    });
  }

  render() {
    const window = Dimensions.get('window')

    return (
      (this.state.interactionsComplete === true && Object.keys(this.props.currentTrack).length > 0) ?
      <>
        <View style={{height: 48, position: 'absolute', flexDirection: 'row', justifyContent: 'space-between', width: window.width - 20, top: 40, marginHorizontal: 10, zIndex: 200}}>
          <TouchableWithoutFeedback onPress={() => this.goBack()}>
            <Image source={downArrow} style={{height: 48, width: 48, zIndex: 300}} />
          </TouchableWithoutFeedback>
          <View style={{flexDirection: 'row'}}>
            <PointAnimatedItem></PointAnimatedItem>
            <TouchableWithoutFeedback onPress={this.share}>
              <Image source={share} style={{height: 48, width: 48}} />
            </TouchableWithoutFeedback>
          </View>
        </View>
        <View style={{bottom: this.props.fromMini === true ? window.height * 0.3 : window.height * 0.3 - 20, width: window.width, height: window.height * 0.7, justifyContent: 'flex-start'}}>
          <View style={{height: 24}}>
            <StatusBar barStyle='light-content'/>
          </View>
          <ImageBackground 
            style={{flex: 1, marginTop: -100, height: window.height * 1.4, width: window.width * 1.4, marginLeft: -window.width * 0.2}} 
            source={{uri: this.props.currentTrack.thumbnail}} 
            resizeMode='cover' 
            blurRadius={2}
          />
        </View>
        <LinearGradient 
          colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.63)', 'rgba(0, 0, 0, 0.85)']}
          locations={[0, 0.1094, 1]}
          style={{position: 'absolute', bottom: 0, right: 0, left: 0, width: window.width, height: window.height * 0.6}}
        />
        <View style={{position: 'absolute', bottom: window.height * 0.3, backgroundColor: 'rgba(0, 0, 0, 0.4)', zIndex: 100}}>
          <PlaybackUpper fromMini={this.props.fromMini} navigation={this.props.navigation} />
        </View>
        <Controls />
      </>
      :
      <View style={{ backgroundColor: 'black' }}></View>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentTrack: state.currentTrack,
    playerView: state.playerView,
    language: state.language
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
})

export default connect(mapStateToProps, mapDispatchToProps)(PlaybackScreen)
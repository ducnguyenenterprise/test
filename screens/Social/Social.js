import React from 'react'
import { View, Platform, StatusBar, SafeAreaView, Text, Dimensions } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { WebView } from 'react-native-webview'

class SocialScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 44 : 32}}>
          <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
        </LinearGradient>
        <SafeAreaView style={{flex: 1}}>
          {
            Platform.OS === 'ios' ?
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 150}}>
              <Text style={{fontFamily: 'Roboto-Regular', fontSize: 20, textTransform: 'uppercase', color: 'rgb(119, 120, 135)'}}>Coming soon</Text>
            </View>
            :
            <WebView
            source={{uri: 'https://www.facebook.com/WavesVietnam'}}
          />
          }
        </SafeAreaView>
      </View>
    )
  }
}

export default SocialScreen
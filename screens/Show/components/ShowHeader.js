import React from 'react'
import { View, Image, ScrollView, Dimensions, TouchableWithoutFeedback, Text } from 'react-native'
import i18n from '../../../utils/i18n'
import TrackPlayer from 'react-native-track-player'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import Swiper from 'react-native-swiper'
import { Heading3light, AuthorLight } from '../../../utils/textMixins'
import { transformShowDescription } from '../../Player/utils/functions'
import followButton from '../../../assets/follow_button.png'
import followViet from '../../../assets/follow_vietnamese.png'
import Api from '../../../utils/Api'
import { storeSavedShows } from '../../Library/utils/actions'
import FastImage from 'react-native-fast-image'
import { getItem, setItem } from '../../../utils/asyncstorage'
import { UnitOfWork } from '../../../App'
import { POINT_EVENTS } from '../../../models/point_event'
import { handleShowPoint } from '../../../utils/showPointUtil'

class ShowHeader extends TrackPlayer.ProgressComponent {
  constructor(props) {
    super(props)
    this.state = {
      isSaved: null
    }

    this.api = new Api
  }

  handleShow = async (id) => {
    const userId = await getItem('userId')
    this.setState({ isSaved: !this.state.isSaved })
    if (this.props.savedShows.find( ({ source_id }) => source_id === id ) !== undefined) {
      this.api.removeFromLibrary(userId, id)
      .then(library => {
        // if library.success === true && library.removed === true
        this.props.storeSavedShows(library.data.shows)

        if (this.state.isSaved !== false) {
          this.setState({
            isSaved: false
          })
        }
      })
    } else {
      this.api.addToLibrary(userId, id, 'show')
      .then(library => {
        // if library.success === true && library.inserted === true
        this.props.storeSavedShows(library.data.shows)
        
        handleShowPoint()
        UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.Favourite);
        if (this.state.isSaved !== true) {
          this.setState({
            isSaved: true
          })
        }
      })
    }
    
  }

  render() {
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'

    if (this.state.isSaved === null) {
      if ((this.props.savedShows.find( ({ source_id }) => source_id === this.props.showData.source_id ) !== undefined) && this.state.isSaved !== true) {
        this.setState({
          isSaved: true
        })
      } else if ((this.props.savedShows.find( ({ source_id }) => source_id === this.props.showData.source_id ) === undefined) && this.state.isSaved !== false) {
        this.setState({
          isSaved: false
        })
      }
    }

    return (
      <View>
        <LinearGradient 
          colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.63)', 'rgba(0, 0, 0, 0.63)']}
          locations={[0.6, 0.6 + 0.1094, 1]}
          style={{zIndex: 0, width: window.width}}>
        <Swiper
          paginationStyle={{bottom: 12}}
          dotStyle={{height: 6.5, width: 6.5, marginLeft: 6.5, marginRight: 6.5}}
          activeDotStyle={{height: 6.5, width: 6.5, marginLeft: 6.5, marginRight: 6.5}}
          showsButtons={false} 
          dotColor='rgba(255, 255, 255, 0.4)' 
          activeDotColor='rgba(255, 255, 255, 0.9)'
          style={{height: 226 - 26 + 64 + 16, marginTop: 10}}
        >
          <View style={{width: window.width, alignItems: 'center', justifyContent: 'center'}}>
            <FastImage source={{uri: this.props.showData.thumbnail}} style={{width: 226, height: 226}} />
          </View>
          <View style={{height: 220, marginTop: 6}}>
            <ScrollView nestedScrollEnabled={true} contentContainerStyle={{flexGrow: 1, width: window.width - 40, alignItems: 'flex-start', marginHorizontal: 20}}>
              {transformShowDescription(this.props.showData.description)}
            </ScrollView>
          </View>
        </Swiper>
        <View style={{alignItems: 'center', marginHorizontal: 20}}>
          <Heading3light styles={{marginBottom: 10, textAlign: 'center'}}>{this.props.showData.title}</Heading3light>
          <AuthorLight styles={{marginBottom: 18}}>{this.props.showData.author}</AuthorLight>
          {
            this.state.isSaved === true
            ?
            <TouchableWithoutFeedback onPress={() => this.handleShow(this.props.showData.source_id)}>
              <View style={{width: 138, height: 32, backgroundColor: 'rgba(240, 241, 243, 0.2)', borderRadius: 24, alignItems: 'center', justifyContent: 'center', marginBottom: 23}}>
                <Text style={{fontFamily: 'Roboto', fontSize: 18, letterSpacing: 0.5, color: 'rgba(240, 241, 243, 0.4)'}}>{i18n.t('following button')}</Text>
              </View>
            </TouchableWithoutFeedback>
            :
            <TouchableWithoutFeedback onPress={() => this.handleShow(this.props.showData.source_id)} style={{width: this.props.language === 'English' ? 90 : 112, height: 32}}>
              <Image source={this.props.language === 'English' ? followButton : followViet} style={{width: this.props.language === 'English' ? 90 : 112, height: 32, marginBottom: 23}}/>
            </TouchableWithoutFeedback>
          }
        </View>
        </LinearGradient>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    showData: state.showData,
    savedShows: state.savedShows,
    language: state.language
  }
}

const mapDispatchToProps = dispatch => ({
  storeSavedShows: (shows) => dispatch(storeSavedShows(shows))
})

export default connect(mapStateToProps, mapDispatchToProps)(ShowHeader)
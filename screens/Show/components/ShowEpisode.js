import React from 'react'
import { View, Dimensions, TouchableWithoutFeedback, Image, ImageBackground, Animated } from 'react-native'
import play from '../../../assets/episode_play.png'
import fullHeart from '../../../assets/full_show_heart.png'
import emptyHeart from '../../../assets/empty_show_heart.png'
import { connect } from 'react-redux'
import { EpisodeTitleLight, TimeLight } from '../../../utils/textMixins'
import TrackPlayer from 'react-native-track-player'
import { getItem, setItem } from '../../../utils/asyncstorage'
import { setPlayerState, setPlayerView, addTrack } from '../../Player/utils/actions'
import { transformMiniEpisodeDescription, formatTime } from '../../Player/utils/functions'
import { setActiveScreen } from '../../utils/actions'
import RNFS from 'react-native-fs'
import Api from '../../../utils/Api'
import { storeSavedEpisodes, storeDownloadedIds } from '../../Library/utils/actions'
import { withNavigation } from 'react-navigation'
import Helper from '../../utils/Helper'
import i18n from '../../../utils/i18n'
import Toast from 'react-native-simple-toast'
import { UnitOfWork } from '../../../App'
import { POINT_EVENTS } from '../../../models/point_event'
import { handleShowPoint } from '../../../utils/showPointUtil'

class Episode extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSaved: null,
      scale: new Animated.Value(0)
    }
    this.toast = null
    this.api = new Api()
  }

  playEpisode = episode => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      addTrack: this.props.addTrack, 
      setPlayerState: this.props.setPlayerState,
      playerView: 'miniDown'
    })

    this.helper.playEpisode(episode)
    
  }

  openEpisode = async episode => {
    this.props.setPlayerView('hidden')
    if (this.props.downloadedIds.length > 0) {
      if (this.props.downloadedIds.includes(episode.source_id)) {
        episode['isDownloaded'] = true
      } else {
        episode['isDownloaded'] = false
      }
    }
    
    const url = episode.isDownloaded === true ? 'file://' + RNFS.DocumentDirectoryPath + `/music/${episode.source_id}.mp3` : episode.file_id
    const track = {
      id: /*episode.id ? episode.id :*/ episode.source_id,
      url: url,
      title: episode.title,
      artist: episode.author, 
      artwork: episode.thumbnail,
      duration: episode.audio_length,
      show_id: episode.show_id,
      file_id: episode.file_id,
      thumbnail: episode.thumbnail,
      description: episode.description,
      source_id: episode.source_id ? episode.source_id : null,
      show_source_id: episode.show_source_id
    }
    
    this.props.addTrack(track)
    //await TrackPlayer.add(track)
    
    //seek to the saved position if it exists
    let position = 0
    getItem(track.id)
    .then(async res => {
      if (res !== undefined && res !== null) {
        position = JSON.parse(res)
      }
      await TrackPlayer.stop()
      await TrackPlayer.reset()
      await TrackPlayer.add(track)
      position !== null && TrackPlayer.seekTo(position)
      TrackPlayer.play()
    })
        
    this.props.setActiveScreen('Show', { navbar: true })
    this.props.setPlayerState('playing')
    this.props.navigation.navigate('PlaybackScreen', { navbar: false, navigateBackToShowScreen: this.props.navigateBackToShowScreen, fromMini: true, miniPosition: 'miniDown' })
    setItem('lastPlayed', JSON.stringify(track))
  }

  handleEpisode = (id, fileId) => {
    Toast.show(!this.state.isSaved === true ? i18n.t('added to library') : i18n.t('removed from library'))
    this.setState({ isSaved: !this.state.isSaved })
    
    this.helper = new Helper({
      savedEpisodes: this.props.savedEpisodes,
      downloadedIds: this.props.downloadedIds,
      storeSavedEpisodes: this.props.storeSavedEpisodes,
      storeDownloadedIds: this.props.storeDownloadedIds,
      api: this.api,
      connectionType: this.props.connectionType
    })

    this.helper.handleEpisode(id, fileId)
    // .then(res => {
    //   if (res === 'saved' && this.state.isSaved !== true) {
    //     this.setState({
    //       isSaved: true
    //     })
    //   } else if (res === 'removed' && this.state.isSaved === true) {
    //     this.setState({
    //       isSaved: false
    //     })
    //   }
    // })
  }

  animateHeart = () => {
    Animated.spring(this.state.scale, {
      toValue: 2,
      friction: 3,
    }).start(async () => {
      this.state.scale.setValue(0)
      if(this.state.isSaved === true){
        const userId = await getItem('userId');
        handleShowPoint();
        await UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.Favourite);
      }
    })
  }

  render() {
    const window = Dimensions.get('window')
    const { episodeData } = this.props

    if (this.state.isSaved === null) {
      if (this.props.savedEpisodes.find( ({ source_id }) => source_id === this.props.episodeData.source_id ) !== undefined) {
        this.setState({
          isSaved: true
        })
      } else if (this.props.savedEpisodes.find( ({ source_id }) => source_id === this.props.episodeData.source_id ) === undefined) {
        this.setState({
          isSaved: false
        })
      }
    }

    const bouncyHeart = this.state.scale.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [1, .6, 1]
    })

    return (
      <View style={{flex: 1, backgroundColor: this.props.isFirst !== true && 'rgba(0, 0, 0, 0.85)'}}>
        <TouchableWithoutFeedback onPress={() => this.openEpisode(episodeData)}>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginVertical: 10}}>
              <TouchableWithoutFeedback onPress={() => this.playEpisode(episodeData)}>
                <ImageBackground source={{uri: episodeData.thumbnail}} style={{width: 50, height: 50, marginLeft: 20, alignItems: 'center', justifyContent: 'center'}}>
                  <TouchableWithoutFeedback onPress={() => this.playEpisode(episodeData)}>
                    <Image source={play} style={{width: 32, height: 32}} />
                  </TouchableWithoutFeedback>
                </ImageBackground>
              </TouchableWithoutFeedback>
              <View style={{backgroundColor: 'transparent', flexDirection: 'row', flex: 1, justifyContent: 'space-between'}}>
              <View style={{paddingLeft: 10, justifyContent: 'center', maxWidth: window.width - 50 - 66 - 10 - 10}}>
                <EpisodeTitleLight lines={1}>{episodeData.title}</EpisodeTitleLight>
                <TimeLight styles={{paddingTop: 1}}>{episodeData.progress === null ? '00:00' : formatTime(episodeData.progress)} / {formatTime(this.props.episodeData.audio_length)}</TimeLight>
              </View>
              <TouchableWithoutFeedback onPress={() => this.handleEpisode(episodeData.source_id, episodeData.file_id)}>
                <View style={{width: 48 + 12 + 6, height: 48, paddingRight: 12, paddingLeft: 6}}>
                  <TouchableWithoutFeedback onPress={() => {this.handleEpisode(episodeData.source_id, episodeData.file_id); this.animateHeart()}}>
                    <Animated.Image source={this.state.isSaved === true ? fullHeart : emptyHeart} style={{width: 48, height: 48, marginTop: -10, transform: [{ scale: bouncyHeart }]}}/>
                  </TouchableWithoutFeedback>
                </View>
              </TouchableWithoutFeedback>
              </View>
            </View>
            {transformMiniEpisodeDescription(episodeData.description)}
          </View>
        </TouchableWithoutFeedback>
        <View style={{width: window.width, height: 2, backgroundColor: '#232735'}}></View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    playerView: state.playerView,
    isPlaying: state.isPlaying,
    downloadedIds: state.downloadedIds,
    savedEpisodes: state.savedEpisodes,
    connectionType: state.connectionType
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track)),
  setActiveScreen: (activeScreen) => dispatch(setActiveScreen(activeScreen)),
  storeSavedEpisodes: (episodes) => dispatch(storeSavedEpisodes(episodes)),
  storeDownloadedIds: (idString) => dispatch(storeDownloadedIds(idString))
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Episode))
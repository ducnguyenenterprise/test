import { STORE_SHOW } from './types'

export const storeShow = showData => {
  return {
    type: STORE_SHOW,
    showData
  }
}
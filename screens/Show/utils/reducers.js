import { STORE_SHOW } from './types'

export const storeShow =  (state = {}, action) => {
  switch (action.type) {
    case STORE_SHOW:
     return action.showData
    default:
      return state
  }
}
import React from 'react'
import { View, Image, StatusBar, Dimensions, ImageBackground, ActivityIndicator, BackHandler, TouchableWithoutFeedback, FlatList, InteractionManager, Platform, SafeAreaView, Animated } from 'react-native'
import { setPlayerView } from '../Player/utils/actions'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import backArrow from '../../assets/back_arrow.png'
import share from '../../assets/share.png'
import { setActiveScreen } from '../utils/actions'
import ShowEpisode from './components/ShowEpisode'
import ShowHeader from './components/ShowHeader'
import { getItem } from '../../utils/asyncstorage'
import Share from 'react-native-share'
import { storeShow } from './utils/actions'
import i18n from '../../utils/i18n'
import PointAnimatedItem from '../Common/PointAnimatedItem'
import { handleShowPoint } from '../../utils/showPointUtil'
import { POINT_EVENTS } from '../../models/point_event'
import { UnitOfWork } from '../../App'

class ShowScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      interactionsComplete: false
    }

    this.animatedValue = new Animated.Value(0)
  }

  componentDidMount() {

    InteractionManager.runAfterInteractions(() => {
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        if (this.props.navigation.isFocused()) {
          if (this.props.playerView === 'episode') {
            this.props.setPlayerView('miniDown')
            return true
          } else {
            this.props.setActiveScreen('')
            this.props.playerView !== 'hidden' && this.props.setPlayerView('miniUp')
            this.props.navigation.goBack(null)
            this.props.storeShow({})
            return true
          }
        }
      })
  
      this.props.setPlayerView('hidden')
  
      getItem('lastPlayed')
      .then(res => {
        if (res === undefined || res === null) {
          this.props.setPlayerView('hidden')
        } else {
          this.props.setPlayerView('miniDown')
        }
      })
    })

    this.setState({
      interactionsComplete: true
    })
  }

  goBack = () => {
    this.props.navigation.goBack()
    InteractionManager.runAfterInteractions(() => {
      const isNavbar = this.props.navigation.getParam('navbar', false)
      if (this.props.playerView !== 'hidden') {
        this.props.setPlayerView(isNavbar === true ? 'miniUp' : 'miniDown')
      }
      this.props.storeShow({})
    })
  }

  navigateBackToShowScreen = () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.props.navigation.isFocused()) {
        if (this.props.playerView === 'episode') {
          this.props.setPlayerView('miniDown')
          return true
        } else {
          this.props.setActiveScreen('')
          this.props.playerView !== 'hidden' && this.props.setPlayerView('miniUp')
          this.props.navigation.goBack(null)
          return true
        }
      }
    })
  }

  share = () => {
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    const link = Platform.OS === 'ios' ?  'https://apps.apple.com/gb/app/waves-podcast-player/id1492378044' : 'https://play.google.com/store/apps/details?id=com.waves8.app'
    const shareOptions = {
      title: 'Share via',
      message: `${i18n.t('share-1')} ${this.props.showData.title} ${i18n.t('share-2')} ${link}${i18n.t('share-3')}`,
      // url: 'https://waves8.com',
      // social: Share.Social.WHATSAPP,
      
    }

    handleShowPoint()
    Share.open(shareOptions).then(async openReturn => {
      if(openReturn.dismissedAction){
        const userId = await getItem('userId');
        await UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.Share);
      }
    });
  }

  render() {
    const window = Dimensions.get('window')

    const interpolatedColor = this.animatedValue.interpolate({
      inputRange: [0, 500],
      outputRange: ['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.85)'],
      extrapolate: 'clamp'
    })

    const event = Animated.event([
      {
        nativeEvent: { 
          contentOffset: {
            y: this.animatedValue
          }
        }
      }
    ])

    if (this.state.interactionsComplete === true) {
      return (
        Object.keys(this.props.showData).length === 0 ?
        <>
          <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 44 : 32}}>
            <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
          </LinearGradient>
          <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator size="large" color="#FF724C" />
            </View>
          </SafeAreaView>
        </>
        :
        <View style={{flex: 1}}>
          <Animated.View style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 44 : 32, backgroundColor: interpolatedColor}}>
            <StatusBar barStyle='light-content' translucent={true} />
          </Animated.View>
          <SafeAreaView style={{flex: 1}}>
            <Animated.View style={{backgroundColor: interpolatedColor, marginBottom: 16}}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10}}>
                <TouchableWithoutFeedback onPress={() => this.goBack()}>
                  <Image source={backArrow} style={{height: 48, width: 48}} />
                </TouchableWithoutFeedback>
                <View style={{flexDirection: 'row'}}>
                  <PointAnimatedItem></PointAnimatedItem>
                  <TouchableWithoutFeedback onPress={this.share}>
                    <Image source={share} style={{height: 48, width: 48}} />
                  </TouchableWithoutFeedback>
                </View>
              </View>
            </Animated.View>
            <ImageBackground 
              style={{flex: 1, zIndex: -10, marginTop: -120, resizeMode: 'cover'}} 
              source={{uri: this.props.showData.thumbnail}} 
              blurRadius={2}
            >
              <View style={{flex: 1, width: window.width, backgroundColor: 'rgba(0, 0, 0, 0.4)'}}>
                <FlatList
                  onScroll={event}
                  style={{marginBottom: this.props.playerView === 'hidden' ? 0 : 50, marginTop: 104, paddingBottom: (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 34 : 0}}
                  keyExtractor={item => item.id + item.title + item.source_id}
                  data={this.props.showData.episodes}
                  renderItem={({item, index}) => {
                    if (index !== 0) {
                      return <ShowEpisode episodeData={item} navigateBackToShowScreen={this.navigateBackToShowScreen} />
                    } else {
                      return (
                        <LinearGradient 
                          colors={['rgba(0, 0, 0, 0.63)', 'rgba(0, 0, 0, 0.85)', 'rgba(0, 0, 0, 0.85)']}
                          locations={[0, 0.9, 1]}
                          style={{zIndex: 0, width: window.width}}
                        >
                          <ShowEpisode episodeData={item} navigateBackToShowScreen={this.navigateBackToShowScreen} isFirst={true} />
                        </LinearGradient>
                      )
                    }
                    
                  }}
                  ListHeaderComponent={() => (
                    <ShowHeader />
                  )}
                  ListEmptyComponent={() => (
                    <LinearGradient 
                      colors={['rgba(0, 0, 0, 0.63)', 'rgba(0, 0, 0, 0.85)', 'rgba(0, 0, 0, 0.85)']}
                      locations={[0, 0.9, 1]}
                      style={{zIndex: 0, width: window.width}}
                    >
                      <View style={{height: 350, width: window.width}}></View>
                    </LinearGradient>
                  )}
                />
              </View>
            </ImageBackground>
          </SafeAreaView>
          {
            ((Dimensions.get('window').height >= 812 && Platform.OS === 'ios') && this.props.playerView === 'hidden') && <View style={{position: 'absolute', bottom: 0, right: 0, left: 0, height: 34, backgroundColor: 'rgba(0, 0, 0, 0.9)'}}></View>
          }
          {
            (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') &&
            <Animated.View style={{height: 44, position: 'absolute', top: 0, left: 0, right: 0, backgroundColor: interpolatedColor}}>
              <StatusBar barStyle='light-content' translucent={true} />
            </Animated.View>
          }
        </View>
      )
    } else {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        </View>
      )
    }
    
  }
}

const mapStateToProps = state => {
  return {
    playerView: state.playerView,
    showData: state.showData,
    language: state.language
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setActiveScreen: (activeScreen) => dispatch(setActiveScreen(activeScreen)),
  storeShow: (showData) => dispatch(storeShow(showData))
})

export default connect(mapStateToProps, mapDispatchToProps)(ShowScreen)
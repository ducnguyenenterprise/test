import React from 'react'
import { Text, View, TouchableWithoutFeedback, Image, ImageBackground, StatusBar } from 'react-native'
import { storeAuthState } from './utils/actions'
import { setPlayerView } from '../Player/utils/actions'
import { connect } from 'react-redux'
import { refreshSession, validateAccessTokenExpirationDate , setExpirationDate, authorizeUser } from '../../utils/auth/functions'
import { listHome } from '../Home/utils/actions'
import { getItem, setItem } from '../../utils/asyncstorage'
import google from '../../assets/google.png'
import facebook from '../../assets/facebook.png'
import SplashScreen from 'react-native-splash-screen'
import background from '../../assets/intro_bg.png'
import { styles } from './utils/styles'
import logo from '../../assets/intro_logo.png'
import Auth0 from 'react-native-auth0'
const auth0 = new Auth0({ domain: 'waves8.au.auth0.com', clientId: 'UVFEBa2LRPBACtZq4a4gO5eaQFFo4SBI' })

class IntroScreen extends React.Component {

  componentDidMount() {
    SplashScreen.hide()
    this.props.setPlayerView('hidden')
    let socialIdentityProvider = null
    // retrieve auth data from AsyncStorage and check access token availability & expiration date
    /*getItem('authState')
    .then(authState => {
      if (authState !== null || authState !== undefined) {
        const { accessTokenExpirationDate, refreshToken } = authState
        socialIdentityProvider = authState.socialIdentityProvider
        const isExpired = validateAccessTokenExpirationDate(accessTokenExpirationDate)
        
        if (!isExpired) {
          // if not expired, save auth data to Redux and navigate to Home screen
          this.props.storeAuthState({ 
            accessTokenExpirationDate, 
            refreshToken, 
            socialIdentityProvider
          })
          this.props.navigation.navigate('App')
          return 'valid'
        } else {
          // if expired, refresh the access token
          return refreshSession(socialIdentityProvider, refreshToken)
        }
      }
    })
    .then(result => {
      let refreshedAuthState
      if (result !== 'valid') {
        refreshedAuthState = result
        const accessTokenExpirationDate = setExpirationDate()

        const authObject = {
          accessTokenExpirationDate, 
          refreshToken: refreshedAuthState.refreshToken, 
          socialIdentityProvider
        }

        // save refreshed auth data to Redux & AsyncStorage
        this.props.storeAuthState(authObject)
        setItem('authState', JSON.stringify(authObject))
      }
    })*/


  }

  handleAuthorization = socialIdentityProvider => {
    return auth0
    .webAuth
    .authorize({scope: 'openid offline_access profile email', connection: socialIdentityProvider})
    .then(credentials => {
      console.log('CREDENTIALS', credentials)
    })
    .catch(error => console.log(error));
  }

  handleStart() {
    this.props.listHome({})
    setItem('persona', 'f6b7ee17-8bce-4cbb-93cc-3117f2b6d938') //hardcoded default persona for now
    .then(() => {
      this.props.navigation.navigate('App')
    })
  }

  render() {
    return (
      <ImageBackground source={background} resizeMode='stretch' style={styles.imageBg}>
        <StatusBar backgroundColor='transparent' translucent={true} />
        <Image source={logo} style={styles.logo} />
        <TouchableWithoutFeedback onPress={() => this.handleAuthorization('facebook')}>
          <View style={[styles.buttonContainer, styles.fbBgColor]}>
            <View style={styles.rowContainer}>
              <Image source={facebook} style={styles.icon}/>
              <Text style={[styles.buttonText, styles.fbText]}>Sign up with facebook</Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => this.handleAuthorization('google-oauth2')}>
          <View style={[styles.buttonContainer, styles.googleBgColor]}>
            <View style={styles.rowContainer}>
              <Image source={google} style={styles.icon}/>
              <Text style={[styles.buttonText, styles.googleText]}>Sign up with google</Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <View style={{flexDirection: 'row', width: window.width, height: 18, alignItems: 'center', justifyContent: 'center', marginBottom: 20}}>
          <View style={{width: 42, height: 1, backgroundColor: '#D3D4D8', marginTop: 2}}></View>
          <Text style={{fontFamily: 'Roboto', fontSize: 16, color: '#D3D4D8', marginHorizontal: 10}}>or</Text>
          <View style={{width: 42, height: 1, backgroundColor: '#D3D4D8', marginTop: 2}}></View>
        </View>
        <TouchableWithoutFeedback onPress={() => this.handleStart()}>
          <View style={[styles.buttonContainer, styles.guestBgColor]}>
            <Text style={[styles.buttonText, styles.guestText]}>Start listening now!</Text>
          </View>
        </TouchableWithoutFeedback>
      </ImageBackground>
    )
  }
}

const mapStateToProps = state => {
  return {
    authState: state.authState
  }
}

const mapDispatchToProps = dispatch => ({
  listHome: (homeData) => dispatch(listHome(homeData)),
  storeAuthState: (authState) => dispatch(storeAuthState(authState)),
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView))
})

export default connect(mapStateToProps, mapDispatchToProps)(IntroScreen)

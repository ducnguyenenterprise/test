import { StyleSheet, Dimensions } from 'react-native'

const window = Dimensions.get('window')

export const styles = StyleSheet.create({
  imageBg: {
    flex: 1, 
    justifyContent: 'flex-start', 
    alignItems: 'center'
  },
  logo: {
    height: 170, 
    width: 170, 
    marginTop: 70 + 24, 
    marginBottom: 70
  },
  buttonContainer: {
    width: window.width - 60, 
    height: 48, 
    borderRadius: 24, 
    overflow: 'hidden', 
    justifyContent: 'center', 
    alignItems: 'center', 
    marginBottom: 20,
  },
  fbBgColor: {
    backgroundColor: '#507BBF'
  },
  googleBgColor: {
    backgroundColor: 'white'
  },
  guestBgColor: {
    backgroundColor: '#3E465F'
  },
  rowContainer: {
    flexDirection: 'row', 
    marginHorizontal: 20, 
    alignItems: 'center'
  },
  icon: {
    width: 24, 
    height: 24
  },
  buttonText: {
    fontFamily: 'Roboto', fontSize: 19, letterSpacing: 0.5, textTransform: 'capitalize'
  },
  fbText: {
    paddingLeft: 5,
    color: 'white'
  },
  googleText: {
    paddingLeft: 10,
    color: '#E04A32'
  },
  guestText: {
    color: 'white'
  }
})
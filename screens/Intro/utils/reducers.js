import { STORE_AUTH_STATE } from './types'

const auth =  (state = {}, action) => {
  switch (action.type) {
    case STORE_AUTH_STATE:
      const authStateCopy = JSON.parse(JSON.stringify({
        ...state,
        authState: action.authState
      }))
      return authStateCopy
    default:
      return state
  }
}

export default auth
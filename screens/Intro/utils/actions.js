import { STORE_AUTH_STATE } from './types'

export const storeAuthState = authState => {
  return {
    type: STORE_AUTH_STATE,
    authState
  }
}
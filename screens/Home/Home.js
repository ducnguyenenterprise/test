import React from 'react'
import { View, Linking, StyleSheet, StatusBar, Dimensions, BackHandler, ActivityIndicator, RefreshControl, Platform, Animated, SafeAreaView } from 'react-native'
import Carousel from './components/Carousel'
import Title from './components/Title'
import Paragraph from './components/Paragraph'
import Ad from './components/Ad'
import EpisodeBox from './components/EpisodeBox'
import ShowBox from './components/ShowBox'
import CategoryBox from './components/CategoryBox'
import EpisodeSwiper from './components/EpisodeSwiper'
import LoadingScreen from './components/LoadingScreen'
import Header from './components/Header'
import LinearGradient from 'react-native-linear-gradient'
import { connect } from 'react-redux'
import Separator from './components/Separator'
import Api from '../../utils/Api'
import { listHome, setConnectionType } from './utils/actions'
import { getItem, setItem } from '../../utils/asyncstorage'
import { setPlayerState, setPlayerView, addTrack } from '../Player/utils/actions'
import { storeDownloadedIds, storeSavedShows, storeSavedEpisodes } from '../Library/utils/actions'
import TrackPlayer from 'react-native-track-player'
import { alertServerError } from '../../utils/alerts'
import NetInfo from '@react-native-community/netinfo'
import { setInternet, setLanguage } from '../utils/actions'
import { FlatList } from 'react-native-gesture-handler'
import SplashScreen from 'react-native-splash-screen'
import { setAutomaticDownload, setAuthState, storeUserData } from '../Profile/utils/actions'
import { storeShow } from '../Show/utils/actions'
import Helper from '../utils/Helper'
import Error from '../Search/components/Error'
import ItemAnimator from '../../components/ItemAnimator'
import RNExitApp from 'react-native-exit-app'
import {isIphoneX} from '../../utils/config'


class HomeScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      refreshing: false,
      error: false,
      loading: false
    }
    
    this.api = new Api
  }

  async componentDidMount() {
    SplashScreen.hide()

    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        // this.navigate(url)
      })
    } else {
        Linking.addEventListener('url', this.handleOpenURL)
    }
    
    //setItem('persona', 'f6b7ee17-8bce-4cbb-93cc-3117f2b6d938')

    getItem('isLoggedIn')
    .then(res => {
      if (res !== undefined && res !== null) {
        this.props.setAuthState(JSON.parse(res))
      }
    })

    getItem('automaticDownload')
    .then(res => {
      if (res !== undefined && res !== null) {
        this.props.setAutomaticDownload(JSON.parse(res))
      } else {
        setItem('automaticDownload', 'true')
      }
    })

    getItem('language')
    .then(res => {
      if (res !== undefined && res !== null) {
        this.props.setLanguage(res)
      }
    })

    NetInfo.fetch().then(state => {
      this.props.setConnectionType(state.type)
    })
    
    NetInfo.addEventListener(state => {
      this.props.setInternet(state.isConnected)
      this.props.setConnectionType(state.type)
    })
    
    this.props.addTrack({})

    // getItem('persona')
    // .then(personaId => {
    //   return this.api.fetchHome(personaId)
    // })

    this.api.fetchHome('f6b7ee17-8bce-4cbb-93cc-3117f2b6d938') // hardcoded persona id
    .then((data) => {
      if (data.success !== true) {
        this.setState({ error: true })
        //alertServerError()
      } else {
        this.props.listHome(data.home)
      }
      
    })
    .catch(() => {
      console.log('error')
    })
    
    // await TrackPlayer.setupPlayer()
    await TrackPlayer.updateOptions({ 
      stopWithApp: true,
      jumpInterval: 30,
      icon: require('../../assets/appIcon40.png'),
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SEEK_TO,
        TrackPlayer.CAPABILITY_JUMP_FORWARD,
        TrackPlayer.CAPABILITY_JUMP_BACKWARD
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SEEK_TO,
        TrackPlayer.CAPABILITY_JUMP_FORWARD,
        TrackPlayer.CAPABILITY_JUMP_BACKWARD
      ],
    })

    getItem('lastPlayed')
    .then(async (res) => {
       if (res !== undefined && res !== null) {
        this.props.setPlayerView('miniUp')
        this.props.addTrack(JSON.parse(res))
        await TrackPlayer.stop()
        await TrackPlayer.reset()
        await TrackPlayer.add(JSON.parse(res))

        return getItem('lastPlayedAudioPosition')
       } 
    })
    .then(pos => {
      pos && TrackPlayer.seekTo(JSON.parse(pos))
    })

    this.props.setPlayerState('notPlaying')

    TrackPlayer.setRate(1)

    const idString = await getItem('downloadedIds')
    if (idString !== undefined && idString !== null && idString.length > 0) {
      this.props.storeDownloadedIds(idString)
    }

    const idArray = idString ? idString.split('||') : []

    const userId = await getItem('userId')
    const userDataResponse = await this.api.getUserData(userId)
    console.log('userdata', userDataResponse)

    if (userDataResponse.success === true) {
      this.props.storeUserData(userDataResponse.user)
    }

    // let internet = null
    // try {
    //   internet = await NetInfo.fetch()
    // } catch (e) {
    // }
    const internet = await NetInfo.fetch()
    if (internet.isConnected === true) {
      let epLibrary = []
      let showLibrary = []
      
      this.api.fetchLibrary(userId)
      .then(library => {
        // if library.success === true
        epLibrary = library.episodes
        showLibrary = library.shows
        
        if (idArray.length > 0) {
          epLibrary.forEach(ep => {
            if (idArray.includes(ep.source_id)) {
              ep['isDownloaded'] = true
            } else {
              ep['isDownloaded'] = false
            }
          })
        } else {
          epLibrary.forEach(ep => {
            ep['isDownloaded'] = false
          })
        }
        this.props.storeSavedEpisodes(epLibrary)
        this.props.storeSavedShows(showLibrary)
      })
    } else {
      // in case there is no internet connection
      getItem('savedEpisodes')
      .then(res => {
        if (res !== undefined && res !== null) {
          // save to redux
          this.props.storeSavedEpisodes(JSON.parse(res))
        }
      })

      getItem('savedShows')
      .then(res => {
        if (res !== undefined && res !== null) {
          // save to redux
          this.props.storeSavedShows(JSON.parse(res))
        }
      })
    }

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.props.navigation.isFocused()) {
        RNExitApp.exitApp()
        return true
      }
    })
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL)
    TrackPlayer.destroy()
  }

  handleOpenURL = (event) => {
    console.log('DEEPLINK', event)
    this.navigate(event.url)
  }

  // Deep linking
  // To test: com.waves8.app://show/4d3fe717742d4963a85562e9f84d8c79
  navigate = (url) => { 
    const { navigate } = this.props.navigation
    const route = url.replace(/.*?:\/\//g, '')
    const id = route.match(/\/([^\/]+)\/?$/)[1]
    const routeName = route.split('/')[0]
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      navigation: this.props.navigation,
    })

    console.log(url, routeName)

    if (routeName === 'show') {
      this.helper.openShow(id)
    }
  }


  onRefresh = () => {
    this.setState({ refreshing: true })

    this.api.fetchHome('f6b7ee17-8bce-4cbb-93cc-3117f2b6d938') // hardcoded persona id
    .then((data) => {
      if (data.success === true) {
        this.props.listHome(data.home)
        this.setState({ refreshing: false }) 
      }
    })
    .catch(() => {
      console.log('error')
    }) 
   
  }

  retry = () => {
    this.setState({ loading: true })

    this.api.fetchHome('f6b7ee17-8bce-4cbb-93cc-3117f2b6d938') // hardcoded persona id
    .then((data) => {
      if (data.success === true) {
        this.props.listHome(data.home)
        this.setState({ loading: false, error: false }) 
      }
    })
    .catch(() => {
      console.log('error')
    }) 
  }



  renderItem = (list) => {
    const item = list.item
    const ren = () => {
      switch (item.type) {
        case 'carousel':
          return (
            <ItemAnimator index={list.index}>
              <Carousel items={item.data.items} title={item.title} />
              <Separator />
            </ItemAnimator>
          )
        case 'ad':
          return (
            <ItemAnimator index={list.index}>
              <Ad adImg={item.data.image_url} adUrl={item.data.url} title={item.title} />
              <Separator />
            </ItemAnimator>
          )
        case 'categorybox':
          return (
            <ItemAnimator index={list.index}>
              <CategoryBox items={item.data.items} navigation={this.props.navigation} title={item.title} />
              <Separator />
            </ItemAnimator>
          )
        case 'title':
          return (
            <ItemAnimator index={list.index}>
              <Title title={item.data} />
              <View style={{height: 20}}></View>
            </ItemAnimator>
          )
        case 'paragraph':
          return (
            <ItemAnimator index={list.index}>
              <Paragraph text={item.data}/>
              <Separator />
            </ItemAnimator>
          )
        case 'episode':
          return (
            <ItemAnimator index={list.index}>
              <EpisodeBox episode={item} navigation={this.props.navigation} title={item.title} />
              <Separator />
            </ItemAnimator>
          )
        case 'episodebox':
          return (
            <ItemAnimator index={list.index}>
              <EpisodeSwiper episodes={item.data.items} navigation={this.props.navigation} title={item.title} />
              <Separator />
            </ItemAnimator>
          )
        case 'show':
          return (
            <ItemAnimator index={list.index}>
              <ShowBox navigation={this.props.navigation} show={item} title={item.title} />
              <Separator />
            </ItemAnimator>
          )
      }
    }

    return (
      <View style={{flex: 1, alignItems: 'center', marginTop: 10}}>
        {ren()}
      </View>
    )
  }


  render() {
    const window = Dimensions.get('window')

    const home = this.props.home.length > 0 ? this.props.home.filter(el => el !== null) : []

    const styles = StyleSheet.create({ // We need to place this in render to check if playerView is miniUp
      container: {
        flex: 1,
        maxWidth: window.width,
        marginTop: 60,
        marginBottom: this.props.playerView === 'miniUp' ? 50 : 0,
      }
    })

    return (
      this.state.error === true ?
      <View style={{flex: 1}}>
        <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') === true ? 44 : 32}}>
          <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
        </LinearGradient>
        <SafeAreaView style={{flex: 1}}>
          <View style={{position: 'absolute', left: 0, right: 0}}> 
            <Header navigation={this.props.navigation} />
          </View>
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Error retry={this.retry} home={true} />
          </View>
        </SafeAreaView>
      </View>
      :
      (Object.keys(this.props.home).length === 0 || this.state.loading === true)
      ?
      <View style={{flex: 1}}>
        <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') === true ? 44 : 32}}>
          <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
        </LinearGradient>
        <SafeAreaView style={{flex: 1}}>
          <View style={{position: 'absolute', left: 0, right: 0}}> 
            <Header navigation={this.props.navigation} />
          </View>
          <View>
            <LoadingScreen />
          </View>
        </SafeAreaView>
      </View>
      :
      <View style={{flex: 1}}>
        <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') === true ? 44 : 32}}>
          <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
        </LinearGradient>
        <SafeAreaView style={{flex: 1}}>
          <FlatList 
            data={home}
            style={styles.container}
            keyExtractor={item => item.id + item.order}
            refreshControl={
              <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
            }
            initialNumToRender={5}
            maxToRenderPerBatch={10}
            windowSize={30}
            renderItem={this.renderItem}
          />
          <View style={{position: 'absolute', left: 0, right: 0}}> 
            <Header navigation={this.props.navigation} />
          </View>
        </SafeAreaView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    playerView: state.playerView,
    home: state.home,
    internet: state.internet,
    downloadedIds: state.downloadedIds,
    connectionType: state.connectionType
  }
}

const mapDispatchToProps = dispatch => ({
  listHome: (homeData) => dispatch(listHome(homeData)),
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track)),
  storeDownloadedIds: (idString) => dispatch(storeDownloadedIds(idString)),
  setInternet: (connection) => dispatch(setInternet(connection)),
  storeSavedShows: (shows) => dispatch(storeSavedShows(shows)),
  storeSavedEpisodes: (episodes) => dispatch(storeSavedEpisodes(episodes)),
  setAutomaticDownload: (state) => dispatch(setAutomaticDownload(state)),
  setLanguage: (language) => dispatch(setLanguage(language)),
  setConnectionType: (type) => dispatch(setConnectionType(type)),
  setAuthState: (isLoggedIn) => dispatch(setAuthState(isLoggedIn)),
  storeShow: (showData) => dispatch(storeShow(showData)),
  storeUserData: (data) => dispatch(storeUserData(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
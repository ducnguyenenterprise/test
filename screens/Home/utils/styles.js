import { StyleSheet, Dimensions } from 'react-native'

const window = Dimensions.get('window')

export const adStyles = StyleSheet.create({
  adImage: {
    borderRadius: 4, 
    overflow: 'hidden'
  }
})

export const categoryStyles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'flex-start', 
    alignItems: 'flex-start', 
    marginRight: 10, 
    maxWidth: 120,
  },
  image: {
    width: 120, 
    height: 120, 
    borderRadius: 4, 
    overflow: 'hidden', 
    marginBottom: 10
  }
})

export const episodeStyles = StyleSheet.create({
  container: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'center', 
    alignItems: 'center',
    maxWidth: window.width
  },
  epImage: {
    width: 120, 
    height: 120, 
    justifyContent: 'center', 
    alignItems: 'center', 
    borderRadius: 4, 
    overflow: 'hidden'
  },
  touchable: {
    alignItems: 'center', 
    justifyContent: 'center'
  },
  play: {
    width: 100, 
    height: 100
  },
  textContainer: {
    paddingLeft: 10, 
    width: window.width - 20 - 20 - 10 - 120
  }
})
  
export const headerStyles = StyleSheet.create({
  
})

export const separatorStyles = StyleSheet.create({
  separator : {
    height: 1, 
    width: window.width - 20 - 20, 
    backgroundColor: '#D3D4D8', 
    marginTop: 10,
    alignSelf: 'center',
  }
})

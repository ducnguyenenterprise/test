import { LIST_HOME, SET_CONN_TYPE } from './types'

export const listHome = homeData => {
  return {
    type: LIST_HOME,
    homeData
  }
}

export const setConnectionType = connType => {
  return {
    type: SET_CONN_TYPE,
    connType
  }
}
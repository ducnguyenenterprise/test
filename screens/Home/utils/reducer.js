import { LIST_HOME, SET_CONN_TYPE } from './types'

export const listHome =  (state = {}, action) => {
  switch (action.type) {
    case LIST_HOME:
     return action.homeData
    default:
      return state
  }
}

export const connectionType =  (state = {}, action) => {
  switch (action.type) {
    case SET_CONN_TYPE:
     return action.connType
    default:
      return state
  }
}

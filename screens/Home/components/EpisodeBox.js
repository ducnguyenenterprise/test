import React from 'react'
import { View, ImageBackground, Image, TouchableWithoutFeedback } from 'react-native'
import play from '../../../assets/home_play.png'
import { connect } from 'react-redux'
import { setPlayerState, setPlayerView, addTrack, setShowTitle } from '../../Player/utils/actions'
import { EpisodeTitle, Author, Category } from '../../../utils/textMixins'
import { episodeStyles } from '../utils/styles'
import { transformDescriptionOnHomeScreen } from '../../Player/utils/functions'
import Helper from '../../utils/Helper'
import TrackPlayer from 'react-native-track-player'
import { getItem, setItem } from '../../../utils/asyncstorage'
import { setActiveScreen } from '../../utils/actions'
import NavigationService from '../../../NavigationService'

class EpisodeBox extends React.Component {

  playEpisode = () => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      addTrack: this.props.addTrack, 
      setPlayerState: this.props.setPlayerState,
      playerView: 'miniUp'
    })

    this.helper.playEpisode(this.props.episode.data)
  }

  openEpisode = episode => {
    this.props.setShowTitle('')
    this.props.setPlayerView('hidden')
    if (this.props.downloadedIds.length > 0) {
      if (this.props.downloadedIds.includes(episode.source_id)) {
        episode['isDownloaded'] = true
      } else {
        episode['isDownloaded'] = false
      }
    }
    

    const url = episode.isDownloaded === true ? 'file://' + RNFS.DocumentDirectoryPath + `/music/${episode.source_id}.mp3` : episode.file_id
    const track = {
      id: /*episode.id ? episode.id :*/ episode.source_id,
      url: url,
      title: episode.title,
      artist: episode.author, 
      artwork: episode.thumbnail,
      duration: episode.audio_length,
      show_id: episode.show_id,
      file_id: episode.file_id,
      thumbnail: episode.thumbnail,
      description: episode.description,
      source_id: episode.source_id ? episode.source_id : null,
      show_source_id: episode.show_source_id,
    }
    this.props.addTrack(track)

    // check if the track has already a saved position in asyncstorage 
    let position = null
    getItem(track.id)
    .then(async res => {
      if (res !== undefined && res !== null) {
        position = JSON.parse(res)
      }
      await TrackPlayer.stop()
      await TrackPlayer.reset()
      await TrackPlayer.add(track)
      // seek to the saved position if it exists
      position !== null && TrackPlayer.seekTo(position)
      TrackPlayer.play()
    })
    
    this.props.setPlayerState('playing')
    NavigationService.navigate('PlaybackScreen', { fromMini: true, miniPosition: 'miniUp' })
    setItem('lastPlayed', JSON.stringify(track))
  }
  
  render() {
    const { episode } = this.props
    return (
      <>
        {
          this.props.title.length > 0 &&
          <View style={{flex: 1, justifyContent: 'center', alignSelf: 'flex-start', flexDirection: 'row', paddingHorizontal: 20}}>
            <Category>{this.props.title}</Category>
          </View>
        }
        <TouchableWithoutFeedback onPress={() => this.openEpisode(episode.data)}>
          <View style={episodeStyles.container}>
            <ImageBackground source={{uri: episode.data.thumbnail}} style={episodeStyles.epImage}>
              <TouchableWithoutFeedback style={episodeStyles.touchable} onPress={this.playEpisode}>
                  <Image source={play} style={episodeStyles.play}/>
              </TouchableWithoutFeedback>
            </ImageBackground>
            <View style={episodeStyles.textContainer}>
              <EpisodeTitle lines={2}>{episode.data.title}</EpisodeTitle>
              <Author lines={1}>{episode.data.author}</Author>
              {transformDescriptionOnHomeScreen(episode.data.description)}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    downloadedIds: state.downloadedIds
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track)),
  setActiveScreen: (activeScreen) => dispatch(setActiveScreen(activeScreen)),
  setShowTitle: (title) => dispatch(setShowTitle(title))
})

export default connect(mapStateToProps, mapDispatchToProps)(EpisodeBox)
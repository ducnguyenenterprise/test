import React from 'react'
import { View } from 'react-native'
import { separatorStyles } from '../utils/styles'

class Separator extends React.Component {

  render() {
    return (
      <View style={separatorStyles.separator}></View>
    )
  }
}

export default Separator
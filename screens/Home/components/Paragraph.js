import React from 'react'
import { View } from 'react-native'
import { Body1 } from '../../../utils/textMixins'

class Paragraph extends React.Component {
  render() {
    return (
      <View>
        <Body1>{this.props.text}</Body1>
      </View>
    )
  }
}

export default Paragraph
import React from 'react'
import { View, Image, TouchableWithoutFeedback, Text, Dimensions } from 'react-native'
import { headerStyles } from '../utils/styles'
import search from '../../../assets/header_search.png'
import profile from '../../../assets/header_profile.png'
import orangeProfile from '../../../assets/orange_profile.png'
import logo from '../../../assets/header_logo.png'
import profileBack from '../../../assets/profile_back.png'
import i18n from '../../../utils/i18n'
import { connect } from 'react-redux'
import { setSearchKey } from '../../Search/utils/actions' 
import ProfileAnimatedItem from './ProfileAnimatedItem'

class Header extends React.Component {
  render() {
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <View style={{width: window.width, height: 60, paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderBottomColor: '#F0F1F3', borderBottomWidth: this.props.withTab === true ? 0 : 1}}>
        {
          this.props.profile === true ? 
          <TouchableWithoutFeedback onPress={this.props.goBack}>
            <View style={{height: 48, justifyContent: 'center'}}>
              <Image source={profileBack} style={{width: 48, height: 48}}/>
            </View>
          </TouchableWithoutFeedback>
          :
          <View style={{height: 48, justifyContent: 'center'}}>
            <Image source={logo} style={{width: 48, height: 48}}/>
          </View>
        }
        <TouchableWithoutFeedback onPress={() => {
          this.props.navigation.navigate('Search')
          this.props.setSearchKey()
        }}>
          <View style={{height: 40, backgroundColor: '#F0F1F3', borderRadius: 24, overflow: 'hidden', width: window.width - 48 - 48 - 20 - 20}}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
              <Image source={search} style={{width: 38, height: 38}} />
              <Text style={{fontFamily: 'Roboto', fontSize: 14, color: '#D3D4D8'}}>{i18n.t('search placeholder')}</Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        {
          this.props.profile === true ?
            <View style={{height: 48, justifyContent: 'center'}}>
              <ProfileAnimatedItem profile = {this.props.profile}/>
            </View>
          :
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Profile')}>
            <View style={{height: 48, justifyContent: 'center'}}>
              <ProfileAnimatedItem profile = {this.props.profile}/>
            </View>
          </TouchableWithoutFeedback>
        }
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    language: state.language
  }
}

const mapDispatchToProps = dispatch => ({
  setSearchKey: () => dispatch(setSearchKey()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
import React from 'react'
import { Dimensions, TouchableWithoutFeedback, View, Animated } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import FastImage from 'react-native-fast-image'
import Helper from '../../utils/Helper'
import { storeShow } from '../../Show/utils/actions'
import Api from '../../../utils/Api'
import { connect } from 'react-redux'
import { setPlayerView, addTrack, setPlayerState } from '../../Player/utils/actions'
import { withNavigation } from 'react-navigation'
import { Category } from '../../../utils/textMixins'

class ImageCarousel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeSlide: 0,
      scaleValue: new Animated.Value(0)
    }

    this.api = new Api
  }

  componentDidMount() {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      api: this.api,
      navigation: this.props.navigation,

      addTrack: this.props.addTrack,
      setPlayerState: this.props.setPlayerState,
      playerView: 'episode'
    })
  }

  openShow = id => {
    this.helper.openShow(id)
  }

  playEpisode = episode => {
    this.helper.playEpisode(episode)
  }

  renderCarouselItem = ({item, index}) => {
    return (
      <TouchableWithoutFeedback style={{width: 315}} onPress={() => item.type === 'episode' ? this.playEpisode(item) : this.openShow(item.source_id)}>
        <FastImage source={{uri: item.carousel_thumbnail}} style={{width: 315, height: 120, paddingBottom: -10, borderRadius: 4, overflow: 'hidden'}} />
      </TouchableWithoutFeedback>
    )
  }

  render() {
    const window = Dimensions.get('window')
    return (
      <>
        {
          this.props.title.length > 0 &&
          <View style={{flex: 1, justifyContent: 'center', alignSelf: 'flex-start', flexDirection: 'row', paddingHorizontal: 20}}>
            <Category>{this.props.title}</Category>
          </View>
        }
        <Carousel
          ref={(c) => { this._carousel = c }}
          data={this.props.items}
          renderItem={this.renderCarouselItem}
          sliderWidth={window.width}
          itemWidth={330}
          layout={'default'}
          onSnapToItem={(index) => this.setState({ activeSlide: index })}
          loop={true}
          loopClonesPerSide={2}
          autoplay={true}
          autoplayDelay={500}
          autoplayInterval={7000}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          containerCustomStyle={{paddingLeft: 7.5}}
        />
        <Pagination
          dotsLength={this.props.items.length}
          activeDotIndex={this.state.activeSlide}
          dotStyle={{
            width: 6,
            height: 6,
            borderRadius: 5,
            backgroundColor: '#000000'
          }}
          inactiveDotStyle={{
            width: 6,
            height: 6,
            borderRadius: 5,
            backgroundColor: '#BDBDBD'  
          }}
          inactiveDotScale={1}
          containerStyle={{bottom: 15, marginBottom: -46}}
        />
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    playerView: state.playerView,
    isPlaying: state.isPlaying
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  storeShow: (showData) => dispatch(storeShow(showData)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track))  
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ImageCarousel))
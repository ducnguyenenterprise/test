import React from 'react'
import { View } from 'react-native'
import { Category } from '../../../utils/textMixins'

class Title extends React.Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignSelf: 'flex-start', flexDirection: 'row'}}>
        <Category>{this.props.title}</Category>
      </View>
    )
  }
}

export default Title
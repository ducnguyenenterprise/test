import React from 'react'
import { View, Dimensions, ActivityIndicator } from 'react-native'

class LoadingScreen extends React.Component {

  render() {
		const window = Dimensions.get('window')
		const color = '#F0F1F3'
		return (
      <View style={{marginTop: 80}}>
				<View style={{flexDirection: 'row'}}>
					<View style={{height: 120, width: (window.width - 40 - 315) / 2, backgroundColor: color, borderTopRightRadius: 5, borderBottomRightRadius: 5}}></View>
					<View style={{height: 120, width: 315, backgroundColor: color, marginHorizontal: 20, borderRadius: 5}}></View>
					<View style={{height: 120, width: (window.width - 40 - 315) / 2, backgroundColor: color, borderTopLeftRadius: 5, borderBottomLeftRadius: 5}}></View>
				</View>
				<View style={{height: 1, width: window.width - 40, alignSelf: 'center', backgroundColor: color, marginVertical: 20}} />
				<View style={{height: 18, width: 160, backgroundColor: color, marginLeft: 20, marginBottom: 20}}></View>
				<View style={{flexDirection: 'row', marginHorizontal: 20}}>
					<View>
						<View style={{height: (window.width - 40 - 20) / 3, width: (window.width - 40 - 20) / 3, backgroundColor: color, borderRadius: 4}}></View>
						<View style={{height: 11, width: 80, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: 9, width: 50, backgroundColor: color, marginTop: 6}}></View>
					</View>
					<View style={{marginHorizontal: 10}}>
						<View style={{height: (window.width - 40 - 20) / 3, width: (window.width - 40 - 20) / 3, backgroundColor: color, borderRadius: 4}}></View>
						<View style={{height: 11, width: 80, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: 9, width: 50, backgroundColor: color, marginTop: 6}}></View>
					</View>
					<View>
						<View style={{height: (window.width - 40 - 20) / 3, width: (window.width - 40 - 20) / 3, backgroundColor: color, borderRadius: 4}}></View>
						<View style={{height: 11, width: 80, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: 9, width: 50, backgroundColor: color, marginTop: 6}}></View>
					</View>
				</View>
				<View style={{height: 1, width: window.width - 40, alignSelf: 'center', backgroundColor: color, marginVertical: 20}} />
				<View style={{flexDirection: 'row', marginHorizontal: 20}}>
					<View style={{height: 120, width: 120, backgroundColor: color, marginRight: 10, borderRadius: 4}}></View>
					<View>
						<View style={{height: 14, width: 120, backgroundColor: color}}></View>
						<View style={{height: 10, width: 60, backgroundColor: color, marginTop: 5}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 15}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
					</View>
				</View>
				<View style={{height: 1, width: window.width - 40, alignSelf: 'center', backgroundColor: color, marginVertical: 20}} />
				<View style={{flexDirection: 'row', marginHorizontal: 20}}>
					<View style={{height: (window.width - 40 - 20) / 3, width: (window.width - 40 - 20) / 3, backgroundColor: color, borderRadius: 4}}></View>
					<View style={{height: (window.width - 40 - 20) / 3, width: (window.width - 40 - 20) / 3, backgroundColor: color, borderRadius: 4, marginHorizontal: 10}}></View>
					<View style={{height: (window.width - 40 - 20) / 3, width: (window.width - 40 - 20) / 3, backgroundColor: color, borderRadius: 4}}></View>
				</View>
				<View style={{height: 1, width: window.width - 40, alignSelf: 'center', backgroundColor: color, marginVertical: 20}} />
				<View style={{flexDirection: 'row', marginHorizontal: 20}}>
					<View style={{height: 120, width: 120, backgroundColor: color, marginRight: 10, borderRadius: 4}}></View>
					<View>
						<View style={{height: 14, width: 120, backgroundColor: color}}></View>
						<View style={{height: 10, width: 60, backgroundColor: color, marginTop: 5}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 15}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 4, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
					</View>
				</View>
				<View style={{position: 'absolute', alignItems: 'center', justifyContent: 'center', top: 40, alignSelf: 'center'}}>
					<ActivityIndicator size="large" color="#FF724C" />
				</View>
      </View>
    )
  }
}

export default LoadingScreen
import React from 'react'
import { View, ImageBackground, TouchableWithoutFeedback, Image } from 'react-native'
import Swiper from 'react-native-swiper'
import play from '../../../assets/home_play.png'
import { episodeStyles } from '../utils/styles'
import { EpisodeTitle, Author, Category } from '../../../utils/textMixins'
import { setPlayerView, addTrack, setPlayerState, setShowTitle } from '../../Player/utils/actions'
import { connect } from 'react-redux'
import { transformDescriptionOnHomeScreen } from '../../Player/utils/functions'
import Helper from '../../utils/Helper'
import TrackPlayer from 'react-native-track-player'
import { getItem, setItem } from '../../../utils/asyncstorage'
import { setActiveScreen } from '../../utils/actions'
import NavigationService from '../../../NavigationService'
import RNFS from 'react-native-fs'

class EpisodeSwiper extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    
  }

  playEpisode = episode => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      addTrack: this.props.addTrack, 
      setPlayerState: this.props.setPlayerState,
      playerView: 'miniUp'
    })

    this.helper.playEpisode(episode)
  }

  openEpisode = async episode => {
    this.props.setShowTitle('')
    this.props.setPlayerView('hidden')
    if (this.props.downloadedIds.length > 0) {
      if (this.props.downloadedIds.includes(episode.source_id)) {
        episode['isDownloaded'] = true
      } else {
        episode['isDownloaded'] = false
      }
    }

    const fileExists = await RNFS.exists(RNFS.DocumentDirectoryPath + '/music')

    const url = (episode.isDownloaded === true && fileExists === true) ? 'file://' + RNFS.DocumentDirectoryPath + `/music/${episode.source_id}.mp3` : episode.file_id
    const track = {
      id: /*episode.id ? episode.id :*/ episode.source_id,
      url: url,
      title: episode.title,
      artist: episode.author, 
      artwork: episode.thumbnail,
      duration: episode.audio_length,
      show_id: episode.show_id,
      file_id: episode.file_id,
      thumbnail: episode.thumbnail,
      description: episode.description,
      source_id: episode.source_id ? episode.source_id : null,
      show_source_id: episode.show_source_id,
    }
    
    this.props.addTrack(track)

    // check if the track has already a saved position in asyncstorage 
    let position = null
    getItem(track.id)
    .then(async res => {
      console.log('RES', res, track.id)
      if (res !== undefined && res !== null) {
        position = JSON.parse(res)
      }
      TrackPlayer.stop()
      TrackPlayer.reset()
      await TrackPlayer.add(track)
      // seek to the saved position if it exists
      position !== null && TrackPlayer.seekTo(position)
      TrackPlayer.play()
    })
    
    this.props.setPlayerState('playing')
    NavigationService.navigate('PlaybackScreen', { fromMini: true, miniPosition: 'miniUp' })
    setItem('lastPlayed', JSON.stringify(track))
  }

  render() {
    return (
      <>
        {
          this.props.title.length > 0 &&
          <View style={{flex: 1, justifyContent: 'center', alignSelf: 'flex-start', flexDirection: 'row', paddingHorizontal: 20}}>
            <Category>{this.props.title}</Category>
          </View>
        }
        <View style={{flex: 1, height: 140, paddingLeft: 20}}>
          <Swiper 
            dotStyle={{height: 6, width: 6, marginRight: 7, marginLeft: 7, marginVertical: 6}}
            activeDotStyle={{height: 6, width: 6, marginRight: 7, marginLeft: 7, marginVertical: 6}}
            showsButtons={false} 
            dotColor='#BDBDBD'
            loop={false}
            activeDotColor='black'
            paginationStyle={{bottom: -5}}
          >
            
            {
              this.props.episodes.map(ep => {
                return (
                  <TouchableWithoutFeedback onPress={() => this.openEpisode(ep)} key={ep.title + ep.id}>
                    <View style={{flex: 1, flexDirection: 'row', paddingBottom: 20}}>
                      <ImageBackground source={{uri: ep.thumbnail}} style={episodeStyles.epImage}>
                        <TouchableWithoutFeedback style={episodeStyles.touchable} onPress={() => this.playEpisode(ep)}>
                          <Image source={play} style={episodeStyles.play}/>
                        </TouchableWithoutFeedback>
                      </ImageBackground>
                      <View style={episodeStyles.textContainer}>
                        <EpisodeTitle lines={2}>{ep.title}</EpisodeTitle>
                        <Author lines={1}>{ep.author}</Author>
                        {transformDescriptionOnHomeScreen(ep.description)}
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                )
              })
            }
          </Swiper>
        </View>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    savedEpisodes: state.savedEpisodes,
    downloadedIds: state.downloadedIds
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track)),
  setActiveScreen: (activeScreen) => dispatch(setActiveScreen(activeScreen)),
  setShowTitle: (title) => dispatch(setShowTitle(title))
})

export default connect(mapStateToProps, mapDispatchToProps)(EpisodeSwiper)
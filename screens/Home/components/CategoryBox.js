import React from 'react'
import { ScrollView, View, Image, TouchableWithoutFeedback } from 'react-native'
import { EpisodeTitle, Author } from '../../../utils/textMixins'
import { categoryStyles } from '../utils/styles'
import Api from '../../../utils/Api'
import { storeShow } from '../../Show/utils/actions'
import { setPlayerView, addTrack, setPlayerState } from '../../Player/utils/actions'
import { connect } from 'react-redux'
import play from '../../../assets/dark_play.png'
import Helper from '../../utils/Helper'
import FastImage from 'react-native-fast-image'
import { Category } from '../../../utils/textMixins'

class CategoryBox extends React.Component {
  constructor(props) {
    super(props)
    this.api = new Api
    this.player = null
  }

  componentDidMount() {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      api: this.api,
      navigation: this.props.navigation,

      addTrack: this.props.addTrack,
      setPlayerState: this.props.setPlayerState,
      playerView: 'miniUp'
    })
  }

  openShow = id => {
    this.helper.openShow(id)
  }

  playEpisode = episode => {
    this.helper.playEpisode(episode)
  }
  
  render() {
    return (
      <>
        {
          this.props.title.length > 0 &&
          <View style={{flex: 1, justifyContent: 'center', alignSelf: 'flex-start', flexDirection: 'row', paddingHorizontal: 20}}>
            <Category>{this.props.title}</Category>
          </View>
        }
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{paddingLeft: 20, paddingRight: 20, alignItems: 'flex-start'}}>
          {
            this.props.items.map(item => {
              const { source_id, id, image_url, thumbnail, author, title, type } = item
              if (type === 'show') {
                return (
                  <TouchableWithoutFeedback onPress={() => this.openShow(source_id)} key={item.source_id + item.author}>
                    <View key={id + image_url} style={categoryStyles.container}>
                      <FastImage source={{uri: thumbnail}} style={categoryStyles.image} />
                      <EpisodeTitle lines={2}>{title}</EpisodeTitle>
                      <Author lines={2}>{author}</Author>
                    </View>
                  </TouchableWithoutFeedback>
                )
              } else if (type === 'episode') {
                return (
                  <TouchableWithoutFeedback onPress={() => this.playEpisode(item)}>
                    <View key={id + image_url} style={categoryStyles.container}>
                      <View style={{position: 'absolute', width: 120, height: 120, zIndex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image source={play} style={{width: 64, height: 64}}/>
                      </View>
                      <FastImage source={{uri: thumbnail}} style={categoryStyles.image} />
                      <EpisodeTitle lines={2}>{title}</EpisodeTitle>
                      <Author lines={2}>{author}</Author>
                    </View>
                  </TouchableWithoutFeedback>
                )
              }
              
            })
          }
        </ScrollView>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    playerView: state.playerView,
    isPlaying: state.isPlaying
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  storeShow: (showData) => dispatch(storeShow(showData)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track))  
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryBox)

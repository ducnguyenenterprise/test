import React from 'react'
import { View, Image } from 'react-native'
import profile from '../../../assets/header_profile.png'
import orangeProfile from '../../../assets/orange_profile.png'
import yellowProfile from '../../../assets/yellow_profile.png'
import { connect } from 'react-redux'
import { Animated } from 'react-native'

class ProfileAnimatedItem extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      fadeYellowValue: new Animated.Value(0),
      fadeBackValue: new Animated.Value(1)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.showPoint === this.props.showPoint) return
    
    if (this.props.showPoint === true) {
      Animated.timing(this.state.fadeBackValue, {
        toValue: 0,
        useNativeDriver: true,
        duration: 300
      }).start(() => {
      });

      Animated.timing(this.state.fadeYellowValue, {
        toValue: 1,
        useNativeDriver: true,
        duration: 600
      }).start(() => {
        Animated.timing(this.state.fadeYellowValue, {
          toValue: 0,
          useNativeDriver: true,
          duration: 400
        }).start(() => {
        });
  
        Animated.timing(this.state.fadeBackValue, {
          toValue: 1,
          useNativeDriver: true,
          duration: 200
        }).start(() => {
        });
      });
    }
  }

  profileIcon() {
    if (this.props.showPoint === true) {
      return (
        <View>
          <Animated.View style={{opacity: this.state.fadeBackValue}}>
            <Image source={this.props.profile? orangeProfile :profile} style={{width: 48, height: 48}}/>
          </Animated.View>
          <Animated.View style={{opacity: this.state.fadeYellowValue, position:'absolute'}}>
            <Image source={yellowProfile} style={{width: 48, height: 48}}/>
          </Animated.View>
        </View>
      )
    } else {
      return (
        <Image source={this.props.profile? orangeProfile :profile} style={{width: 48, height: 48}}/>
      )
    }
  }

  render() {
    return this.profileIcon()
  }
}

const mapStateToProps = state => {
  return {
    showPoint: state.showPoint
  }
}

export default connect(mapStateToProps)(ProfileAnimatedItem)
import React from 'react'
import { View, TouchableWithoutFeedback } from 'react-native'
import { connect } from 'react-redux'
import { setPlayerView } from '../../Player/utils/actions'
import { EpisodeTitle, Author, Category } from '../../../utils/textMixins'
import { episodeStyles } from '../utils/styles'
import Api from '../../../utils/Api'
import { storeShow } from '../../Show/utils/actions'
import { transformDescriptionOnHomeScreen } from '../../Player/utils/functions'
import Helper from '../../utils/Helper'
import FastImage from 'react-native-fast-image'

class ShowBox extends React.Component {
  constructor(props) {
    super(props)
    this.api = new Api
  }

  openShow = id => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      navigation: this.props.navigation,
    })

    this.helper.openShow(id)
  }
  
  render() {
    const { data } = this.props.show
    return (
      <>
        {
          this.props.title.length > 0 &&
          <View style={{flex: 1, justifyContent: 'center', alignSelf: 'flex-start', flexDirection: 'row'}}>
            <Category>{this.props.title}</Category>
          </View>
        }
        <TouchableWithoutFeedback style={episodeStyles.touchable} onPress={() => this.openShow(data.source_id)}>
          <View style={episodeStyles.container}>
              <FastImage source={{uri: data.thumbnail}} style={episodeStyles.epImage} />
            <View style={episodeStyles.textContainer}>
              <EpisodeTitle lines={2}>{data.title}</EpisodeTitle>
              <Author lines={1}>{data.author}</Author>
              {transformDescriptionOnHomeScreen(data.description)}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  storeShow: (showData) => dispatch(storeShow(showData))
})

export default connect(mapStateToProps, mapDispatchToProps)(ShowBox)
import React from 'react'
import { Image, Linking, TouchableWithoutFeedback, Dimensions, View } from 'react-native'
import { adStyles } from '../utils/styles'
import FastImage from 'react-native-fast-image'
import { Category } from '../../../utils/textMixins'

class Ad extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      width: 0,
      height: 0
    }
  }

  handleClick = () => {
    Linking.canOpenURL(this.props.adUrl).then(supported => {
      if (supported) {
        Linking.openURL(this.props.adUrl)
      } else {
        console.log('error', this.props.adUrl)
      }
    })
  }

  componentDidMount() {
    Image.getSize(this.props.adImg, (width, height) => { this.setState({ width, height }) })
  }

  render() { 
    const { width, height } = this.state
    // calculating the dimensions of a fully responise image   
    const window = Dimensions.get('window')
    const calculatedWidth = window.width - 20 - 20
    const actualWidth = width
    const actualHeight = height
    const ratio = calculatedWidth / actualWidth
    const calculatedHeight = actualHeight * ratio
    
    return ( 
      <>
        {
          this.props.title.length > 0 &&
          <View style={{flex: 1, justifyContent: 'center', alignSelf: 'flex-start', flexDirection: 'row', paddingHorizontal: 20}}>
            <Category>{this.props.title}</Category>
          </View>
        }
        {
          (width !== 0 && height !== 0) ? (       
            <TouchableWithoutFeedback onPress={this.handleClick}>
              <FastImage source={{uri: this.props.adImg}} style={[{width: calculatedWidth, height: calculatedHeight}, adStyles.adImage]} />
            </TouchableWithoutFeedback>
          ) : (
            null
          )
        }
      </>
    )
  }
}

export default Ad
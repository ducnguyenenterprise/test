import React from 'react'
import { View, Image } from 'react-native'
import yellowProfile from '../../assets/yellow_profile.png'
import { connect } from 'react-redux'
import { Animated } from 'react-native'

class PointAnimatedItem extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      fadeYellowValue: new Animated.Value(0),
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.showPoint === this.props.showPoint) return
    
    if (this.props.showPoint === true) {
      Animated.timing(this.state.fadeYellowValue, {
        toValue: 1,
        useNativeDriver: true,
        duration: 400
      }).start(() => {
        Animated.timing(this.state.fadeYellowValue, {
          toValue: 0,
          useNativeDriver: true,
          duration: 300,
          delay: 300
        }).start(() => {
        });
      });
    }
  }

  profileIcon() {
    if (this.props.showPoint === true) {
      return (
        <View>
          <Animated.View style={{opacity: this.state.fadeYellowValue}}>
            <Image source={yellowProfile} style={{width: 48, height: 48}}/>
          </Animated.View>
        </View>
      )
    } else {
      return (
        <View/>
      )
    }
  }

  render() {
    return this.profileIcon()
  }
}

const mapStateToProps = state => {
  return {
    showPoint: state.showPoint
  }
}

export default connect(mapStateToProps)(PointAnimatedItem)
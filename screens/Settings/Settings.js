import React from 'react'
import { Text, View } from 'react-native'
import i18n from '../../utils/i18n'

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text accessibilityLabel='settingsText'>{i18n.t('settings')}</Text>
      </View>
    )
  }
}

export default SettingsScreen
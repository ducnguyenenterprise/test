import { AUTOMATIC_DOWNLOAD, LOGIN_MODAL, AUTH, STORE_USER_DATA } from './types'

export const setAutomaticDownload = isAutomatic => {
  return {
    type: AUTOMATIC_DOWNLOAD,
    isAutomatic
  }
}

export const setLoginModal = openState => {
  return {
    type: LOGIN_MODAL,
    openState
  }
}

export const setAuthState = isLoggedIn => {
  return {
    type: AUTH,
    isLoggedIn
  }
}

export const storeUserData = data => {
  return {
    type: STORE_USER_DATA,
    data
  }
}


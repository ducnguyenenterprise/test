import { StyleSheet, Dimensions } from 'react-native'

const window = Dimensions.get('window')

export const styles = StyleSheet.create({
  buttonContainer: {
    width: window.width - 60, 
    height: 48, 
    borderRadius: 24, 
    overflow: 'hidden', 
    justifyContent: 'center', 
    alignItems: 'center', 
    marginBottom: 20,
  },
  fbBgColor: {
    backgroundColor: '#507BBF'
  },
  googleBgColor: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#DF4931'
  },
  appleBgColor: {
    backgroundColor: 'black'
  },
  guestBgColor: {
    backgroundColor: '#3E465F'
  },
  rowContainer: {
    flexDirection: 'row',
  },
  icon: {
    width: 24, 
    height: 24,
  },
  buttonText: {
    fontFamily: 'Roboto',
    fontSize: 21,
    letterSpacing: 0.5,
    //textTransform: 'capitalize',
  },
  fbText: {
    paddingLeft: 5,
    color: 'white'
  },
  googleText: {
    paddingLeft: 10,
    color: '#E04A32'
  },
  appleText: {
    paddingLeft: 10,
    color: 'white'
  }
})
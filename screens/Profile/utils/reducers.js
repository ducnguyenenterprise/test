import { AUTOMATIC_DOWNLOAD, LOGIN_MODAL, AUTH, STORE_USER_DATA } from './types'
import { setItem } from '../../../utils/asyncstorage'

export const automaticDownload =  (state = {}, action) => {
  switch (action.type) {
    case AUTOMATIC_DOWNLOAD:
      setItem('automaticDownload', JSON.stringify(action.isAutomatic))
     return action.isAutomatic
    default:
      return state
  }
}

export const loginModal =  (state = {}, action) => {
  switch (action.type) {
    case LOGIN_MODAL:
     return action.openState
    default:
      return state
  }
}

export const isLoggedIn =  (state = {}, action) => {
  switch (action.type) {
    case AUTH:
      setItem('isLoggedIn', JSON.stringify(action.isLoggedIn))
     return action.isLoggedIn
    default:
      return state
  }
}

export const userData =  (state = {}, action) => {
  switch (action.type) {
    case STORE_USER_DATA:
      
      setItem('userData', typeof action.data === 'object' ? JSON.stringify(action.data) : action.data)
     return action.data
    default:
      return state
  }
}
import React from 'react'
import { View, BackHandler, StatusBar, Dimensions, ScrollView, Switch, Alert, Platform, SafeAreaView, Linking } from 'react-native'
import i18n from '../../utils/i18n'
import { connect } from 'react-redux'
import { setPlayerView } from '../Player/utils/actions'
import { storeDownloadedIds, storeSavedEpisodes, storeSavedShows } from '../Library/utils/actions'
import { getItem, setItem, removeItem } from '../../utils/asyncstorage'
import LinearGradient from 'react-native-linear-gradient'
import Header from '../Home/components/Header'
import { EpisodeTitle } from '../../utils/textMixins'
import { setAutomaticDownload, storeUserData, setAuthState, storePoint } from './utils/actions'
import LoginModal from './components/LoginModal'
import { setLanguage } from '../utils/actions'
import User from './components/User'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'
import Api from '../../utils/Api'
import { ProfileViewModel } from '../../view_models/profile_screen/profile_view_model'
import { UnitOfWork } from '../../App'

export const handleLogoutComplete = {
  completed: () => {}
};

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = new ProfileViewModel(this, UnitOfWork);
    
    this.api = new Api()
  }

  componentDidMount() {
    this.props.setPlayerView('hidden')
    
    const languageValue = this.props.language === 'Vietnamese' ? false : true

    this.setState({ switch: this.props.automaticDownload, languageSwitch: languageValue })

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.props.navigation.isFocused()) {
        if (Object.keys(this.props.currentTrack).length !== 0) {
          getItem('lastPlayed')
          .then(res => {
            if (res !== undefined && res !== null) {
              this.props.setPlayerView('miniUp')
            }
          })
          this.props.navigation.goBack(null)
          return true
        } else {
          this.props.navigation.goBack(null)
          return true
        }
      }
    })
  }

  goBack = () => {
    getItem('lastPlayed')
    .then(res => {
      if (res !== undefined && res !== null) {
        this.props.setPlayerView('miniUp')
      }
    })
    this.props.navigation.goBack(null)
  }

  setAutoDownload = (value) => {
    this.setState({ switch: value })
    this.props.setAutomaticDownload(value)
  }

  setLanguage = (value) => {
    this.setState({ languageSwitch: value })
    const language = value === true ? 'English' : 'Vietnamese'
    this.props.setLanguage(language)
  }

  handleLogout = async () => {
    // generate new user
    let res = await this.api.newUser()
    const userId = res.data.id
    await setItem('userId', userId)

    // clear library, downloadedIds, userData
    this.props.storeSavedEpisodes([])
    this.props.storeSavedShows([])
    this.props.storeDownloadedIds('')

    this.props.setAuthState(false)
    this.props.storeUserData({})

    await removeItem('savedEpisodes')
    await removeItem('savedShows')
    await removeItem('downloadedIds')
    await removeItem('userData')
    await handleLogoutComplete.completed();
  }

  alertLogout = () => {
    Alert.alert(
      'Are you sure to exit?',
      '',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Logout', onPress: () => this.handleLogout()},
      ],
      {cancelable: false},
    )
  }

  feedBack = async () => {
    Linking.openURL('https://waves8.com/feedback/');
  };

  render() {
    const window = Dimensions.get('window')
    this.props.setPlayerView('hidden')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <View style={{flex: 1}}>
        <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 44 : 32}}>
          <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
        </LinearGradient>
        <SafeAreaView style={{flex: 1}}>
          <View style={{position: 'absolute', top: 0, left: 0, right: 0, zIndex: 999, backgroundColor: 'white'}}> 
            <Header navigation={this.props.navigation} profile={true} goBack={this.goBack}/>
          </View>
          <ScrollView contentContainerStyle={{paddingTop: 60, marginHorizontal: 20, width: window.width - 40}}>
            <User isLoggedIn={this.props.isLoggedIn} />
            <View style={{height: 80, borderBottomWidth: 1, borderBottomColor: '#D3D4D8', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
              <EpisodeTitle>{i18n.t('auto-download')}</EpisodeTitle>
              <Switch
                ios_backgroundColor={'#D3D4D8'}
                thumbColor={'white'}
                trackColor={ {false: '#D3D4D8', true: '#FF724C'}}
                value={this.state.switch}
                onValueChange={(value) => this.setAutoDownload(value)}
              />
            </View>
            <View style={{height: 80, borderBottomWidth: 1, borderBottomColor: '#D3D4D8', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
              <EpisodeTitle>{i18n.t('vietnamese')}</EpisodeTitle>
              <Switch
                ios_backgroundColor={'#D3D4D8'}
                thumbColor={'white'}
                trackColor={ {false: '#D3D4D8', true: '#FF724C'}}
                value={this.state.languageSwitch}
                onValueChange={(value) => this.setLanguage(value)}
              />
              <EpisodeTitle>{i18n.t('english')}</EpisodeTitle>
            </View>
            <TouchableWithoutFeedback onPress={() => Linking.openURL('https://waves8.com/terms-of-service/')}>
              <View style={{height: 80, borderBottomWidth: 1, borderBottomColor: '#D3D4D8', flexDirection: 'row', alignItems: 'center'}}>
                <EpisodeTitle>{i18n.t('terms-of-use')}</EpisodeTitle>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => Linking.openURL('https://waves8.com/privacy-policy/')}>
              <View style={{height: 80, borderBottomWidth: 1, borderBottomColor: '#D3D4D8', flexDirection: 'row', alignItems: 'center'}}>
                <EpisodeTitle>{i18n.t('privacy-policy')}</EpisodeTitle>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={this.feedBack}>
              <View style={{height: 80, borderBottomWidth: 1, borderBottomColor: '#D3D4D8', flexDirection: 'row', alignItems: 'center'}}>
                <EpisodeTitle>{i18n.t('feedback')}</EpisodeTitle>
              </View>
            </TouchableWithoutFeedback>
            {
              this.props.isLoggedIn === true &&
              <TouchableWithoutFeedback onPress={this.alertLogout}>
                <View style={{height: 80, borderBottomWidth: 1, borderBottomColor: '#D3D4D8', flexDirection: 'row', alignItems: 'center'}}>
                  <EpisodeTitle>{i18n.t('log out')}</EpisodeTitle>
                </View>
              </TouchableWithoutFeedback>
            }
          </ScrollView>
          <LoginModal />
        </SafeAreaView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentTrack: state.currentTrack,
    language: state.language,
    automaticDownload: state.automaticDownload,
    isLoggedIn: state.isLoggedIn
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setAutomaticDownload: (isAutomatic) => dispatch(setAutomaticDownload(isAutomatic)),
  setLanguage: (state) => dispatch(setLanguage(state)),
  setAuthState: (isLoggedIn) => dispatch(setAuthState(isLoggedIn)),
  storeSavedShows: (shows) => dispatch(storeSavedShows(shows)),
  storeSavedEpisodes: (episodes) => dispatch(storeSavedEpisodes(episodes)),
  storeDownloadedIds: (ids) => dispatch(storeDownloadedIds(ids)),
  storeUserData: (data) => dispatch(storeUserData(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)


import React from 'react'
import { Modal, Text, TouchableWithoutFeedback, View, Dimensions, Image, ActivityIndicator, Platform } from 'react-native'
import { connect } from 'react-redux'
import { setLoginModal, setAuthState, storeUserData } from '../utils/actions'
import { storeSavedShows, storeSavedEpisodes } from '../../Library/utils/actions'
import i18n from '../../../utils/i18n'
import { Episode } from '../../../utils/textMixins'
import google from '../../../assets/google.png'
import facebook from '../../../assets/facebook.png'
import apple from '../../../assets/apple.png'
import apple_id from '../../../assets/appleid_button.png'
import { styles } from '../utils/styles'
import { getItem, setItem } from '../../../utils/asyncstorage'
import Auth0 from 'react-native-auth0'
import Api from '../../../utils/Api'
import { UnitOfWork } from '../../../App'
import { handleShowPoint } from '../../../utils/showPointUtil'
import { POINT_EVENTS } from '../../../models/point_event'

const auth0 = new Auth0({ domain: 'waves8.au.auth0.com', clientId: 'UVFEBa2LRPBACtZq4a4gO5eaQFFo4SBI' })
export const handleAuthorizationComplete = {
  completed:()=>{}
};
class Speed extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false
    }

    this.api = new Api()
  }
  
  handleAuthorization = async socialIdentityProvider => {
    this.setState({ loading: true })
    auth0
    .webAuth
    .authorize({scope: 'openid offline_access profile email', connection: socialIdentityProvider})
    .then(async credentials => {
      let userId = await getItem('userId')
      const auth = await this.api.authenticate(userId, credentials.idToken)
      if (auth.data.id !== userId) {
        userId = auth.data.id
      }
      await setItem('userId', auth.data.id)

      const userDataResponse = await this.api.getUserData(userId)

      if (userDataResponse.success === true) {
        this.props.storeUserData(userDataResponse.user)
        this.props.setLoginModal('closed')
        this.setState({ loading: false })
      }

      const library = await this.api.fetchLibrary(auth.data.id)
      // if library.success === true
      epLibrary = library.episodes
      showLibrary = library.shows
      
      if (this.props.downloadedIds.length > 0) {
        epLibrary.forEach(ep => {
          if (this.props.downloadedIds.includes(ep.source_id)) {
            ep['isDownloaded'] = true
          } else {
            ep['isDownloaded'] = false
          }
        })
      } else {
        epLibrary.forEach(ep => {
          ep['isDownloaded'] = false
        })
      }
      this.props.storeSavedEpisodes(epLibrary)
      this.props.storeSavedShows(showLibrary)
      this.props.setAuthState(true)
      if (await UnitOfWork.point_user_repository.checkAllowAddPoint(POINT_EVENTS.SignIn) === true) {
        handleShowPoint();
        await UnitOfWork.point_user_repository.addPointToUser(userId);
      }
      await handleAuthorizationComplete.completed();
      
    })
    .catch(error => console.log(error))
  }

  render() {
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <>
      <Modal
        animationType='slide'
        transparent={true}
        visible={this.props.loginModal}
        >
        <View style={{position: 'absolute', bottom: 0, height: window.height + 20, width: window.width}} blurRadius={54}>
          <TouchableWithoutFeedback onPress={() => {this.props.setLoginModal(false); this.setState({loading: false})}}>
            <View style={{flex: 1, height: window.height, width: window.width, backgroundColor: 'rgba(0, 0, 0, 0.4)'}}></View>
          </TouchableWithoutFeedback>
          <View style={{flex: 1, height: window.height * 0.3, width: window.width, backgroundColor: '#F2F2F2'}}>
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 25}}>
              <Episode>{i18n.t('sign in modal text')}</Episode>
            </View>
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
              {
                this.state.loading ?
                <ActivityIndicator />
                :
                <>
                  {
                    Platform.OS === 'ios' &&
                    <TouchableWithoutFeedback onPress={() => this.handleAuthorization('apple')}>
                      <Image source={apple_id} style={{width: window.width - 60, height: (window.width - 60) * (64 / 375), borderRadius: 24, marginBottom: 20}} />
                    </TouchableWithoutFeedback>
                  }
                  <TouchableWithoutFeedback onPress={() => this.handleAuthorization('facebook')}>
                    <View style={[styles.buttonContainer, styles.fbBgColor]}>
                      <View style={styles.rowContainer}>
                        <Image source={facebook} style={styles.icon}/>
                        <Text style={[styles.buttonText, styles.fbText]}>{i18n.t('facebook-auth')}</Text>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback onPress={() => this.handleAuthorization('google-oauth2')}>
                    <View style={[styles.buttonContainer, styles.googleBgColor]}>
                      <View style={styles.rowContainer}>
                        <Image source={google} style={styles.icon}/>
                        <Text style={[styles.buttonText, styles.googleText]}>{i18n.t('google-auth')}</Text>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </>
              }
            </View>
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 20, width: window.width - 60, marginHorizontal: 30}}>
              <Text style={{fontFamily: 'Roboto-Regular', fontSize: 12, color: '#3E465F', textAlign: 'center'}}>{i18n.t('terms-part1')} {i18n.t('terms-of-use')} {i18n.t('terms-part2')} {i18n.t('privacy-policy')}</Text>
            </View>
          </View>
        </View>
      </Modal>
      </>
    ) 
  }
}

const mapStateToProps = state => {
  return {
    loginModal: state.loginModal,
    language: state.language,
    downloadedIds: state.downloadedIds,
  }
}

const mapDispatchToProps = dispatch => ({
  setLoginModal: (state) => dispatch(setLoginModal(state)),
  setAuthState: (isLoggedIn) => dispatch(setAuthState(isLoggedIn)),
  storeSavedShows: (shows) => dispatch(storeSavedShows(shows)),
  storeSavedEpisodes: (episodes) => dispatch(storeSavedEpisodes(episodes)),
  storeUserData: (data) => dispatch(storeUserData(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Speed)
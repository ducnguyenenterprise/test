import React from 'react'
import { View, Image, Dimensions, TouchableWithoutFeedback, ImageBackground, ActivityIndicator, Text, AppState } from 'react-native'
import i18n from '../../../utils/i18n'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import profile from '../../../assets/profile-placeholder.png'
import edit from '../../../assets/edit.png'
import editViet from '../../../assets/edit_viet.png'
import editImg from '../../../assets/edit_img.png'
import { Heading3, Heading5, ButtonText } from '../../../utils/textMixins'
import LoginModal, { handleAuthorizationComplete } from './LoginModal'
import { setLoginModal, storeUserData } from '../utils/actions'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'
import { getItem, removeItem } from '../../../utils/asyncstorage'
import Api from '../../../utils/Api'
import { UserViewModel } from '../../../view_models/profile_screen/user_view_model'
import { UnitOfWork, AppStateChange } from '../../../App'
import { handleShowPoint } from '../../../utils/showPointUtil'
import { handleLogoutComplete } from '../Profile'
import { POINT_EVENTS } from '../../../models/point_event'

class User extends React.Component {
  constructor(props) {
    super(props)
    
    this.state = new UserViewModel(this, UnitOfWork);

    this.api = new Api
  }

  _appStateChange = async (appState, nextAppState) => {
    if (appState.match(/inactive|background/) && nextAppState === 'active'){
      await this.state.getPointByUser();
    }
  };

  componentDidMount() {
    this.state.getPointByUser();
    handleAuthorizationComplete.completed = async () => {
      const userId = await getItem("userId");
      await UnitOfWork.point_user_repository.updatePointToUser(userId, this.state.point);
      await this.state.getPointByUser();
    };
    handleLogoutComplete.completed = async () => {
      await this.state.getPointByUser();
    };
    AppStateChange.addAsyncAppStateChanges(this._appStateChange);
  }

  componentWillUnmount(){
    AppStateChange.removeAsyncAppStateChanges(this._appStateChange);
  }

  edit = () => {
    this.setState({editing: true});
  }

  selectImage = () => {
    
    const options = {
      title: 'Select Image',
      storageOptions: {
        //skipBackup: true,
        path: 'images',
      },
    }

    ImagePicker.launchImageLibrary(options, (response) => {
      
      console.log('Response = ', response)
    
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else {
    
        this.setState({
          imagePath: response.path,
          imageUri: response.uri,
          img: response
        })
      }
    })
  }

  uploadPhoto = async (photo) => {
    this.setState({
      editing: false,
      imageUri: ''
    })
    
    const url = 'https://upload.uploadcare.com/base/'
    let body = new FormData()
    body.append('file', {
      name: photo.fileName,
      type: 'image/jpg',
      uri: photo.uri
    })
    body.append('UPLOADCARE_PUB_KEY', '0cdb0948e5f71e4c1ba1')
    body.append('UPLOADCARE_STORE', '1')

    let response = await axios.post(url, body, { timeout: 20000 })
    const imgUrl = 'https://ucarecdn.com/' + response.data.file + '/'
    

    const userId = await getItem('userId')
    const userUpdate = await this.api.changeProfilePicture(userId, imgUrl)

    if (userUpdate.data.success === true) {
      this.props.storeUserData(userUpdate.data.user)
    }

  }

  render() {
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'

    let imgSource = ''

    if (this.props.isLoggedIn === true) {
      if (this.state.editing === false) {
        if (this.props.userData && this.props.userData.image_url) {
          imgSource = {uri: this.props.userData.image_url}
        } else {
          imgSource = profile
        }

      } else {
        if (this.state.imageUri.length > 0) {
          imgSource = {uri: this.state.imageUri}
        } else {
          if (this.props.userData && this.props.userData.image_url) {
            imgSource = {uri: this.props.userData.image_url}
          } else {
            imgSource = profile
          }
        }
        
      }
      
    } else {
      imgSource = profile
    }

    return (
      <View style={{flexDirection: 'row', width: window.width - 40, borderBottomWidth: 1, borderBottomColor: '#D3D4D8', alignItems: 'center', justifyContent: 'flex-start'}}>
        <View>
          {
            this.state.loading === true ? 
            <ActivityIndicator size="large" color="#FF724C" />
            :
            <ImageBackground source={imgSource} style={{height: 96, width: 96, borderRadius: 96 / 2, overflow: 'hidden', marginTop: 20, marginBottom: 24, marginRight: 20, alignItems: 'center', justifyContent: 'flex-end'}}>
            {
              this.state.editing === true && 
              <TouchableWithoutFeedback onPress={this.selectImage}>
                <Image source={editImg} style={{width: 32, height: 32, marginBottom: 20}} />
              </TouchableWithoutFeedback>
            }
          </ImageBackground>
          }
        </View>
        <View>
          {
            this.props.isLoggedIn === true ?
            <>
              <View style={{marginBottom: 14}}>
                <Heading3>{this.props.userData.name}</Heading3>
                <View style={{flex: 1, flexDirection: "row", alignItems: "stretch"}}>
                  <Text style={{color: "#FF9549",fontSize: 16, lineHeight:18, fontWeight:"bold", alignSelf: 'center'}}>{this.state.point}</Text>
                  <Text style={{color: "#A1A4B1", fontSize: 16, lineHeight:18, alignSelf: 'center', marginLeft:5, marginRight:5}}>Drops</Text>
                </View>
              </View>
              {
                this.state.editing === true ?
                <TouchableWithoutFeedback onPress={() => this.uploadPhoto(this.state.img)}>
                  <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: 38, width: 140, borderRadius: 24, alignItems: 'center', justifyContent: 'center', marginBottom: 20}}>
                    <ButtonText>{i18n.t('done button')}</ButtonText>
                  </LinearGradient>
                </TouchableWithoutFeedback>
                :
                <TouchableWithoutFeedback onPress={this.edit}>
                  <Image source={this.props.language === 'Vietnamese' ? editViet : edit} style={{width: 141, height: 38, marginBottom: 20}} />
                </TouchableWithoutFeedback>
              }
            </>
            :
            <>
              <View style={{marginBottom: 14}}>
                <Heading3>{i18n.t('username placeholder')}</Heading3>
                <View style={{flex: 1, flexDirection: "row", alignItems: "stretch"}}>
                  <Text style={{color: "#FF9549",fontSize: 16, lineHeight:18, fontWeight:"bold", alignSelf: 'center'}}>{this.state.point}</Text>
                  <Text style={{color: "#A1A4B1", fontSize: 16, lineHeight:18, alignSelf: 'center', marginLeft:5, marginRight:5}}>Drops</Text>
                </View>
              </View>
              <TouchableWithoutFeedback onPress={() => this.props.setLoginModal(true)}>
                <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: 38, width: 140, borderRadius: 24, alignItems: 'center', justifyContent: 'center', marginBottom: 20}}>
                  <ButtonText>{i18n.t('sign in button')}</ButtonText>
                </LinearGradient>
              </TouchableWithoutFeedback>
            </>
          }
        </View>
        <LoginModal />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    language: state.language,
    userData: state.userData,
    isLoggedIn: state.isLoggedIn
  }
}

const mapDispatchToProps = dispatch => ({
  setLoginModal: (state) => dispatch(setLoginModal(state)),
  storeUserData: (data) => dispatch(storeUserData(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(User)


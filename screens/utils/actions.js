import { ACTIVE_SCREEN, ADD_FOLLOWED_SHOW, ADD_SAVED_EPISODE, SET_INTERNET, SET_LANGUAGE, SHOW_POINT } from './types'

export const setActiveScreen = activeScreen => {
  return {
    type: ACTIVE_SCREEN,
    activeScreen
  }
}

export const addFollowedShow = show => {
  return {
    type: ADD_FOLLOWED_SHOW,
    show
  }
}

export const addSavedEpisode = episode => {
  return {
    type: ADD_SAVED_EPISODE,
    episode
  }
}

export const setInternet = connection => {
  return {
    type: SET_INTERNET,
    connection
  }
}

export const setShowPoint = showPoint => {
  return {
    type: SHOW_POINT,
    showPoint
  }
}

export const setLanguage = language => {
  return {
    type: SET_LANGUAGE,
    language
  }
}
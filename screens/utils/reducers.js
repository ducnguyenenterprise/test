import { ACTIVE_SCREEN, ADD_FOLLOWED_SHOW, ADD_SAVED_EPISODE, SET_INTERNET, SET_LANGUAGE, SHOW_POINT } from './types'
import { setItem } from '../../utils/asyncstorage'

export const activeScreen =  (state = {}, action) => {
  switch (action.type) {
    case ACTIVE_SCREEN:
     return action.activeScreen
    default:
      return state
  }
}

export const followedShows =  (state = [], action) => {
  const showsCopy = JSON.parse(JSON.stringify(state))
  switch (action.type) {
    case ADD_FOLLOWED_SHOW:
      showsCopy.push(action.show)
     return showsCopy
    default:
      return state
  }
}

export const savedEpisodes =  (state = [], action) => {
  const episodesCopy = JSON.parse(JSON.stringify(state))
  switch (action.type) {
    case ADD_SAVED_EPISODE:
      episodesCopy.push(action.episode)
     return episodesCopy
    default:
      return state
  }
}

export const internet =  (state = {}, action) => {
  switch (action.type) {
    case SET_INTERNET:
     return action.connection
    default:
      return state
  }
}

export const showPoint =  (state = {}, action) => {
  switch (action.type) {
    case SHOW_POINT:
     return action.showPoint
    default:
      return state
  }
}

export const language =  (state = {}, action) => {
  switch (action.type) {
    case SET_LANGUAGE:
      setItem('language', action.language)
     return action.language
    default:
      return state
  }
}
import { getItem, setItem } from '../../utils/asyncstorage'
import RNFS from 'react-native-fs'
import TrackPlayer from 'react-native-track-player'
import { alertServerError } from '../../utils/alerts'
import Api from '../../utils/Api'
import { InteractionManager } from 'react-native'

export default class Helper {
  constructor({
    setPlayerView = null, 
    addTrack = null, 
    setPlayerState = null,
    playerView = null,

    storeShow = null,
    navigation = null,

    savedEpisodes = null,
    downloadedIds = null,
    storeSavedEpisodes = null,
    storeDownloadedIds = null,
    connectionType = null
  }) {
    this.setPlayerView = setPlayerView
    this.addTrack = addTrack
    this.setPlayerState = setPlayerState
    this.playerView = playerView,

    this.storeShow = storeShow,
    this.api = new Api(),
    this.navigation = navigation,

    this.savedEpisodes = savedEpisodes,
    this.downloadedIds = downloadedIds,
    this.storeSavedEpisodes = storeSavedEpisodes,
    this.storeDownloadedIds = storeDownloadedIds,
    this.connectionType = connectionType
  }

  generateKey = () => {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
  }

  storeEvent = (body) => {
    // generate a random key to store event in async
    const eventKey = this.generateKey()

    // we add the key to a string of keys waiting to be sent to the Api
    getItem('eventString')
    .then(string => {
      setItem('eventString', string + '||' + eventKey)
    })

    const bodyWithKey = {...body, event_key: eventKey }

    // save data to async with the random key
    setItem(eventKey, JSON.stringify(bodyWithKey))
  }

  async playEpisode(episode) {
    const userId = await getItem('userId')
    //id, url, title and artist required to TrackPlayer
    const url = episode.isDownloaded === true ? 'file://' + RNFS.DocumentDirectoryPath + `/music/${episode.source_id}.mp3` : episode.file_id
    const track = {
      id: /*episode.id ? episode.id : */episode.source_id,
      url: url,
      title: episode.title,
      artist: episode.author, 
      artwork: episode.thumbnail,
      duration: episode.audio_length,
      show_id: episode.show_id,
      file_id: episode.file_id,
      thumbnail: episode.thumbnail,
      description: episode.description,
      source_id: episode.source_id ? episode.source_id : null,
      show_source_id: episode.show_source_id
    }
    
    this.setPlayerView(this.playerView)
    // add track to redux
    this.addTrack(track)

    // check if the track has already a saved position in asyncstorage 
    let position = null
    getItem(track.id)
    .then(async res => {
      if (res !== undefined && res !== null) {
        position = JSON.parse(res)
      }
      TrackPlayer.stop()
      TrackPlayer.reset()
      await TrackPlayer.add(track)
      // seek to the saved position if it exists
      const isadded = await TrackPlayer.getCurrentTrack()
      position !== null && TrackPlayer.seekTo(position)
      TrackPlayer.play()
    })
    
    this.setPlayerState('playing')

    // save track as last played to async storage
    setItem('lastPlayed', JSON.stringify(track))

    const body = {
      event_type: 'play',
      content_type: 'episode',
      content_id: track.source_id,
      date: new Date(),
      user_id: userId,
      progress: position
    }

    this.storeEvent(body)
  }

  openShow(showId) {
    this.navigation.navigate('Show', { navbar: true })

    getItem('lastPlayed')
    .then(res => {
      if (res === undefined || res === null) {
        this.setPlayerView('hidden')
      } else {
        this.setPlayerView('miniDown')
      }
    })
    
    // fetch the show data then save it to redux
    let show = {}
    let userId = ''
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        //this.setPlayerView('miniDown')
        this.storeShow({})
        getItem('userId')
        .then(uid => {
          userId = uid
          return this.api.fetchShow(showId)
        })
        .then(showData => {
          show = showData.show
          let promises = []
          show.episodes.forEach(ep => {
            // episode ids of the show
            promises.push(getItem(ep.source_id))

          })

          // get the episode progresses from asyncStorage
          return Promise.all(promises)
        })
        .then(progresses => {
          if (!progresses) {
            alertServerError()
          }
          progresses.map((progress, i) => {
            show.episodes[i]['progress'] = JSON.parse(progress)
          })
          this.storeShow(show)
        })
        .catch(() => {
          alertServerError()
        })

        const body = {
          event_type: 'open',
          content_type: 'show',
          content_id: showId,
          date: new Date(),
          user_id: userId
        }
    
        this.storeEvent(body)
      }, 10)
    })
    

  }

  addDownloadedStatus = epLibrary => {
    if (this.downloadedIds.length > 0) {
      epLibrary.forEach(ep => {
        if (this.downloadedIds.includes(ep.source_id)) {
          ep['isDownloaded'] = true
        } else {
          ep['isDownloaded'] = false
        }
      })
    } else {
      epLibrary.forEach(ep => {
        ep['isDownloaded'] = false
      })
    }

    return epLibrary
  }

  handleEpisode = async (id, fileId) => {
    const userId = await getItem('userId')
    if (this.savedEpisodes.find( ({ source_id }) => source_id === id ) !== undefined) {
      return this.api.removeFromLibrary(userId, id)
      .then(library => {
        if (library.data.success === true) {
          const editedLibrary = this.addDownloadedStatus(library.data.episodes)        
          this.storeSavedEpisodes(editedLibrary)
        }

        const path = RNFS.DocumentDirectoryPath + `/music/${id}.mp3`
        return RNFS.unlink(path)
      })
      .then(() => {
        const idsArrayCopy = this.downloadedIds
        const index = idsArrayCopy.indexOf(id)
        if (index > -1) {
          idsArrayCopy.splice(index, 1)
        }
        
        this.storeDownloadedIds(idsArrayCopy.join('||'))

        const savedEpisodesCopy = JSON.parse(JSON.stringify(this.savedEpisodes))
        savedEpisodesCopy.find( ({ source_id }) => source_id === id).isDownloaded = false
        this.storeSavedEpisodes(savedEpisodesCopy)

        //return 'removed'
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch((err) => {
        // TODO error handling
        console.log(err.message)
        //return 'removed'
      })

      
    } else {
      const body = {
        event_type: 'save',
        content_type: 'episode',
        content_id: id,
        date: new Date(),
        user_id: userId,
      }
  
      this.storeEvent(body)

      let editedLibrary = null
      return this.api.addToLibrary(userId, id, 'episode')
      .then(library => {
        console.log('LIBBB', library)
        if (library.data.success === true && library.data.inserted === true) {
          editedLibrary = this.addDownloadedStatus(library.data.episodes)
          this.storeSavedEpisodes(editedLibrary)
          //return 'saved'
        }
      })
      
    }
    
  }

}
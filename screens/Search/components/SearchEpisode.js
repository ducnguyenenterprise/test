import React from 'react'
import { View, Image, TouchableWithoutFeedback, Dimensions, Text, ImageBackground, Animated } from "react-native"
import { connect } from 'react-redux'
import { episodeStyles } from '../../Home/utils/styles'
import { EpisodeTitle, Author } from '../../../utils/textMixins'
import play from '../../../assets/episode_play.png'
import fullHeart from '../../../assets/full_heart.png'
import emptyHeart from '../../../assets/empty_heart.png'
import downloaded from '../../../assets/downloaded.png'
import { setPlayerState, setPlayerView, addTrack, setShowTitle } from '../../Player/utils/actions'
import Api from '../../../utils/Api'
import { storeSavedEpisodes, storeDownloadedIds } from '../../Library/utils/actions'
import Helper from '../../utils/Helper'
import { formatTime } from '../../Player/utils/functions'
import TrackPlayer from 'react-native-track-player'
import NavigationService from '../../../NavigationService'
import { getItem, setItem } from '../../../utils/asyncstorage'
import { UnitOfWork } from '../../../App'
import { POINT_EVENTS } from '../../../models/point_event'

class Episode extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSaved: null,
      scale: new Animated.Value(0)
    }
    
    this.api = new Api()
  }

  componentDidUpdate(prevProps) {
    const isFoundPreviously = prevProps.savedEpisodes.find( ({ source_id }) => source_id === this.props.episode.source_id ) !== undefined
    const isFoundNow = this.props.savedEpisodes.find( ({ source_id }) => source_id === this.props.episode.source_id ) !== undefined

    if (this.state.isSaved === true && isFoundPreviously && !isFoundNow) {
      this.setState({
        isSaved: false
      })
    }
  }

  playEpisode = () => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      addTrack: this.props.addTrack, 
      setPlayerState: this.props.setPlayerState,
    })

    this.helper.playEpisode(this.props.episode)
  }

  handleEpisode = (id, fileId) => {
    this.setState({ isSaved: !this.state.isSaved })
    
    this.helper = new Helper({
      savedEpisodes: this.props.savedEpisodes,
      downloadedIds: this.props.downloadedIds,
      storeSavedEpisodes: this.props.storeSavedEpisodes,
      storeDownloadedIds: this.props.storeDownloadedIds,
      api: this.api,
      connectionType: this.props.connectionType
    })

    this.helper.handleEpisode(id, fileId)
    // .then(res => {
    //   if (res === 'saved' && this.state.isSaved !== true) {
    //     this.setState({
    //       isSaved: true
    //     })
    //   } else if (res === 'removed' && this.state.isSaved === true) {
    //     this.setState({
    //       isSaved: false
    //     })
    //   }
    // })
  }

  openEpisode = async episode => {
    this.props.setShowTitle('')
    this.props.setPlayerView('hidden')
    if (this.props.downloadedIds.length > 0) {
      if (this.props.downloadedIds.includes(episode.source_id)) {
        episode['isDownloaded'] = true
      } else {
        episode['isDownloaded'] = false
      }
    }

    const url = episode.isDownloaded === true ? 'file://' + RNFS.DocumentDirectoryPath + `/music/${episode.source_id}.mp3` : episode.file_id
    const track = {
      id: /*episode.id ? episode.id : */episode.source_id,
      url: url,
      title: episode.title,
      artist: episode.author, 
      artwork: episode.thumbnail,
      duration: episode.audio_length,
      show_id: episode.show_id,
      file_id: episode.file_id,
      thumbnail: episode.thumbnail,
      description: episode.description,
      source_id: episode.source_id ? episode.source_id : null,
      show_source_id: episode.show_source_id,
    }
    this.props.addTrack(track)

    // check if the track has already a saved position in asyncstorage 
    let position = null
    getItem(track.id)
    .then(async res => {
      console.log('RES', res, track.id)
      if (res !== undefined && res !== null && res !== "undefined") {
        position = JSON.parse(res)
      }

      TrackPlayer.stop()
      TrackPlayer.reset()
      await TrackPlayer.add(track)
      // seek to the saved position if it exists
      position !== null && TrackPlayer.seekTo(position)
      TrackPlayer.play()
    })
   
    this.props.setPlayerState('playing')
    NavigationService.navigate('PlaybackScreen', { fromMini: true, miniPosition: 'miniUp' })
    setItem('lastPlayed', JSON.stringify(track))
  }

  animateHeart = () => {
    Animated.spring(this.state.scale, {
      toValue: 2,
      friction: 3,
    }).start(async () => {
      this.state.scale.setValue(0)
      if(this.state.isSaved === true){
        const userId = await getItem('userId');
        await UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.Favourite);
      }
    })
  }

  render() {
    const window = Dimensions.get('window')
    const { episode } = this.props

    if (this.state.isSaved === null) {
      if (this.props.savedEpisodes.find( ({ source_id }) => source_id === this.props.episode.source_id ) !== undefined) {
        this.setState({
          isSaved: true
        })
      } else if (this.props.savedEpisodes.find( ({ source_id }) => source_id === this.props.episode.source_id ) === undefined) {
        this.setState({
          isSaved: false
        })
      }
    }

    const bouncyHeart = this.state.scale.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [1, .6, 1]
    })

    return (
      <View style={{flex: 1, flexDirection: 'row', width: window.width - 40, height: 59.65 + 20 + 20, justifyContent: 'flex-start', alignItems: 'center', borderBottomColor: '#D3D4D8', borderBottomWidth: 1, marginHorizontal: 20}}>
        <View style={episodeStyles.touchable}>
          <TouchableWithoutFeedback onPress={this.playEpisode} style={{width: 59.65, height: 59.65}}>
            <ImageBackground source={{uri: episode.thumbnail}} style={{width: 59.65, height: 59.65, borderRadius: 4, overflow: 'hidden', justifyContent: 'center', alignItems: 'center'}}>
              <TouchableWithoutFeedback onPress={this.playEpisode}>
                <Image source={play} style={{width: 32, height: 32}} />
              </TouchableWithoutFeedback>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </View>
        <TouchableWithoutFeedback onPress={() => this.openEpisode(episode)}>
        <View style={{paddingLeft: 10, width: window.width - 20 - 20 - 59.65 - 8 - 28, marginRight: 8}}>
          <EpisodeTitle lines={2}>{episode.title}</EpisodeTitle>
          <Author lines={1}>{episode.author}</Author>
          <Text style={{fontFamily: 'Roboto-Regular', fontSize: 12, color: '#A1A4B1'}}>{formatTime(episode.audio_length)}</Text>
        </View>
        </TouchableWithoutFeedback>
        <View style={{height: 59.65, justifyContent: 'space-between'}}>
          <TouchableWithoutFeedback style={{width: 28, height: 28}} onPress={() => {this.handleEpisode(episode.source_id, episode.file_id); this.animateHeart()}}>
            <Animated.Image source={this.state.isSaved === true ? fullHeart : emptyHeart} style={{width: 28, height: 28, alignSelf: 'center', transform: [{ scale: bouncyHeart }]}}/>
          </TouchableWithoutFeedback>
          {
            this.props.downloadedIds.includes(episode.source_id) === true &&
            <View style={{width: 28, height: 28, paddingTop: 10}}>
              <Image source={downloaded} style={{height: 18, width: 18, alignSelf: 'center', justifySelf: 'flex-start'}} />
            </View>
          }
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    episodeResults: state.episodeResults,
    savedEpisodes: state.savedEpisodes,
    downloadedIds: state.downloadedIds,
    connectionType: state.connectionType
  }
}

const mapDispatchToProps = dispatch => ({
  storeSearchValue: (value) => dispatch(storeSearchValue(value)),
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track)),
  storeSavedEpisodes: (episodes) => dispatch(storeSavedEpisodes(episodes)),
  storeDownloadedIds: (idString) => dispatch(storeDownloadedIds(idString)),
  setShowTitle: (title) => dispatch(setShowTitle(title))
})

export default connect(mapStateToProps, mapDispatchToProps)(Episode)

import React from 'react'
import { FlatList } from "react-native"
import i18n from '../../../utils/i18n'
import { connect } from 'react-redux'
import Show from './Show'
import SearchEpisode from './SearchEpisode'
import SearchLoading from './SearchLoading'
import Error from './Error'
import { listEpisodeResults, listShowResults } from '../utils/actions'
import Api from '../../../utils/Api'
import ScrollableTabView from 'react-native-scrollable-tab-view'

class SearchTabs extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    
    this.api = new Api()
    this.episodeList = null
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.internet === false && this.props.internet === true) {
      this.props.updateSearch()
    }
  }

  getMoreResults = type => {
    if (this.props.internet === false) return false
    
    let storedResults = []
    switch (type) {
      case 'show':
        storedResults = this.props.showResults
        break
      case 'episode':
        storedResults = this.props.episodeResults
        break
    }
    
    let resultsCopy = [...storedResults.results]
  
    this.api.search(this.props.keywords[0], type, storedResults.next_page)
    .then(res => {
      res.results = resultsCopy.concat(res.results)
      if (type === 'show') {
        this.props.listShowResults(res)
      } else if (type === 'episode') {
        this.props.listEpisodeResults(res)
      }
    })
  }
  
  renderEpisodes = () => {
    return (
      <FlatList
        tabLabel={i18n.t('tab-episodes')}
        style={{paddingBottom: 50}}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.id + item.title + item.source_id}
        data={this.props.episodeResults.results}
        ListHeaderComponent={
          !this.props.episodeResults.success &&
          (this.props.internet === true ?
          <SearchLoading />
          :
          <Error />)
        }
        renderItem={({item, index}) => (
          <SearchEpisode episode={item} />
        )}
        onEndReached={() => {console.log('ONENDREACHED'); this.getMoreResults('episode')}}
        onEndReachedThreshold={0.95}
      />
    )
  }

  renderShows = () => {
    return (
      <FlatList
        tabLabel={i18n.t('tab-shows')}
        style={{paddingBottom: 50}}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.id + item.title + item.source_id}
        data={this.props.showResults.results}
        ListHeaderComponent={
          !this.props.showResults.success &&
          (this.props.internet === true ?
          <SearchLoading />
          :
          <Error />)
        }
        renderItem={({item, index}) => (
          <Show show={item} />
        )}
        onEndReached={() => {console.log('ONENDREACHEDS'); this.getMoreResults('show')}}
        onEndReachedThreshold={0.95}
      />
    )
  }

  renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'first':
        return this.renderEpisodes()
      case 'second':
        return this.renderShows()
    }
  }

  render() {
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <ScrollableTabView 
        style={{marginTop: 60, width: 'auto', justifyContent: 'flex-start', height: 48, paddingBottom: this.props.playerView === 'hidden' ? 0 : 50}}
        tabBarActiveTextColor={'#3E465F'}
        tabBarInactiveTextColor={'#D3D4D8'}
        tabBarTextStyle={{fontFamily: 'Roboto', fontWeight: '500', fontSize: 18, textTransform: 'uppercase'}}
        tabBarUnderlineStyle={{ backgroundColor: "#FF724C", borderTopLeftRadius: 2, borderTopRightRadius: 2, height: 3 }}
      >
        {this.renderShows()}
        {this.renderEpisodes()}
      </ScrollableTabView>
    )
  }
}

const mapStateToProps = state => {
  return {
    episodeResults: Object.keys(state.episodeResults).length > 0 ? JSON.parse(JSON.stringify(state.episodeResults)) : {},
    showResults: Object.keys(state.showResults).length > 0 ? JSON.parse(JSON.stringify(state.showResults)) : {},
    keywords: state.keywords,
    language: state.language,
    internet: state.internet,
    playerView: state.playerView
  }
}

const mapDispatchToProps = dispatch => ({
  listEpisodeResults: (results) => dispatch(listEpisodeResults(results)),
  listShowResults: (results) => dispatch(listShowResults(results))
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchTabs)

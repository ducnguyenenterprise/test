import React from 'react'
import { View, TextInput, Image, TouchableWithoutFeedback, Dimensions } from "react-native"
import i18n from '../../../utils/i18n'
import search from '../../../assets/header_search.png'
import clearInput from '../../../assets/clear_input.png'
import activeSearch from '../../../assets/active_search.png'
import { connect } from 'react-redux'
import { storeSearchValue, listEpisodeResults, listShowResults, storeKeywords } from '../utils/actions'
import Api from '../../../utils/Api'
import { getItem, setItem } from '../../../utils/asyncstorage'
import { withNavigation } from 'react-navigation'
import Helper from '../../utils/Helper'
import profile from '../../../assets/header_profile.png'

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      input: ''
    }
    this.api = new Api
    this.input = null

    this.currentPage = 0
   }

   componentDidMount() {
     this.timer = null

     this.helper = new Helper({
      setItem: setItem,
      getItem: getItem
    })
   }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.toUpdateSearch === false && this.props.toUpdateSearch === true) {
      
      this.getResults(this.props.searchValue)
      this.props.updateSearch()
    }
  
  }

   handleChange = input => {
     clearTimeout(this.timer)
     this.setState({ input })
     this.timer = setTimeout(() => this.triggerChange(input), 500)
   }

   triggerChange = (input = null) => {
     const text = input !== null ? input : this.state.input
     this.getResults(text)
     
     if (text.length > 0) {
      if (this.props.keywords.length < 10) {
        let keywordArray = []
        keywordArray.push(text, ...this.props.keywords)
        this.props.storeKeywords(keywordArray)
 
        setItem('keywords', keywordArray.join(']|['))
      } else if (this.props.keywords.length === 10) {
        let keywordCopy = []
        keywordCopy.push(...this.props.keywords)
        const keywordArray = keywordCopy.slice(0, 9)
        keywordArray.unshift(text)
        this.props.storeKeywords(keywordArray)
 
        setItem('keywords', keywordArray.join(']|['))
      }
     }

   }

   getResults = async (input, page = 0) => {

     this.props.listEpisodeResults({})
     this.props.listShowResults({})
     
     if (input.length > 2) {
       this.props.storeSearchValue(input)
       this.props.changeScreen(false)
       this.api.search(input, 'show', page, this.props.language)
       .then(res => {
         this.props.listShowResults(res)
       })

       this.api.search(input, 'episode', page, this.props.language)
       .then(res => {
         this.props.listEpisodeResults(res)
       })
     } else if (input.length === 0) {
      this.props.changeScreen(true)
     }

      const userId = await getItem('userId')
      const body = {
        event_type: 'tap',
        content_type: 'search',
        date: new Date(),
        user_id: userId,
        query: input
      }

      this.helper.storeEvent(body)
   }
  
   render() {
    if (this.props.tag !== null && this.state.input !== this.props.tag) {
      this.setState({
        input: this.props.tag
      })
      
      this.handleChange(this.props.tag)
    }
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <View>
        <View style={{width: window.width, height: 60, flexDirection: 'row', alignItems: 'center'}}>
          <View style={{height: 40, marginLeft: 20, marginRight: 5, width: window.width - 20 - 48 - 10, backgroundColor: '#F0F1F3', borderRadius: 24, overflow: 'hidden'}}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', zIndex: 0}}>
              <Image source={this.state.input.length > 0 ?  activeSearch : search} style={{width: 38, height: 38}} />
              <TextInput 
                style={{fontFamily: 'Roboto', fontSize: 14, color: '#232735', width: window.width - 20 - 48 - 10 - 38 - 5 - 32}}
                placeholder={i18n.t('search placeholder')}
                placeholderTextColor='#D3D4D8'
                value={this.state.input}
                onChangeText={this.handleChange}
                ref={input => this.props.setRef ? this.props.setRef(input) : console.log('NO PROP TO SET')}
              />
              {
                this.state.input.length > 0 &&
                <TouchableWithoutFeedback onPress={() => { this.setState({ input: '' }); this.props.changeScreen(true) }} style={{width: 32, height: 32, alignItems: 'center', justifyContent: 'center'}}>
                  <View style={{width: 32, height: 32, alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={clearInput} style={{width: 16, height: 16}} />
                  </View>
                </TouchableWithoutFeedback>
              }
            </View>
          </View>
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Profile')}>
            <View style={{height: 48, justifyContent: 'center'}}>
              <Image source={profile} style={{width: 48, height: 48}}/>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    searchValue: state.searchValue,
    keywords: state.keywords,
    language: state.language,
    showResults: state.showResults,
    episodeResults: state.episodeResults
  }
}

const mapDispatchToProps = dispatch => ({
  storeSearchValue: (value) => dispatch(storeSearchValue(value)),
  listEpisodeResults: (results) => dispatch(listEpisodeResults(results)),
  listShowResults: (results) => dispatch(listShowResults(results)),
  storeKeywords: (words) => dispatch(storeKeywords(words)),
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(Header))

import React from 'react'
import { Image, View, TouchableWithoutFeedback, ActivityIndicator } from "react-native"
import { connect } from 'react-redux'
import noInternet from '../../../assets/no_internet.png'
import i18n from '../../../utils/i18n'
import retry from '../../../assets/retry.png'
import { Body2, Heading5 } from '../../../utils/textMixins'
import { listEpisodeResults, listShowResults } from '../utils/actions'
import Api from '../../../utils/Api'

class Error extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false
    }
    this.api = new Api
   }

  retry = () => {
    this.setState({ loading: true })
    setTimeout(() => this.setState({ loading: false }), 300)
    // console.log('INPUT', this.props.searchValue)
    // this.props.listEpisodeResults({})
    // this.props.listShowResults({})
    
    // this.api.search(this.props.searchValue, 'show', 0, this.props.language)
    // .then(res => {
    //   console.log('RES1', res)
    //   this.props.listShowResults(res)
    // })

    // this.api.search(this.props.searchValue, 'episode', 0, this.props.language)
    // .then(res => {
    //   console.log('RES2', res)
    //   this.props.listEpisodeResults(res)
    // })
  }

  render() {
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <View style={{alignItems: 'center'}}>
        {
          this.state.loading === true ?
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 160}}>
            <ActivityIndicator size="large" color="#FF724C" />
          </View>
          :
          <>
          <Image source={noInternet} style={{marginTop: 20, width: 196, height: 196}} />
          <Heading5 styles={{marginTop: 20}}>{i18n.t('internet error title')}</Heading5>
          <Body2 styles={{marginTop: 10, color: '#A1A4B1'}}>{i18n.t('internet error text')}</Body2>
          <TouchableWithoutFeedback onPress={this.props.home === true ? this.props.retry : this.retry}>
            <Image source={retry} style={{width: 140, height: 38, marginTop: 21}} />
          </TouchableWithoutFeedback>
          </>
        }
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    language: state.language,
    searchValue: state.searchValue
  }
}

const mapDispatchToProps = dispatch => ({
  listEpisodeResults: (results) => dispatch(listEpisodeResults(results)),
  listShowResults: (results) => dispatch(listShowResults(results)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Error)

import React from 'react'
import { View, Dimensions, ActivityIndicator } from 'react-native'

class SearchLoading extends React.Component {

	renderRow = () => {
		const window = Dimensions.get('window')
		const color = '#F0F1F3'
		return (
			<>
				<View style={{flexDirection: 'row', marginHorizontal: 20}}>
					<View style={{height: 120, width: 120, backgroundColor: color, marginRight: 10, borderRadius: 4}}></View>
					<View>
						<View style={{height: 14, width: 120, backgroundColor: color}}></View>
						<View style={{height: 10, width: 60, backgroundColor: color, marginTop: 5}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 3, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 15}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 3, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
						<View style={{height: (120 - 14 - 14 - 5 - 15 - 30) / 3, width: window.width - 20 - 20 - 10 - 120, backgroundColor: color, marginTop: 10}}></View>
					</View>
				</View>
				<View style={{height: 1, width: window.width - 40, alignSelf: 'center', backgroundColor: color, marginVertical: 20}} />
			</>
		)
	}

  render() {
		return (
      <View style={{marginTop: 20}}>
				{this.renderRow()}
				{this.renderRow()}
				{this.renderRow()}
				{this.renderRow()}
				<View style={{position: 'absolute', alignItems: 'center', justifyContent: 'center', top: 40, alignSelf: 'center'}}>
					<ActivityIndicator size="large" color="#FF724C" />
				</View>
      </View>
    )
  }
}

export default SearchLoading
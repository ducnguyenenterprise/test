import React from 'react'
import { View, Image, TouchableWithoutFeedback, Dimensions } from "react-native"
import { connect } from 'react-redux'
import { episodeStyles } from '../../Home/utils/styles'
import { transformDescriptionOnSearchScreen } from '../../Player/utils/functions'
import { EpisodeTitle, Author } from '../../../utils/textMixins'
import subscribe from '../../../assets/subscribe.png'
import unsubscribe from '../../../assets/unsubscribe.png'
import Api from '../../../utils/Api'
import { setPlayerView } from '../../Player/utils/actions'
import { storeShow } from '../../Show/utils/actions'
import { withNavigation } from 'react-navigation'
import { storeSavedShows } from '../../Library/utils/actions'
import Helper from '../../utils/Helper'
import FastImage from 'react-native-fast-image'
import { getItem } from '../../../utils/asyncstorage'

class ShowTab extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSaved: null
    }

    this.api = new Api
  }

  openShow = id => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      api: this.api,
      navigation: this.props.navigation,
    })

    this.helper.openShow(id)

  }

  handleShow = async (id) => {
    const userId = await getItem('userId')
    this.setState({ isSaved: !this.state.isSaved })
    if (this.props.savedShows.find( ({ source_id }) => source_id === id ) !== undefined) {
      this.api.removeFromLibrary(userId, id)
      .then(library => {
        // if library.success === true && library.removed === true
        this.props.storeSavedShows(library.data.shows)

        if (this.state.isSaved !== false) {
          this.setState({
            isSaved: false
          })
        }
      })
    } else {
      this.api.addToLibrary(userId, id, 'show')
      .then(library => {
        // if library.success === true && library.inserted === true
        this.props.storeSavedShows(library.data.shows)

        if (this.state.isSaved !== true) {
          this.setState({
            isSaved: true
          })
        }
      })
    }
    
  }

  render() {
    const window = Dimensions.get('window')
    const { show } = this.props

    if (this.state.isSaved === null) {
      if (this.props.savedShows.find( ({ source_id }) => source_id === this.props.show.source_id ) !== undefined) {
        this.setState({
          isSaved: true
        })
      } else if (this.props.savedShows.find( ({ source_id }) => source_id === this.props.show.source_id ) === undefined) {
        this.setState({
          isSaved: false
        })
      }
    }

    return (
      <TouchableWithoutFeedback onPress={() => this.openShow(show.source_id)}>
        <View style={{flex: 1, flexDirection: 'row', height: 140, marginHorizontal: 20, justifyContent: 'flex-start', alignItems: 'center', borderBottomColor: '#D3D4D8', borderBottomWidth: 1}}>
          <View style={episodeStyles.touchable}>
            <FastImage source={{uri: show.thumbnail}} style={{width: 100, height: 100, borderRadius: 4, overflow: 'hidden'}} />
          </View>
          <View style={{paddingLeft: 10, width: window.width - 20 - 20 -10 - 100 - 8 - 32, marginRight: 8}}>
            <EpisodeTitle lines={2}>{show.title}</EpisodeTitle>
            <Author lines={1}>{show.author}</Author>
            {transformDescriptionOnSearchScreen(show.description)}
          </View>
          <TouchableWithoutFeedback onPress={() => this.handleShow(show.source_id)}>
            <View style={{width: 32, height: 32, alignSelf: 'flex-start', marginTop: 20}}>
              <Image source={this.state.isSaved === true ? unsubscribe : subscribe} style={{width: 32, height: 32}}/>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = state => {
  return {
    showResults: state.showResults,
    savedShows: state.savedShows
  }
}

const mapDispatchToProps = dispatch => ({
  storeSearchValue: (value) => dispatch(storeSearchValue(value)),
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  storeShow: (showData) => dispatch(storeShow(showData)),
  storeSavedShows: (shows) => dispatch(storeSavedShows(shows))
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ShowTab))

import React from 'react'
import { View, Image, TouchableWithoutFeedback, Text, Dimensions, ScrollView } from "react-native"
import i18n from '../../../utils/i18n'
import search from '../../../assets/keyword_search.png'
import { getItem, setItem } from '../../../utils/asyncstorage'
import { connect } from 'react-redux'
import { storeKeywords } from '../utils/actions'
import RNFS from 'react-native-fs'
import Api from '../../../utils/Api'
 
RNFS.exists(RNFS.DocumentDirectoryPath + '/music') 
.then(exists => {
  if (exists === false) {
    // create music directory
    const path = RNFS.DocumentDirectoryPath + '/music'

    RNFS.mkdir(path)
    .then((success) => {
      //console.log('FILE WRITTEN!')
    })
    .catch((err) => {
      //console.log(err.message)
    })
  }
})


class Intro extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      topKeywordIndex: null,
      recentKeywordIndex: null,
      topSearchesArray: []
    }

    this.api = new Api
  }

   componentDidMount() {
    this.api.fetchTopSearches()
    .then(res => {
      this.setState({ topSearchesArray: res.top_searches.split(',')})
    })

     getItem('keywords')
     .then(res => {
       if (res !== undefined && res !== null) {
         const keywordArray = res.split(']|[')
         this.props.storeKeywords(keywordArray)
       }
     })
   }

   handleClear = () => {
    this.props.storeKeywords([])
    setItem('keywords', null)
   }
  
   render() {
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'

    const topKeywords = this.state.topSearchesArray
    return (
      <ScrollView style={{flex: 1, marginTop: 60, marginBottom: 50, width: window.width, borderTopColor: '#F0F1F3', borderTopWidth: 1}}>
        <View style={{flex: 1, marginLeft: 20}}>
          <View style={{height: 24, marginTop: 18, marginBottom: 10, justifyContent: 'center', alignItems: 'flex-start'}}>
            <Text style={{fontFamily: 'Roboto', fontSize: 18, textTransform: 'uppercase', color: '#3E465F'}}>{i18n.t('hot keywords')}</Text>
          </View>
          <View style={{flexWrap: 'wrap', flexDirection: 'row'}}>
            {
              topKeywords.map((keyword, index) => {
                return (
                  <TouchableWithoutFeedback onPress={() => { this.props.tagClick(keyword); this.setState({ topKeywordIndex: index}) }}>
                    <View style={{height: 38, alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.topKeywordIndex === index ? '#FF724C' : '#A1A4B1', borderRadius: 24, overflow: 'hidden', marginRight: 10, marginTop: 10}}>
                      <Text style={{fontFamily: 'Roboto', fontSize: 18, textTransform: 'uppercase', color: 'white', paddingHorizontal: 20}}>{keyword}</Text>
                    </View>
                  </TouchableWithoutFeedback>
                )
              })
            }
          </View>
          <View style={{backgroundColor: '#F0F1F3', height: 1, width: window.width - 40, marginRight: 20, marginTop: 23}}></View>
          <View style={{height: 24, marginTop: 18, marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Text style={{fontFamily: 'Roboto', fontSize: 18, textTransform: 'uppercase', color: '#3E465F'}}>{i18n.t('recent searches')}</Text>
            <TouchableWithoutFeedback onPress={this.handleClear}>
              <Text style={{fontFamily: 'Roboto', fontSize: 14, color: this.props.keywords.length > 0 ? '#FF724C' : '#D3D4D8', padding: 20}}>{i18n.t('clear recent searches')}</Text>
            </TouchableWithoutFeedback>
          </View>
          <View>
            {
              this.props.keywords.map((keyword, index) => {
                return (
                  <TouchableWithoutFeedback onPress={() => { this.props.tagClick(keyword); this.setState({ recentKeywordIndex: index}) }}>
                    <View style={{flexDirection: 'row', height: 48, alignItems: 'center', justifyContent: 'flex-start'}}>
                      <Image source={search} style={{width: 48, height: 48}}/>
                      <Text style={{fontFamily: 'Roboto', fontSize: 16, color: this.state.recentKeywordIndex === index ? '#FF724C' : '#A1A4B1'}}>{keyword}</Text>
                    </View>
                  </TouchableWithoutFeedback>
                )
              })
            }
          </View>
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = state => {
  return {
    keywords: state.keywords,
    language: state.language
  }
}

const mapDispatchToProps = dispatch => ({
  storeKeywords: (words) => dispatch(storeKeywords(words))
})

export default connect(mapStateToProps, mapDispatchToProps)(Intro)

import { SEARCH_VALUE, SEARCH_TAB, LIST_SHOW_RESULTS, LIST_EPISODE_RESULTS, STORE_KEYWORDS, SEARCH_KEY } from './types'

export const storeSearchValue = value => {
  return {
    type: SEARCH_VALUE,
    value
  }
}

export const setSearchKey = () => {
  const randomString = [...Array(8)].map(i=>(~~(Math.random()*36)).toString(36)).join('')
  console.log('SETTTTT : ', randomString)
  return {
    type: SEARCH_KEY,
    randomString
  }
}

export const setActiveSearchTab = activeTab => {
  return {
    type: SEARCH_TAB,
    activeTab
  }
}

export const listShowResults = results => {
  return {
    type: LIST_SHOW_RESULTS,
    results
  }
}

export const listEpisodeResults = results => {
  return {
    type: LIST_EPISODE_RESULTS,
    results
  }
}

export const storeKeywords = words => {
  return {
    type: STORE_KEYWORDS,
    words
  }
}
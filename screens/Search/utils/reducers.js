import { SEARCH_VALUE, SEARCH_TAB, LIST_SHOW_RESULTS, LIST_EPISODE_RESULTS, STORE_KEYWORDS, SEARCH_KEY } from './types'

export const searchValue =  (state = {}, action) => {
  switch (action.type) {
    case SEARCH_VALUE:
     return action.value
    default:
      return state
  }
}

export const activeSearchTab =  (state = {}, action) => {
  switch (action.type) {
    case SEARCH_TAB:
     return action.activeTab
    default:
      return state
  }
}

export const searchKey = (state = '', action) => {
  switch (action.type) {
    case SEARCH_KEY:
      return action.randomString
    default:
      return state
  }
}

export const showResults =  (state = {}, action) => {
  switch (action.type) {
    case LIST_SHOW_RESULTS:
     return action.results
    default:
      return state
  }
}

export const episodeResults =  (state = {}, action) => {
  switch (action.type) {
    case LIST_EPISODE_RESULTS:
     return action.results
    default:
      return state
  }
}

export const keywords =  (state = {}, action) => {
  switch (action.type) {
    case STORE_KEYWORDS:
     return action.words
    default:
      return state
  }
}
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  input: {
    marginTop: 50, 
    borderColor: 'grey',
    borderWidth: 1,
    height: 40,
    width: 250,
    paddingLeft: 20 
  }
})
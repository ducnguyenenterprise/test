import React from 'react'
import { View, InteractionManager, Platform, StatusBar, SafeAreaView, Dimensions } from "react-native"
import Header from './components/Header'
import Intro from './components/Intro'
import SearchTabs from './components/SearchTabs'
import { connect } from 'react-redux'
import { getItem } from '../../utils/asyncstorage'
import { setPlayerView } from '../Player/utils/actions'
import KeyboardListener from 'react-native-keyboard-listener'
import LinearGradient from 'react-native-linear-gradient'
import { listEpisodeResults, listShowResults } from './utils/actions'

class SearchScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      displayIntro: true,
      tag: null,
      updateSearch: false
    }

    this.searchInput = null
  }

  updateSearch = () => {
    this.setState({ updateSearch: !this.state.updateSearch })
  }

  changeScreen = (displayIntro) => {
    this.setState({ displayIntro })
    if (displayIntro === true) {
      InteractionManager.runAfterInteractions(() => {
        this.props.listEpisodeResults({})
        this.props.listShowResults({})
      })
    }
  }

  setPlayer = view => {
    getItem('lastPlayed')
    .then(res => {
      if (res !== undefined && res !== null) {
        this.props.setPlayerView(view)
      }
    })
  }

  tagClick = tag => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        tag,
        displayIntro: false
      })
      setTimeout(() => {
        this.setState({
          tag: null
        })
      }, 1)
    })
  }

  setRef = ref => {
    this.searchInput = ref
  }
  
  focus = () => {
    this.searchInput.focus()
  }

  show = () => {
    getItem('lastPlayed')
    .then(res => {
      if (res !== undefined && res !== null) {
        this.props.setPlayerView('miniDown')
      }
    })
  }

  hide = () => {
    getItem('lastPlayed')
    .then(res => {
      if (res !== undefined && res !== null) {
        this.props.setPlayerView('miniUp')
      }
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.searchKey !== this.props.searchKey) {
      this.focus()
    }
  }
  
  render() {

    return (
      <View style={{flex: 1}}>
        <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 44 : 32}}>
          <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
        </LinearGradient>
        <SafeAreaView style={{flex: 1}}>
          {
            <View style={{flex: 1, width: window.width, justifyContent: 'center',}}>
              <KeyboardListener
                onDidShow={this.show}
                onDidHide={this.hide}
              />
              <View style={{position: 'absolute', top: 0, left: 0, right: 0}}> 
                <Header setRef={this.setRef} changeScreen={this.changeScreen} tag={this.state.tag} toUpdateSearch={this.state.updateSearch} updateSearch={this.updateSearch} />
              </View>
              {
                this.state.displayIntro === false  ?
                <SearchTabs updateSearch={this.updateSearch} />
                :
                <Intro tagClick={this.tagClick} />
              }
            </View>
          }
        </SafeAreaView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    searchValue: state.searchValue,
    internet: state.internet,
    searchKey: state.searchKey
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  listEpisodeResults: (results) => dispatch(listEpisodeResults(results)),
  listShowResults: (results) => dispatch(listShowResults(results))
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen)
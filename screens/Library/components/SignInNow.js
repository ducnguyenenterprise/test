import React from 'react'
import { Image, View, Dimensions, Text, TouchableWithoutFeedback } from "react-native"
import { connect } from 'react-redux'
import profile from '../../../assets/profile-placeholder.png'
import i18n from '../../../utils/i18n'
import { Body1, Heading5 } from '../../../utils/textMixins'
import { setLoginModal } from '../../Profile/utils/actions'
import LoginModal from '../../Profile/components/LoginModal'

class SignInNow extends React.Component {
  render() {
    const window = Dimensions.get('window')
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <View style={{marginTop: 20, borderBottomColor: '#D3D4D8', borderBottomWidth: 1}}>
        <View style={{flexDirection: 'row'}}>
          <Image source={profile} style={{width: 96, height: 96, borderRadius: 80, overflow: 'hidden', marginRight: 20}} />
          <View style={{width: window.width - 40 - 96 - 20}}>
            <Heading5 styles={{marginBottom: 18}}>{i18n.t('keep playlists')}</Heading5>
            <Body1>{i18n.t('sign in text')}</Body1>
          </View>
        </View>
        <View style={{alignItems: 'flex-end'}}>
        <TouchableWithoutFeedback onPress={() => this.props.setLoginModal(true)}>
          <View style={{width: 160, height: 45, alignItems: 'flex-end'}}>
            <Text style={{fontFamily: 'Roboto-Regular', fontSize: 18, textTransform: 'capitalize', color: '#FF724C'}}>{i18n.t('sign in now')}</Text>
          </View>
        </TouchableWithoutFeedback>
        </View>
        <LoginModal />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    language: state.language
  }
}

const mapDispatchToProps = dispatch => ({
  setLoginModal: (openState) => dispatch(setLoginModal(openState))
})

export default connect(mapStateToProps, mapDispatchToProps)(SignInNow)

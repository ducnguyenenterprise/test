import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import i18n from '../../../utils/i18n'
import { connect } from 'react-redux'
import Api from '../../../utils/Api'
import { storeShow } from '../../Show/utils/actions'
import { setPlayerView } from '../../Player/utils/actions'
import { withNavigation } from 'react-navigation'
import Helper from '../../utils/Helper'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import FollowedShows from './FollowedShows'
import SavedEpisodes from './SavedEpisodes'

class LibraryTabs extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    
    }
    
    this.api = new Api()
   }

  openShow = id => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      api: this.api,
      navigation: this.props.navigation,
    })

    this.helper.openShow(id)

  }

  render() {
    i18n.locale = this.props.language === 'Vietnamese' ? 'vi' : 'en'
    return (
      <ScrollableTabView 
        style={{marginTop: 60, width: 'auto', justifyContent: 'flex-start', height: 48, paddingBottom: this.props.playerView === 'hidden' ? 0 : 50}}
        tabBarActiveTextColor={'#3E465F'}
        tabBarInactiveTextColor={'#D3D4D8'}
        tabBarTextStyle={{fontFamily: 'Roboto', fontWeight: '500', fontSize: 18, textTransform: 'uppercase'}}
        tabBarUnderlineStyle={{ backgroundColor: "#FF724C", borderTopLeftRadius: 2, borderTopRightRadius: 2, height: 3 }}
      >
        <FollowedShows tabLabel={i18n.t('tab-shows')} />
        <SavedEpisodes tabLabel={i18n.t('tab-episodes')} />
      </ScrollableTabView>
    )
  }
}

const mapStateToProps = state => {
  return {
    language: state.language,
    playerView: state.playerView
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  storeShow: (showData) => dispatch(storeShow(showData))
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(LibraryTabs))

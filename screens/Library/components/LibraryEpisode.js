import React from 'react'
import { View, Image, TouchableWithoutFeedback, Dimensions, Alert, ImageBackground, ActivityIndicator } from "react-native"
import { connect } from 'react-redux'
import { episodeStyles } from '../../Home/utils/styles'
import { EpisodeTitle, Author } from '../../../utils/textMixins'
import play from '../../../assets/episode_play.png'
import remove from '../../../assets/unsubscribe.png'
import { setItem, getItem } from '../../../utils/asyncstorage'
import { setPlayerState, setPlayerView, addTrack } from '../../Player/utils/actions'
import download from '../../../assets/download.png'
import downloaded from '../../../assets/downloaded.png'
import RNFS from 'react-native-fs'
import Helper from '../../utils/Helper'
import Api from '../../../utils/Api'
import { storeSavedEpisodes, storeDownloadedIds } from '../../Library/utils/actions'
import Toast from 'react-native-simple-toast'
import i18n from '../../../utils/i18n'

class Episode extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isDownloading: false,
    }
    
    this.api = new Api()
    this.automatic = false // local copy of redux savedEpisodes
  }

  playEpisode = () => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      addTrack: this.props.addTrack, 
      setPlayerState: this.props.setPlayerState,
      playerView: 'miniUp'
    })

    this.helper.playEpisode(this.props.episode)

  }

  alertRemove = (id) => {
    Alert.alert(
      'Are you sure to remove this episode?',
      '',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Remove', onPress: () => this.removeEpisode(id)},
      ],
      {cancelable: false},
    )
  }

  removeEpisode = async id => {
    Toast.show(i18n.t('removed from library'))
    const userId = await getItem('userId')
    const library = await this.api.removeFromLibrary(userId, id)
    if (library.data.success === true && library.data.removed === true) {
      const epLibrary = library.data.episodes
      if (this.props.downloadedIds.length > 0) {
        epLibrary.forEach(ep => {
          if (this.props.downloadedIds.includes(ep.source_id)) {
            ep['isDownloaded'] = true
          } else {
            ep['isDownloaded'] = false
          }
        })
      } else {
        epLibrary.forEach(ep => {
          ep['isDownloaded'] = false
        })
      }
      this.props.storeSavedEpisodes(epLibrary)
    }

    const path = RNFS.DocumentDirectoryPath + `/music/${id}.mp3`
 
    RNFS.unlink(path)
    .then(() => {
      const idsArrayCopy = JSON.parse(JSON.stringify(this.props.downloadedIds))
      const index = idsArrayCopy.indexOf(id)
      if (index > -1) {
        idsArrayCopy.splice(index, 1)
      }
      
      this.props.storeDownloadedIds(idsArrayCopy.join('||'))

      // You only need this if you want to stop downloads: 
      // const savedEpisodesCopy = JSON.parse(JSON.stringify(this.props.savedEpisodes))

      // savedEpisodesCopy.find( ({ source_id }) => source_id === id).isDownloaded = false
      // this.props.storeSavedEpisodes(savedEpisodesCopy)
    })
    // `unlink` will throw an error, if the item to unlink does not exist
    .catch((err) => {
      // TODO error handling
      console.log(err.message)
    })
  }

  downloadEpisode = async (sourceId, fileId) => {
    console.log('DOWNLOADING: ', sourceId)
    const userId = await getItem('userId')
    this.setState({ isDownloading: true })
    
    this.helper = new Helper({
      getItem: getItem, 
      setItem: setItem,
    })

    const body = {
      event_type: 'save',
      content_type: 'episode',
      content_id: sourceId,
      date: new Date(),
      user_id: userId,
    }

    this.helper.storeEvent(body)
    
    const filePath = RNFS.DocumentDirectoryPath + `/music/${sourceId}.mp3`

    RNFS.downloadFile({fromUrl: fileId, toFile: filePath}).promise
    .then(res => {
      // if success
      if (res.jobId) {
        this.setState({ isDownloading: false })
        const idsArrayCopy = JSON.parse(JSON.stringify(this.props.downloadedIds))
        idsArrayCopy.push(sourceId)
        this.props.storeDownloadedIds(idsArrayCopy.join('||')) // this saves it to async as well
        
        const savedEpisodesCopy = JSON.parse(JSON.stringify(this.props.savedEpisodes))

        savedEpisodesCopy.find( ({ source_id }) => source_id === sourceId).isDownloaded = true
        this.props.storeSavedEpisodes(savedEpisodesCopy)
      }
    })
    
  }

  // observeStore = (store, select, onChange) => {
  //   let currentState
  
  //   function handleChange() {
  //     let nextState = select(store.getState())
  //     if (nextState !== currentState) {
  //       currentState = nextState
  //       onChange(currentState)
  //     }
  //   }
  
  //   let unsubscribe = store.subscribe(handleChange)
  //   handleChange()
  //   return unsubscribe
  // }

  render() {
    const window = Dimensions.get('window')
    const { episode } = this.props

    if (this.props.automaticDownload === true && episode.isDownloaded === false && !JSON.stringify(this.props.downloadedIds).includes(episode.source_id) && JSON.stringify(this.automatic) !== JSON.stringify(this.props.automaticDownload) && this.props.connectionType === 'wifi') {
      this.downloadEpisode(episode.source_id, episode.file_id)
      this.automatic = this.props.automaticDownload
      
    }

    return (
      <View style={{flex: 1, flexDirection: 'row', width: window.width - 40, marginHorizontal: 20, height: 75 + 20 + 20, alignItems: 'center', borderBottomColor: '#D3D4D8', borderBottomWidth: 1}}>
        <View style={episodeStyles.touchable}>
          <TouchableWithoutFeedback onPress={this.playEpisode} style={{width: 75, height: 75}}>
            <ImageBackground source={{uri: episode.thumbnail}} style={{width: 75, height: 75, borderRadius: 4, overflow: 'hidden', justifyContent: 'center', alignItems: 'center'}}>
              <TouchableWithoutFeedback onPress={this.playEpisode}>
                <Image source={play} style={{width: 40, height: 40}} />
              </TouchableWithoutFeedback>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </View>
        <View style={{paddingLeft: 10, width: window.width - 20 - 20 - 75 - 8 - 32, marginRight: 8}}>
          <EpisodeTitle lines={2}>{episode.title}</EpisodeTitle>
          <Author lines={1}>{episode.author}</Author>
        </View>
        <View style={{height: 75, justifyContent: 'space-between', alignItems: 'center'}}>
          <TouchableWithoutFeedback style={{width: 32, height: 32}} onPress={() => this.alertRemove(episode.source_id)}>
            <View style={{width: 32, height: 32, alignItems: 'center', justifyContent: 'center'}}>
              <Image source={remove} style={{width: 30, height: 30}}/>
            </View>
          </TouchableWithoutFeedback>
          { this.state.isDownloading === true ?
            <View style={{width: 32, height: 32, alignSelf: 'center', paddingTop: 10}}>
              <ActivityIndicator size='small' color='#FF724C' />
            </View>
            :
            episode.isDownloaded === true ?
            <View style={{width: 32, height: 32, alignSelf: 'center', paddingTop: 10}}>
              <Image source={downloaded} style={{height: 22, width: 22, alignSelf: 'center'}} />
            </View>
            :
            <TouchableWithoutFeedback onPress={() => this.downloadEpisode(episode.source_id, episode.file_id)}>
              <View style={{width: 32, height: 32}}>
                <Image source={download} style={{height: 32, width: 32, alignSelf: 'center', marginTop: 8}} />
              </View>
            </TouchableWithoutFeedback>
          }
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    downloadedIds: state.downloadedIds,
    savedEpisodes: state.savedEpisodes,
    automaticDownload: state.automaticDownload,
    connectionType: state.connectionType
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  setPlayerState: (isPlaying) => dispatch(setPlayerState(isPlaying)),
  addTrack: (track) => dispatch(addTrack(track)),
  storeSavedEpisodes: (episodes) => dispatch(storeSavedEpisodes(episodes)),
  storeDownloadedIds: (id) => dispatch(storeDownloadedIds(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Episode)

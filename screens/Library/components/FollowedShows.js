import React from 'react'
import { View, TouchableWithoutFeedback, Dimensions, ScrollView } from "react-native"
import { connect } from 'react-redux'
import Api from '../../../utils/Api'
import { EpisodeTitle, Author } from '../../../utils/textMixins'
import { storeShow } from '../../Show/utils/actions'
import { setPlayerView } from '../../Player/utils/actions'
import { withNavigation } from 'react-navigation'
import Helper from '../../utils/Helper'
import FastImage from 'react-native-fast-image'
import SignInNow from './SignInNow'

class FollowedShows extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      
    }
    
    this.api = new Api()
   }

  openShow = id => {
    this.helper = new Helper({
      setPlayerView: this.props.setPlayerView, 
      storeShow: this.props.storeShow,
      api: this.api,
      navigation: this.props.navigation,
    })

    this.helper.openShow(id)
  }

  render() {
    const window = Dimensions.get('window')
    return (
      <>
        <ScrollView contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', marginLeft: 20}}>
          { !this.props.isLoggedIn && <SignInNow /> }
          { 
            this.props.savedShows.map((show, i) => {
              return (
                <TouchableWithoutFeedback key={i + show.source_id + show.image_url} onPress={() => this.openShow(show.source_id)}>
                  <View style={{width: (window.width - 60) / 3, marginRight: 10, marginTop: 20}}>
                    <FastImage source={{uri: show.thumbnail}} style={{width: (window.width - 60) / 3, height: (window.width - 60) / 3, borderRadius: 4, overflow: 'hidden'}}/>
                    <EpisodeTitle lines={2} styles={{marginTop: 8}}>{show.title}</EpisodeTitle>
                    <Author lines={2}>{show.author}</Author>
                  </View>
                </TouchableWithoutFeedback>
              )
            })
          }
        </ScrollView>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    savedShows: state.savedShows,
    isLoggedIn: state.isLoggedIn
  }
}

const mapDispatchToProps = dispatch => ({
  setPlayerView: (playerView) => dispatch(setPlayerView(playerView)),
  storeShow: (showData) => dispatch(storeShow(showData))
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(FollowedShows))

import React from 'react'
import { FlatList, View } from "react-native"
import { connect } from 'react-redux'
import LibraryEpisode from './LibraryEpisode'
import { withNavigation } from 'react-navigation'
import SignInNow from './SignInNow'

class SavedEpisodes extends React.Component {
  render() {
    return (
      <>
        <FlatList
          style={{paddingBottom: 50}}
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.id + item.title + item.source_id}
          data={this.props.savedEpisodes.length > 0 && this.props.savedEpisodes}
          ListHeaderComponent={() => (
            !this.props.isLoggedIn &&
            <View style={{marginHorizontal: 20}}>
              <SignInNow />
            </View>
          )}
          renderItem={({item, index}) => (
            <LibraryEpisode episode={item} />
          )}
        />
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    savedEpisodes: JSON.parse(JSON.stringify(state.savedEpisodes)),
    isLoggedIn: state.isLoggedIn
  }
}

const mapDispatchToProps = dispatch => ({
 
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(SavedEpisodes))

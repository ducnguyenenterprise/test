export const STORE_SAVED_EPISODES = 'STORE_SAVED_EPISODES'
export const LIBRARY_TAB = 'LIBRARY_TAB'
export const STORE_SAVED_SHOWS = 'STORE_SAVED_SHOWS'
export const STORE_DOWNLOADED_IDS = 'STORE_DOWNLOADED_IDS'

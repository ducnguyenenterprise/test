import { STORE_SAVED_EPISODES, LIBRARY_TAB, STORE_SAVED_SHOWS, STORE_DOWNLOADED_IDS } from './types'

export const storeSavedEpisodes = episodes => {
  return {
    type: STORE_SAVED_EPISODES,
    episodes
  }
}

export const setActiveLibraryTab = activeTab => {
  return {
    type: LIBRARY_TAB,
    activeTab
  }
}

export const storeSavedShows = shows => {
  return {
    type: STORE_SAVED_SHOWS,
    shows
  }
}

export const storeDownloadedIds = idString => {
  return {
    type: STORE_DOWNLOADED_IDS,
    idString
  }
}
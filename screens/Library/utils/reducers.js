import { STORE_SAVED_EPISODES, LIBRARY_TAB, STORE_SAVED_SHOWS, STORE_DOWNLOADED_IDS } from './types'
import { setItem } from '../../../utils/asyncstorage'

export const savedEpisodes =  (state = {}, action) => {
  switch (action.type) {
    case STORE_SAVED_EPISODES:
      setItem('savedEpisodes', JSON.stringify(action.episodes))
     return action.episodes
    default:
      return state
  }
}

export const activeLibraryTab =  (state = {}, action) => {
  switch (action.type) {
    case LIBRARY_TAB:
     return action.activeTab
    default:
      return state
  }
}

export const savedShows =  (state = {}, action) => {
  switch (action.type) {
    case STORE_SAVED_SHOWS:
      setItem('savedShows', JSON.stringify(action.shows))
     return action.shows
    default:
      return state
  }
}

export const downloadedIds =  (state = {}, action) => {
  switch (action.type) {
    case STORE_DOWNLOADED_IDS:
      setItem('downloadedIds', action.idString)
      const idArray = action.idString ? action.idString.split('||') : []
     return idArray
    default:
      return state
  }
}


import React from 'react'
import { View, Platform, StatusBar, SafeAreaView, Dimensions } from 'react-native'
import Header from '../Home/components/Header'
import LibraryTabs from './components/LibraryTabs'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import Api from '../../utils/Api'

class LibraryScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      internet: false
    }
    
    this.api = new Api()
  }
  
  render() {
    return (
      <View style={{flex: 1}}>
        <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} colors={['#FF9549', '#FE4E4E']} style={{height: Platform.OS === 'android' ? StatusBar.currentHeight : (Dimensions.get('window').height >= 812 && Platform.OS === 'ios') ? 44 : 32}}>
          <StatusBar backgroundColor='transparent' translucent={true} barStyle='light-content' />
        </LinearGradient>
        <SafeAreaView style={{flex: 1}}>
          <View style={{position: 'absolute', top: 0, left: 0, right: 0, zIndex: 99999}}> 
            <Header navigation={this.props.navigation} withTab={true} />
          </View>
          <View style={{flex: 1}}>
            <LibraryTabs />
          </View>
        </SafeAreaView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    activeLibraryTab: state.activeLibraryTab,
  }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(LibraryScreen)


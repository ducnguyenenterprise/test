import { combineReducers } from 'redux'
import authState from '../screens/Intro/utils/reducers'
import { isPlaying, currentTrack, playerView, addTrack, speedModal, showTitle } from '../screens/Player/utils/reducer'
import { listHome, connectionType } from '../screens/Home/utils/reducer'
import { storeShow } from '../screens/Show/utils/reducers'
import { activeScreen, internet, showPoint, language } from '../screens/utils/reducers'
import { searchValue, activeSearchTab, showResults, episodeResults, keywords, searchKey } from '../screens/Search/utils/reducers'
import { activeLibraryTab, savedEpisodes, savedShows, downloadedIds } from '../screens/Library/utils/reducers'
import { automaticDownload, loginModal, isLoggedIn, userData } from '../screens/Profile/utils/reducers'

export default combineReducers({
  authState,
  playerView,
  currentTrack,
  isPlaying,
  home: listHome,
  currentTrack: addTrack,
  showData: storeShow,
  activeScreen,
  searchValue,
  searchKey,
  activeSearchTab,
  showResults,
  episodeResults,
  keywords,
  activeLibraryTab,
  savedEpisodes,
  savedShows,
  speedModal,
  downloadedIds,
  internet,
  automaticDownload,
  showPoint,
  language,
  loginModal,
  connectionType,
  showTitle,
  isLoggedIn,
  userData
})
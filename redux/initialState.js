import { showResults, episodeResults } from '../screens/Search/utils/reducers'

const initialState = { 
  playerView: 'hidden',
  isPlaying: 'notPlaying',
  home: [],
  currentTrack: {},
  showData: {},
  activeScreen: '',
  speedModal: 'closed',
  showTitle: '',
  
  searchValue: '',
  activeSearchTab: 'shows',
  searchKey: 'abc',
  showResults,
  episodeResults,
  keywords: [],

  activeLibraryTab: 'playlist',
  savedEpisodes: [],
  savedShows: [],
  downloadedIds: [],
  automaticDownload: true,

  internet: null,
  connectionType: null,
  showPoint: false,
  language: 'Vietnamese',

  loginModal: false,
  isLoggedIn: false,
  userData: {}
}

export default initialState

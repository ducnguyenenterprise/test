import { applyMiddleware, createStore } from 'redux'
import reducer from './reducer'
import initialState from './initialState'

import logger from 'redux-logger'

export const store = createStore(
  reducer,
  initialState,
  applyMiddleware(logger)
)
 
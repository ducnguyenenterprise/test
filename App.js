import React from 'react'
import { Provider } from "react-redux"
import { store } from './redux/store'
import { View, SafeAreaView, StatusBar, Platform, AppState} from 'react-native'
import StackNavigation from './components/Navigator/StackNavigator'
import TrackPlayer from 'react-native-track-player'
import Player from './screens/Player/components/Player'
import codePush from "react-native-code-push"
import RNUxcam from 'react-native-ux-cam'
import { alertTrackPlayerError } from './utils/alerts'
import { setItem, getItem, removeItem } from './utils/asyncstorage'
import Api from './utils/Api'
import OneSignal from 'react-native-onesignal'
import Axios from 'axios'
import NavigationService from './NavigationService'
import playerIcon from './assets/appIcon40.png'
import LinearGradient from 'react-native-linear-gradient'
import RNFS from 'react-native-fs'
RNUxcam.startWithKey('dgq5fdskwyeh1tm')
RNUxcam.optIntoSchematicRecordings()

let generatedUserId = ''
import * as Sentry from '@sentry/react-native';
import { Waves8UnitOfWork } from './unit_of_works/waves8_unit_of_work'
import { POINT_EVENTS } from './models/point_event'
import { handleShowPoint } from './utils/showPointUtil'

Sentry.init({ 
  dsn: 'https://35e59fe0af194a539ef614e144095bfe@sentry.io/1880334', 
});

RNFS.exists(RNFS.DocumentDirectoryPath + '/music')
.then(exists => {
  if (exists === false) {
    // create music directory
    const path = RNFS.DocumentDirectoryPath + '/music'

    RNFS.mkdir(path)
    .then((success) => {
      //console.log('FILE WRITTEN!')
    })
    .catch((err) => {
      //console.log(err.message)
    })
  }
})

console.disableYellowBox = true
export const UnitOfWork = new Waves8UnitOfWork();
export const AppStateChange = {
  asyncAppStateChanges: [],
  addAsyncAppStateChanges: (item) => {
    AppStateChange.asyncAppStateChanges.push(item);
  },
  removeAsyncAppStateChanges: (item) => {
    const index = AppStateChange.asyncAppStateChanges.indexOf(item);
    AppStateChange.asyncAppStateChanges.splice(index, 1);
  }
};

export class AsyncAppStateChangeItem{
  funcName;
  funcCallback;
  constructor(funcName, funcCallback){
    this.funcName = funcName;
    this.funcCallback = funcCallback;
  }
}

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      appState: AppState.currentState,
    }

    this.api = new Api()
    this.player = null
  }

  _handleAppStateChange = async (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      let userId = await getItem('userId');
      if(userId != null && userId != ""){
        let isLoggedIn = JSON.parse(await getItem("isLoggedIn"));
        if (isLoggedIn === true && await UnitOfWork.point_user_repository.checkAllowAddPoint(POINT_EVENTS.DailyLogin) === true) {
          handleShowPoint();
          await UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.DailyLogin);
        }
      }
    }
    AppStateChange.asyncAppStateChanges.forEach(async (item, index) => {
      await item(this.state.appState, nextAppState);
    });
    this.setState({appState: nextAppState});
  };

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    OneSignal.removeEventListener('received', this.onReceived);

    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds = async (device) =>  {
      console.log('Device info: ', device, generatedUserId);
      const onesignalId = device.userId
      let userId = await getItem('userId')

      if (!userId || userId === '') {
        this.api.setOneSignalId(onesignalId, generatedUserId)
      } else {
        this.api.setOneSignalId(onesignalId, userId)
      }
  }

  generateKey = () => {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
  }

  async componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    OneSignal.init('8d4e9ff5-4c4e-4438-a66e-b1dd1370ea79')
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);

    let userId = await getItem('userId');
    if(userId != null && userId != ""){
      if (await UnitOfWork.point_user_repository.checkAllowAddPoint(POINT_EVENTS.DailyLogin) === true) {
        handleShowPoint();
        await UnitOfWork.point_user_repository.addPointToUser(userId, POINT_EVENTS.DailyLogin);
      }
    }
    if (!userId || userId === '') {
      let res = await this.api.newUser()
      userId = res.data.id
      await setItem('userId', userId)
    }
    generatedUserId = userId

    let eventString = await getItem('eventString')
    const eventKeyArray = eventString ? eventString.split('||') : []
    
    eventKeyArray.forEach(async eventKey => {
      const eventAsyncObj = await getItem(eventKey)
      const eventObj = JSON.parse(eventAsyncObj)

      const res = await this.api.addEvent(userId, eventObj)

      if (res.data.success === true) {
        eventString = eventString.replace('||' + res.data.event.event_key, '')
      }

      await setItem('eventString', eventString)

    })

    // generate a random key to store event in async
    const eventKey = this.generateKey()

    // we add the key to a string of keys waiting to be sent to the Api
    getItem('eventString')
    .then(string => {
      setItem('eventString', string + '||' + eventKey)
    })

    const body = {
      event_key: eventKey,
      event_type: 'open',
      content_type: 'app',
      date: new Date(),
      user_id: userId
    }

    // save data to async with the random key
    setItem(eventKey, JSON.stringify(body))
  }

  componentDidUpdate(){
    console.log("componentDidUpdate");
  }

  render() {
    return (
      <Provider store={store}>
        <StackNavigation ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
        <View style={{position: 'absolute', bottom: 0, left: 0, right: 0}}>
          <Player />
        </View>
      </Provider>
    )
  }
}

let codePushOptions = { 
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME, 
  installMode: codePush.InstallMode.ON_NEXT_RESUME 
}
App = codePush(codePushOptions)(App)

export default App
import { BaseModel } from "./base_model";

export class User extends BaseModel {
    onesignal_id = "";
    email = "";
    name = "";
    social_id = "";
    persona_id = "";
    image_url = "";
}
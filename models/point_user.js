import { BaseModel } from "./base_model";
import { User } from "./user";

export class PointUser extends BaseModel {
    user = new User();
    created_date = new Date();
    modified_date = new Date();
    point_events = [];
    total = 0;
}
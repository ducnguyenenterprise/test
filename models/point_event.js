import { BaseModel } from "./base_model";

export const POINT_EVENTS = {
    ListenFiveMinute: "ListenFiveMinute",
    ListenFull: "ListenFull",
    Favourite: "Favourite",
    Share: "Share",
    FillProfile: "FillProfile",
    SignIn: "SignIn",
    FeedBack: "FeedBack",
    NotifyResponse: "NotifyResponse",
    DailyLogin: "DailyLogin"
  };

export class PointEvent extends BaseModel {
    
    user_id = "";
    event_name = POINT_EVENTS.DailyLogin;
    point = 0;
    created_date = new Date();
    updated_date = new Date();
}
